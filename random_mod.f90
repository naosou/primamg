module random1
  ! sikinote
  ! Date : 2015/03/17
  !      : 2015/09/07
  implicit none
  interface random_range
     module procedure &
          random_irange, &
          random_drange
  end interface random_range
contains    

  subroutine random_irange(iout,imin,imax,num)
    integer,intent(in)::imin,imax
    integer, intent(in) :: num
    integer,intent(out)::iout(num)
    double precision, allocatable :: d(:)
    integer a_size, i
    
    a_size = size(iout)
    allocate(d(a_size))
    
    call random_number(d)

    do i = 1, a_size
       d(i)=d(i)*dble(imax-imin+1)+dble(imin)-0.5d0
       iout(i)=floor(d(i))
       if(d(i)-dble(iout(i)).ge.0.5d0)then
          iout(i)=iout(i)+1
       endif
    enddo

    deallocate(d)
  end subroutine random_irange

  subroutine random_drange(dout,dmin,dmax,num)
    double precision,intent(in)::dmin,dmax
    integer, intent(in) :: num
    double precision,intent(out)::dout(num)
    double precision, allocatable :: d(:)
    integer a_size, i

    a_size = size(dout)
    allocate(d(a_size))
    
    call random_number(d)

    do i = 1, a_size
       dout(i)=d(i)*(dmax-dmin)+dmin
    enddo
    
  end subroutine random_drange

end module random1
