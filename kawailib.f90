subroutine CRSmatCCSeqCCS(CRS,CCS,mat_out)
  use m_primapr
  implicit none
  type(primapr), intent(in) :: CRS, CCS
  type(primapr), intent(inout) :: mat_out
  integer count
  integer i,j,k,l
  logical flag
  
  allocate(mat_out%aprl(CCS%nd))

  !Count the number of nonzero-elements.
  count = 0
  do l = 1, CCS%nd
     mat_out%aprl(l)%ndl = 0
     do k = 1, CRS%nd

        flag = .false.
        do j = 1, CCS%aprl(l)%ndl
           do i = 1 ,CRS%aprl(k)%ndl
              if(CRS%aprl(k)%lct(i) > CCS%aprl(l)%lct(j)) exit
              if(CRS%aprl(k)%lct(i) == CCS%aprl(l)%lct(j)) then
                 count = count + 1
                 mat_out%aprl(l)%ndl = mat_out%aprl(l)%ndl + 1
                 flag = .true.
                 exit
              endif
           enddo
           if(flag) exit
        enddo
        
     enddo
  enddo

  !Calculate CRSmatrix mat CCSmatrix. Output form is CCS.
  mat_out%nonzero = count
  allocate(mat_out%basep_avl(count), mat_out%basep_lct(count))
  
  count = 1
  do l = 1, CCS%nd
     mat_out%aprl(l)%avl(1:mat_out%aprl(l)%ndl) = basep_avl(count:count+mat_out%aprl(l)%ndl-1)
     mat_out%aprl(l)%lct(1:mat_out%aprl(l)%ndl) = basep_lct(count:count+mat_out%aprl(l)%ndl-1)
     count = count + mat_out%aprl(l)%ndl
     do k = 1, CRS%nd

        mat_out%aprl(l)%avl(k) = 0.0d0
        do j = 1, CCS%aprl(l)%ndl
           do i = 1 ,CRS%aprl(k)%ndl
              if(CRS%aprl(k)%lct(i) > CCS%aprl(l)%lct(j)) exit
              if(CRS%aprl(k)%lct(i) == CCS%aprl(l)%lct(j)) then
                 mat_out%aprl(l)%avl(k) = mat_out%aprl(l)%avl(k) + 
              endif
           enddo
        enddo

     enddo
  enddo


end subroutine CRSmatCCSeqCCS

subroutine CRSmatCCSeqCRS(CRS,CCS,out)
