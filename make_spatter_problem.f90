subroutine make_spatter(lenX, lenY, lenZ, nx, ny, nz, val, col_ind, row_ptr, n_val, Bval, n_row)
  use random1
  implicit none
  integer, intent(in) :: nx, ny, nz
  real(8), intent(in) :: lenX, lenY, lenZ
  real(8), pointer, intent(inout) :: val(:), Bval(:)
  integer, pointer, intent(inout) :: col_ind(:), row_ptr(:)
  integer, intent(out) :: n_val, n_row
  integer i, j ,k, mz, my, py, pz, p_source, count_r, count_v, count
  real(8) cent, dx, dy, dz, v_sc
  real(8), allocatable :: dfct(:, :, :)
  character(100) :: read_strg
  
  allocate(dfct(0:nx, 0:ny, 0:nz))
  call random_drange(dfct, 1.0d0, 10.0d0, (nx+1)*(ny+1)*(nz+1))
  ! write(*, *) dfct
  
  ! write(*, *) nx*ny*nz-1,  nx*ny*nz-1,  (nx*ny*nz-2)*7-2*(nx*ny+ny*nz+nx*nz)
  n_row = nx*ny*nz-1
  n_val = (nx*ny*nz-1)*7-2*(nx*ny+ny*nz+nx*nz)-6
 
  allocate(val(n_val), col_ind(n_val), row_ptr(n_row+1), Bval(n_row))
  call random_drange(Bval, -1.0d0, 1.0d0, n_row)
  
  dfct(nx/10:nx/10*9, ny/10:ny/10*9, nz/10:nz/5) = 100.d0
  do k = nz/10, nz/5
     do j = ny/10, ny/10*9
        count = (k-1)*nx*ny+(j-1)*nx
        Bval(count+nx/10-1:count+nx/10*9-1) = 0.d0
     enddo
  enddo
  
  write(*, *) nx/10,nx/10*9, ny/10,ny/10*9, nx/10,nx/5
  
  dx = 1.0d0 / ((lenX / dble(nx-1)) ** 2)
  dy = 1.0d0 / ((lenY / dble(ny-1)) ** 2)
  dz = 1.0d0 / ((lenZ / dble(nz-1)) ** 2)

  v_sc = 10.0d0
  
  mz = 1 - nx * ny
  my = 1 - ny
  py = nx + 1
  pz = nx*ny + 1
  count_r = 1
  count_v = 1
  row_ptr(1) = 1
  p_source = (nz/10-1)*nx*ny+(ny/2-1)*nx+nx/2
  ! write(*, *) 'source point' , nx/2, ny/2, nz/10, p_source
  do k = 1, nz
     do j = 1, ny
        do i = 1, nx
           if((k-1)*nx*ny+(j-1)*nx+i == p_source) then
              ! write(*, *) 'Souce', i, j, k
              mz = mz + 1
              my = my + 1
              py = py + 1
              pz = pz + 1
              cycle
           endif

           ! if(count_r == 6225) write(*, *) 'Check2_bef', i, j, k, count_v
           ! if(count_r == 8725) write(*, *) 'Check3_bef', i, j, k, count_v
           
           cent = 0.0d0

           if((k-2)*nx*ny+(j-1)*nx+i /= p_source) then
              if(k == 1) then
                 cent = cent + dz * dfct(i, j, k-1)
              else
                 val(count_v) = dz * dfct(i, j, k-1)
                 col_ind(count_v) = mz
                 cent = cent + dz * dfct(i, j, k-1)
                 count_v = count_v + 1
                 ! if(count_r == 8776) write(*, *) 'mz'
              endif
           else
              Bval(count_r) = Bval(count_r) - dz * dfct(i, j, k-1) * v_sc 
              mz = mz - 1
              ! write(*, *) 'Souce_mz', i, j, k
           endif
              
           if((k-1)*nx*ny+(j-2)*nx+i /= p_source) then
              if(j == 1) then
                 cent = cent + dy * dfct(i, j-1, k)
              else
                 val(count_v) = dy * dfct(i, j-1, k)
                 col_ind(count_v) = my
                 cent = cent + dy * dfct(i, j-1, k)
                 count_v = count_v + 1
                 ! if(count_r == 8776) write(*, *) 'my'
              endif
           else
              Bval(count_r) = Bval(count_r) - dy * dfct(i, j-1, k) * v_sc 
              my = my - 1
              ! write(*, *) 'Souce_my', i, j, k
           endif

           if((k-1)*nx*ny+(j-1)*nx+i-1 /= p_source) then
              if(i == 1) then
                 cent = cent + dx * dfct(i-1, j, k)
              else
                 val(count_v) = dx * dfct(i-1, j, k)
                 col_ind(count_v) = count_r-1
                 cent = cent + dx * dfct(i-1, j, k)
                 count_v = count_v + 1
                 ! if(count_r == 8776) write(*, *) 'mx'
              endif
           else
              Bval(count_r) = Bval(count_r) - dx * dfct(i-1, j, k) * v_sc 
              ! write(*, *) 'Souce_mx', i, j, k
           endif
           
           if((k-1)*nx*ny+(j-1)*nx+i+1 /= p_source) then
              if(i == nx) then
                 cent = cent + dx * dfct(i, j, k)
              else
                 val(count_v) = dx * dfct(i, j, k)
                 col_ind(count_v) = count_r+1
                 cent = cent + dx * dfct(i, j, k)
                 count_v = count_v + 1
                 ! if(count_r == 8776) write(*, *) 'px'
              endif
           else
              Bval(count_r) = Bval(count_r) - dx * dfct(i, j, k) * v_sc 
              ! write(*, *) 'Souce_px', i, j, k
           endif

           if((k-1)*nx*ny+j*nx+i /= p_source) then
              if(j == ny) then
                 cent = cent + dy * dfct(i, j, k)
              else
                 val(count_v) = dy * dfct(i, j, k)
                 col_ind(count_v) = py
                 cent = cent + dy * dfct(i, j, k)
                 count_v = count_v + 1
                 ! if(count_r == 8776) write(*, *) 'py'
              endif
           else
              Bval(count_r) = Bval(count_r) - dy * dfct(i, j, k) * v_sc
              py = py - 1
              ! write(*, *) 'Souce_py', i, j, k
           endif
           
           if(count_r == 1) write(*, *) dz, dfct(i, j, k)
           if(k*nx*ny+(j-1)*nx+i /= p_source) then
              if(k == nz) then
                 cent = cent + dz * dfct(i, j, k)
              else
                 val(count_v) = dz * dfct(i, j, k)
                 col_ind(count_v) = pz
                 cent = cent + dz * dfct(i, j, k)
                 count_v = count_v + 1
                 ! if(count_r == 8776) write(*, *) 'pz'
              endif
           else
              Bval(count_r) = Bval(count_r) - dz * dfct(i, j, k) * v_sc
              pz = pz - 1
              ! write(*, *) 'Souce_pz', i, j, k
           endif

           val(count_v) = -cent
           col_ind(count_v) = count_r

           count_v = count_v + 1
           count_r = count_r + 1

           row_ptr(count_r) = count_v

           ! if(count_r == 2) write(*, *) '2', val(row_ptr(count_r-1):row_ptr(count_r)-1), col_ind(row_ptr(count_r-1):row_ptr(count_r)-1), row_ptr(count_r-1), row_ptr(count_r)-1
           ! if(count_r == 3) write(*, *) '3', val(row_ptr(count_r-1):row_ptr(count_r)-1), col_ind(row_ptr(count_r-1):row_ptr(count_r)-1), row_ptr(count_r-1), row_ptr(count_r)-1
           
           ! if(count_r == 6226) write(*, *) 'Check2_aft', i, j, k, count_v
           ! if(count_r == 8726) write(*, *) 'Check3_aft', i, j, k, count_v

           mz = mz + 1
           my = my + 1
           py = py + 1
           pz = pz + 1
                 
           ! if(i == 1 .and. j == 1 .and. k == 1) write(*, *) 'Check2', col_ind(1:count_v-1)
              
        enddo
     enddo
  enddo

  write(*, *) 'Check, spatter model', n_val, n_row

end subroutine make_spatter
