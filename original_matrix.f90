subroutine read_original_mm(Aval, col_ind, row_ptr, Bval, nd)
  implicit none
  integer, intent(out) :: nd
  integer, pointer, intent(out) :: col_ind(:), row_ptr(:)
  real(8), pointer, intent(inout) :: Aval(:), Bval(:)
  integer n_len, n_val, i, i_min, i_max, j
  character(100) :: read_strg
  
  interface 
     subroutine make_spatter(lenX, lenY, lenZ, nx, ny, nz, val, col_ind, row_ptr, n_val, Bval, n_row)
       use random1
       implicit none
       integer, intent(in) :: nx, ny, nz
       real(8), intent(in) :: lenX, lenY, lenZ
       real(8), pointer, intent(out) :: val(:), Bval(:)
       integer, pointer, intent(out) :: col_ind(:), row_ptr(:)
       integer, intent(out) :: n_val, n_row
     end subroutine make_spatter

     recursive subroutine quicksort(master, val, n)
       implicit none
       integer, intent(in) :: n
       integer, intent(inout) :: master(:)
       real(8), intent(inout) :: val(:)
     end subroutine quicksort

  end interface
  
  call get_command_argument(3,read_strg)
  !n_len = len_trim(read_strg)
  read_strg = trim(read_strg)

  select case(read_strg)
  case("p_spatter")
     call make_spatter(5.0d-1, 5.0d-1, 2.0d-1, 250, 250, 200, Aval, col_ind, row_ptr, n_val, Bval, nd)
     do i = 1, nd
        i_min = row_ptr(i)
        i_max = row_ptr(i+1)-1
        call quicksort(col_ind(i_min:i_max), aval(i_min:i_max), i_max-i_min+1)
     enddo
     ! write(*, *) 'Check3', col_ind(1:row_ptr(2)-1)
     ! allocate(Bval(nd))
     ! Bval = 0.0d0
  end select

  ! write(*, *) nd, nd, n_val
  ! do j = 1, nd
  !    do i = row_ptr(j), row_ptr(j+1)-1
  !       write(*, *) j, col_ind(i), Aval(i)
  !    enddo
  ! enddo
  ! stop
  
end subroutine read_original_mm

recursive subroutine quicksort(master, val, n)
  implicit none
  integer, intent(in) :: n
  integer, intent(inout) :: master(:)
  real(8), intent(inout) :: val(:)
  integer i, j, pivot, temp_i
  real(8) temp_r
  logical flag

  flag = .false.
  temp_i = master(1)
  do i = 2, n
     if(temp_i /= master(i)) then
        flag = .true.
        exit
     endif
  enddo

  if(.not. flag) return

  pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
  if(pivot == minval(master)) then
     do i = 1, n
        if(master(i) > pivot) then
           pivot = master(i)
           exit
        endif
     enddo
  endif

  i = 1
  j = n
  do while (i <= j)
     
     do while (master(i) < pivot .and. i < n)
        i = i + 1
     enddo

     do while (pivot <= master(j) .and. j > 1) 
        j = j - 1
     enddo

     if (i > j) exit

     temp_i = master(i)
     master(i) = master(j)
     master(j) = temp_i

     temp_r = val(i)
     val(i) = val(j)
     val(j) = temp_r
     
     i = i + 1
     j = j - 1

  enddo

  !if(slave(1) == 109) then
  !   write(*, *) '###################################'
  !   write(*, *) 'pivot is ', pivot, n, i, j
  !   write(*, *) master
  !endif
  
  !write(*, *) 'Check after', i, j, n
  if (i-1 >= 2) call quicksort(master(1:i-1), val(1:i-1), i-1)
  if (n - 1 + 1 >= 2) call quicksort(master(i:n), val(i:n), n - i + 1) 
  
end subroutine quicksort
