!C**************************************************************************
!C  This file includes basic routines for arithmetic of matrices
!C  which are represented by apr format
!C  created by Akihiro Ida at Kyoto University on Jly 2014
!C  last modified Akihiro Ida on Nov 2014
!C  last modified Masatoshi Kawai on Sep 2014
!C**************************************************************************
module m_primapr
  use mod_mbmc
  implicit none
 ! use m_primamg
  ! implicit double precision(a-h,o-z)

  !*** type :: primvec
  type :: primvec
     double precision,pointer :: v(:)=>null()
  end type primvec

  !*** type :: primail
  type :: primail
     integer :: ndl ! Number of non-zero elements in the col.
     double precision :: ado  ! Inverse of the diagonal element
     ! integer diag_ind ! Index of diagonal
     double precision,pointer :: avl(:)=>null()  ! Values of non-zero elements. avl(1) must be the diagonal element.
     integer,pointer :: lct(:)=>null() ! Ordinal numbers in the column direction
  end type primail

  !*** type :: primapr
  type :: primapr
     integer nd ! Number of rows
     integer :: ndl_rm
     integer nonzero ! Number of nonzero elements.
     type(primail), pointer :: aprl(:)
     integer, pointer :: basep_lct(:)
     double precision, pointer :: basep_avl(:)
  end type primapr

  type :: primapr_dense
     integer nd ! Number of rows
     double precision, pointer :: avl(:, :)
  end type primapr_dense

  type :: primdc_col_elem
     integer :: ndl_st1, ndl_st2, ndl_st3, ndl_all, ndl_ar, nonzero
     integer, pointer :: val_st1(:)=>null(), val_st2(:)=>null(), val_st3(:)=>null(), val_all(:)=>null(), val_ar(:)=>null()
  end type primdc_col_elem

  type :: primdc
     integer nd, ndl_rm
     type(primdc_col_elem), allocatable :: row(:)
     integer, pointer :: basep_val(:), basep_ar(:), val_rm(:)=>null()
     integer, allocatable :: rev_ind(:)
  end type primdc

  !*** type :: primaprccs
  type :: primaprccs
     integer nm ! Number of corse-grids
     type(primapr),pointer :: apr(:)=>null()
     type(primapr),pointer :: apr_reord(:)=>null()
     type(primapr),pointer :: tfc(:)=>null()
     type(primapr),pointer :: tcf(:)=>null()
     type(primvec),pointer :: uc(:)=>null()
     type(primvec),pointer :: bc(:)=>null()
     type(primvec),pointer :: rc(:)=>null()
     type(ordering),pointer :: order(:)=>null()
     type(primapr_dense) :: apr_dense
     integer :: num_params = 30
     double precision, pointer :: param(:) ! parameters for AMG
  end type primaprccs

  interface
     double precision function dotp_d(nd,za,zb,stat,end)
       implicit none
       integer, intent(in) :: nd,stat,end
       double precision, intent(in) :: za(:),zb(:)
     end function dotp_d
  end interface

end module m_primapr

!***adotv_ail
subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
  use m_primapr
  implicit none
  integer, intent(in) :: nd, stat, end
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: zv(nd)
  double precision, intent(out) :: zav(nd)
  integer il, it, nt, diag
  double precision zz

  !do il=1,nd
  do il=stat,end
     diag = aprl(il)%lct(1)
     zav(diag) = 0.0d0
     do it=1,aprl(il)%ndl
        nt=aprl(il)%lct(it); zz=aprl(il)%avl(it)
        zav(diag)=zav(diag)+zz*zv(nt)
     enddo
  enddo

endsubroutine adotv_ail

!***adotsub_ail
subroutine adotsub_ail(zr,aprl,zv,nd,stat,end)
  use m_primapr
  implicit none
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: zv(nd)
  double precision, intent(inout) :: zr(nd)
  integer, intent(in) :: nd, stat, end
  double precision, allocatable :: zav(:)

  interface
     subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
       use m_primapr
       implicit none
       integer, intent(in) :: nd,stat,end
       type(primail), intent(in) :: aprl(nd)
       double precision, intent(in) :: zv(nd)
       double precision, intent(out) :: zav(nd)
     end subroutine adotv_ail
  end interface

  allocate(zav(nd))
  call adotv_ail(zav,aprl,zv,nd,stat,end)
  zr(stat:end)=zr(stat:end)-zav(stat:end)
  deallocate(zav)

endsubroutine adotsub_ail

!***gs_ail
subroutine gs_ail(zav,aprl,zv,nd,stat,end,nitr,time,ig)
  use m_primapr
  implicit none
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: zv(nd)
  double precision, intent(inout) :: zav(nd), time(2)
  integer, intent(in) :: nd, nitr, stat, end, ig
  integer i, il, it, nt, diag
  double precision zs

  if(ig == 1) time(2) = time(2) - omp_get_wtime()
  do i=1,nitr
     if(i == 1 .and. ig == 1) time(1) = time(1) - omp_get_wtime()

     do il=stat,end
        diag = aprl(il)%lct(1)

        zs=zv(diag)
        do it=2,aprl(il)%ndl
           nt=aprl(il)%lct(it)
           zs=zs-aprl(il)%avl(it)*zav(nt)
        enddo
        !zav(il)=zav(il)+zs*aprl(il)%ado
        zav(diag)=zs*aprl(il)%ado

     enddo

     if(i == 1 .and. ig == 1) time(1) = time(1) + omp_get_wtime()
  enddo
  if(ig == 1) time(2) = time(2) + omp_get_wtime()

endsubroutine gs_ail

subroutine gs_ail_rev(zav,aprl,zv,nd,stat,end,nitr,time,ig)
  use m_primapr
  implicit none
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: zv(nd)
  double precision, intent(inout) :: zav(nd), time(2)
  integer, intent(in) :: nd, nitr, stat, end, ig
  integer i, il, it, nt, diag
  double precision zs

  if(ig == 1) time(2) = time(2) - omp_get_wtime()
  do i=1,nitr
     if(i == 1 .and. ig == 1) time(1) = time(1) - omp_get_wtime()

     do il=end, stat, -1
        diag = aprl(il)%lct(1)

        zs=zv(diag)
        do it=2,aprl(il)%ndl
           nt=aprl(il)%lct(it)
           zs=zs-aprl(il)%avl(it)*zav(nt)
        enddo
        !zav(il)=zav(il)+zs*aprl(il)%ado
        zav(diag)=zs*aprl(il)%ado

     enddo

     if(i == 1 .and. ig == 1) time(1) = time(1) + omp_get_wtime()
  enddo
  if(ig == 1) time(2) = time(2) + omp_get_wtime()

endsubroutine gs_ail_rev

!***cg_ail
subroutine mgcg_ail(u,aprl,b,amcc,eps,nd,mstep,time)
  use m_primapr
  implicit none
  integer, intent(in) :: nd, mstep
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: b(nd)
  type(primaprccs), intent(inout) :: amcc
  double precision, intent(inout) :: u(nd), time(amcc%nm+7)
  double precision, intent(in) :: eps

  integer in, il, stat, end, istat, iend, clr, blc, num_threads, tmp_i, me_omp, diag, i
  ! double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
  ! double precision, allocatable :: zar(:), capap(:)
  double precision alpha, beta, tmp_d, t_amg, t_oth, t_adotv
  double precision, allocatable, target, save :: zp(:), zq(:), zr(:)
  double precision, save :: bnorm, zrnorm, rho, gamma, nu

1000 format(5(a,i10)/)
2000 format(5(a,f10.4)/)
  
  interface
     subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
       use m_primapr
       implicit none
       integer, intent(in) :: nd, stat, end
       type(primail), intent(in) :: aprl(nd)
       double precision, intent(in) :: zv(nd)
       double precision, intent(out) :: zav(nd)
     end subroutine adotv_ail
  end interface

  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1
  num_threads = 1
  !$ num_threads = omp_get_num_threads()
  time(amcc%nm+7) = 0.0d0
  
  if(me_omp == 1) then
     if(amcc%param(9) /= amcc%param(10)) write(*, *) 'Check param(9) and param(10). This is in the mgcg_ail routine.'
     if(amcc%param(11) /= amcc%param(12)) write(*, *) 'Check param(11) and param(12). This is in the mgcg_ail routine.'
  endif

  ! tmp_i = mod(nd, num_threads)
  ! if(me_omp <= tmp_i) then
  !    istat = ((nd-tmp_i)/num_threads+1) * (me_omp-1) + 1
  !    iend = ((nd-tmp_i)/num_threads+1) * me_omp
  ! else
  !    istat = ((nd-tmp_i)/num_threads) * (me_omp-1) + 1 + tmp_i
  !    iend = ((nd-tmp_i)/num_threads) * me_omp + tmp_i
  ! endif
  istat = amcc%order(1)%thread(me_omp)%stat
  iend = amcc%order(1)%thread(me_omp)%end

  ! write(*, *) 'Check', me_omp, num_threads, nd, istat, iend
  ! stop

  if(me_omp == 1) then
     print*,'cg_ail start'
     allocate(zr(nd),zp(nd),zq(nd))
  endif
  !$OMP barrier
  ! alpha = 0.0;  beta = 0.0

  ! t_amg = 0.0d0
  ! t_oth = 0.0d0

  ! t_oth = omp_get_wtime()

  ! t_adotv = omp_get_wtime()
  stat = amcc%order(1)%thread_rmnd(me_omp)%stat
  end = amcc%order(1)%thread_rmnd(me_omp)%end
  ! do i = amcc%Apr_reord(1)%nd-amcc%Apr_reord(1)%ndl_rm+1, amcc%Apr_reord(1)%nd
  do i = stat, end
     ! write(*, *) 'Check', i
     diag = amcc%Apr_reord(1)%aprl(i)%lct(1)
     zr(diag) = amcc%Apr_reord(1)%aprl(i)%ado * u(diag)
  enddo
  do clr = 1, amcc%order(1)%c_num
     do blc = amcc%order(1)%color(clr)%thread(me_omp)%stat, amcc%order(1)%color(clr)%thread(me_omp)%end
        stat = amcc%order(1)%color(clr)%iblock(blc)%stat
        end = amcc%order(1)%color(clr)%iblock(blc)%end
        write(*, *) 'Check range', stat, end
        call adotv_ail(zr,amcc%Apr_reord(1)%aprl,u,nd,stat,end)
     enddo
  enddo
  !$OMP barrier
  ! t_adotv = omp_get_wtime() - t_adotv

  zr(istat:iend)=b(istat:iend)-zr(istat:iend)
  tmp_d = dotp_d(nd, zr, zr, istat, iend)
  if(me_omp == 1) bnorm = 0.0d0
  !$OMP barrier
  !$OMP atomic
  bnorm = bnorm + tmp_d
  !$OMP barrier
  if(me_omp == 1) then
     bnorm=dsqrt(bnorm)
     zrnorm = bnorm
  endif
  !$OMP barrier

  zp(istat:iend) = 0.0d0
  if(me_omp == 1) then
     amcc%uc(1)%v => zp(1:nd)
     amcc%bc(1)%v => zr(1:nd)
  endif
  !$OMP barrier
  time(amcc%nm+7) = time(amcc%nm+7) - omp_get_wtime()
  call vcycle_symmetric(amcc, time)
  time(amcc%nm+7) = time(amcc%nm+7) + omp_get_wtime()
  
  if(me_omp == 1) rho = 0.0d0
  !$OMP barrier
  tmp_d = dotp_d(nd,zr,zp,istat,iend)
  !$OMP atomic
  rho = rho + tmp_d
  !$OMP barrier

  if(me_omp == 1) amcc%uc(1)%v => zq(1:nd)
     
  do in=1,mstep
     if(me_omp == 1) write(*, *) 'CG convergence', in, zrnorm/bnorm
     if(zrnorm/bnorm<eps) then
        if(me_omp == 1) write(*, *) 'Converged with', in
        exit
     endif

     ! t_adotv = t_adotv - omp_get_wtime()

     ! call adotv_ail(zq,amcc%Apr_reord(1)%aprl,zp,nd,istat,iend)
     stat = amcc%order(1)%thread_rmnd(me_omp)%stat
     end = amcc%order(1)%thread_rmnd(me_omp)%end
     do i = stat, end
        diag = amcc%Apr_reord(1)%aprl(i)%lct(1)
        zq(diag) = amcc%Apr_reord(1)%aprl(i)%ado * zp(diag)
     enddo
     do clr = 1, amcc%order(1)%c_num
        do blc = amcc%order(1)%color(clr)%thread(me_omp)%stat, amcc%order(1)%color(clr)%thread(me_omp)%end
           stat = amcc%order(1)%color(clr)%iblock(blc)%stat
           end = amcc%order(1)%color(clr)%iblock(blc)%end
           call adotv_ail(zq,amcc%Apr_reord(1)%aprl,zp,nd,stat,end)
        enddo
     enddo

     ! adotv = t_adotv + omp_get_wtime()
     
     if(me_omp == 1) gamma = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd,zp,zq,istat,iend)
     !$OMP atomic
     gamma = gamma + tmp_d
     !$OMP barrier
     alpha = rho / gamma

     u(istat:iend)=u(istat:iend)+alpha*zp(istat:iend)
     zr(istat:iend)=zr(istat:iend)-alpha*zq(istat:iend)
     if(me_omp == 1) zrnorm = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd, zr, zr, istat, iend)
     !$OMP atomic
     zrnorm = zrnorm + tmp_d
     !$OMP barrier
     if(me_omp == 1) zrnorm = dsqrt(zrnorm)

     zq(istat:iend) = 0.0d0
     !$OMP barrier
     ! amcc%bc(1)%v => zr(1:nd)
     time(amcc%nm+7) = time(amcc%nm+7) - omp_get_wtime()
     call vcycle_symmetric(amcc, time)     
     time(amcc%nm+7) = time(amcc%nm+7) + omp_get_wtime()

     nu = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd,zq,zr,istat,iend)
     !$OMP atomic
     nu = nu + tmp_d
     !$OMP barrier
     beta = nu / rho
     !$OMP barrier
     if(me_omp == 1) rho = nu

     zp(istat:iend) =zq(istat:iend)+beta*zp(istat:iend)
     !$OMP barrier
  enddo
  
  ! t_oth = omp_get_wtime() - t_oth 
  ! write(*, *) 'Time amg, oth and adotv', t_amg, t_oth-t_amg, t_adotv

endsubroutine mgcg_ail

subroutine cg_ail(u,aprl,b,eps,nd,mstep)
  use m_primapr
  implicit none
  integer, intent(in) :: nd, mstep
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: b(nd)
  double precision, intent(inout) :: u(nd)
  double precision, intent(in) :: eps

  integer in, il, stat, end, istat, iend, clr, blc, tmp_i, diag, i
  ! double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
  ! double precision, allocatable :: zar(:), capap(:)
  double precision alpha, beta, tmp_d, t_amg, t_oth, t_adotv
  double precision, allocatable, target, save :: zp(:), zq(:), zr(:)
  double precision, save :: bnorm, zrnorm, rho, gamma, nu
  integer me_omp, num_threads, rem, num
  
1000 format(5(a,i10)/)
2000 format(5(a,f10.4)/)
  
  interface
     subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
       use m_primapr
       implicit none
       integer, intent(in) :: nd, stat, end
       type(primail), intent(in) :: aprl(nd)
       double precision, intent(in) :: zv(nd)
       double precision, intent(out) :: zav(nd)
     end subroutine adotv_ail
  end interface

  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1
  num_threads = 1
  !$ num_threads = omp_get_num_threads()

  rem = mod(nd, num_threads)
  num = (nd-rem) / num_threads
  write(*, *) "Check init", me_omp, num_threads, nd, rem, num
  if(me_omp <= rem) then
     istat = (me_omp-1) * (num + 1) + 1
     iend  = istat + num 
  else
     istat = (me_omp-1) * num + rem + 1
     iend  = istat + num - 1
  endif
  write(*, *) 'Check range', me_omp, istat, iend

  if(me_omp == 1) then
     print*,'cg_ail start'
     allocate(zr(nd),zp(nd),zq(nd))
  endif
  !$OMP barrier

  call adotv_ail(zr,aprl,u,nd,istat,iend)
  zr(istat:iend)=b(istat:iend)-zr(istat:iend)
  tmp_d = dotp_d(nd, zr, zr, istat, iend)
  if(me_omp == 1) bnorm = 0.0d0
  !$OMP barrier
  !$OMP atomic
  bnorm = bnorm + tmp_d
  !$OMP barrier
  if(me_omp == 1) then
     bnorm=dsqrt(bnorm)
     zrnorm = bnorm
  endif
  !$OMP barrier

  zp(istat:iend) = 0.0d0
  !$OMP barrier
  zp(istat:iend) = zr(istat:iend)
  
  if(me_omp == 1) rho = 0.0d0
  !$OMP barrier
  tmp_d = dotp_d(nd,zr,zp,istat,iend)
  !$OMP atomic
  rho = rho + tmp_d
  !$OMP barrier

  do in=1,mstep
     if(me_omp == 1) write(*, *) 'CG convergence', in, zrnorm/bnorm
     if(zrnorm/bnorm<eps) then
        if(me_omp == 1) write(*, *) 'Converged with', in
        exit
     endif

     call adotv_ail(zq,aprl,zp,nd,istat,iend)
     ! adotv = t_adotv + omp_get_wtime()
     
     if(me_omp == 1) gamma = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd,zp,zq,istat,iend)
     !$OMP atomic
     gamma = gamma + tmp_d
     !$OMP barrier
     alpha = rho / gamma

     u(istat:iend)=u(istat:iend)+alpha*zp(istat:iend)
     zr(istat:iend)=zr(istat:iend)-alpha*zq(istat:iend)
     if(me_omp == 1) zrnorm = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd, zr, zr, istat, iend)
     !$OMP atomic
     zrnorm = zrnorm + tmp_d
     !$OMP barrier
     if(me_omp == 1) zrnorm = dsqrt(zrnorm)

     zq(istat:iend) = zr(istat:iend)
     !$OMP barrier
     ! amcc%bc(1)%v => zr(1:nd)
     
     nu = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd,zq,zr,istat,iend)
     !$OMP atomic
     nu = nu + tmp_d
     !$OMP barrier
     beta = nu / rho
     !$OMP barrier
     if(me_omp == 1) rho = nu

     zp(istat:iend) =zq(istat:iend)+beta*zp(istat:iend)
     !$OMP barrier
  enddo
  
  ! t_oth = omp_get_wtime() - t_oth 
  ! write(*, *) 'Time amg, oth and adotv', t_amg, t_oth-t_amg, t_adotv

endsubroutine cg_ail

!***cg_ail
subroutine cg_ail_seq(u,aprl,b,amcc,eps,nd,mstep,time)
  use m_primapr
  implicit none
  integer, intent(in) :: nd, mstep
  type(primail), intent(in) :: aprl(nd)
  double precision, intent(in) :: b(nd)
  double precision, intent(inout) :: u(nd), time(2)
  type(primaprccs), intent(inout) :: amcc
  double precision, intent(in) :: eps

  integer in, il, istat, iend, clr, blc, num_threads, tmp_i, me_omp, diag, i
  ! double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
  ! double precision, allocatable :: zar(:), capap(:)
  double precision alpha, beta, tmp_d, t_amg, t_oth, t_adotv
  double precision, allocatable, target, save :: zp(:), zq(:), zr(:)
  double precision, save :: bnorm, zrnorm, rho, gamma, nu

1000 format(5(a,i10)/)
2000 format(5(a,f10.4)/)
  
  interface
     subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
       use m_primapr
       implicit none
       integer, intent(in) :: nd, stat, end
       type(primail), intent(in) :: aprl(nd)
       double precision, intent(in) :: zv(nd)
       double precision, intent(out) :: zav(nd)
     end subroutine adotv_ail
  end interface

  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1
  num_threads = 1
  !$ num_threads = omp_get_num_threads()

  if(me_omp == 1) then
     if(amcc%param(9) /= amcc%param(10)) write(*, *) 'Check param(9) and param(10). This is in the cg_ail routine.'
     if(amcc%param(11) /= amcc%param(12)) write(*, *) 'Check param(11) and param(12). This is in the cg_ail routine.'
  endif

  ! tmp_i = mod(nd, num_threads)
  ! if(me_omp <= tmp_i) then
  !    istat = ((nd-tmp_i)/num_threads+1) * (me_omp-1) + 1
  !    iend = ((nd-tmp_i)/num_threads+1) * me_omp
  ! else
  !    istat = ((nd-tmp_i)/num_threads) * (me_omp-1) + 1 + tmp_i
  !    iend = ((nd-tmp_i)/num_threads) * me_omp + tmp_i
  ! endif
  ! t_amg = 0.0d0
  ! t_oth = 0.0d0

  ! t_oth = omp_get_wtime()

  istat = 1
  iend = amcc%Apr(1)%nd

  ! write(*, *) 'Check', me_omp, num_threads, nd, istat, iend
  ! stop

  if(me_omp == 1) then
     print*,'cg_ail_seq start'
     allocate(zr(nd),zp(nd),zq(nd))
  endif
  !$OMP barrier
  ! alpha = 0.0;  beta = 0.0

  ! t_adotv = omp_get_wtime()
  call adotv_ail(zr,amcc%Apr(1)%aprl,u,nd,istat,iend)
  ! t_adotv = omp_get_wtime() - t_adotv

  zr(istat:iend)=b(istat:iend)-zr(istat:iend)
  tmp_d = dotp_d(nd, zr, zr, istat, iend)
  if(me_omp == 1) bnorm = 0.0d0
  !$OMP barrier
  !$OMP atomic
  bnorm = bnorm + tmp_d
  !$OMP barrier
  if(me_omp == 1) then
     bnorm=dsqrt(bnorm)
     zrnorm = bnorm
  endif
  !$OMP barrier

  zp(istat:iend) = 0.0d0
  if(me_omp == 1) then
     amcc%uc(1)%v => zp(1:nd)
     amcc%bc(1)%v => zr(1:nd)
  endif
  !$OMP barrier
  ! t_amg = omp_get_wtime()
  call vcycle_symmetric_seq(amcc, time)
  ! t_amg = omp_get_wtime() - t_amg
  
  if(me_omp == 1) rho = 0.0d0
  !$OMP barrier
  tmp_d = dotp_d(nd,zr,zp,istat,iend)
  !$OMP atomic
  rho = rho + tmp_d
  !$OMP barrier

  if(me_omp == 1) amcc%uc(1)%v => zq(1:nd)
     
  do in=1,mstep
     if(me_omp == 1) write(*, *) 'CG convergence', in, zrnorm/bnorm
     if(zrnorm/bnorm<eps) exit

     ! t_adotv = t_adotv - omp_get_wtime()
     call adotv_ail(zq,amcc%Apr(1)%aprl,zp,nd,istat,iend)
     ! t_adotv = t_adotv + omp_get_wtime()
     gamma = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd,zp,zq,istat,iend)
     !$OMP atomic
     gamma = gamma + tmp_d
     !$OMP barrier
     alpha = rho / gamma

     u(istat:iend)=u(istat:iend)+alpha*zp(istat:iend)
     zr(istat:iend)=zr(istat:iend)-alpha*zq(istat:iend)
     zrnorm = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd, zr, zr, istat, iend)
     !$OMP atomic
     zrnorm = zrnorm + tmp_d
     !$OMP barrier
     if(me_omp == 1) zrnorm = dsqrt(zrnorm)

     zq(istat:iend) = 0.0d0
     !$OMP barrier
     ! amcc%bc(1)%v => zr(1:nd)
     ! t_amg = t_amg - omp_get_wtime()
     call vcycle_symmetric_seq(amcc, time)     
     ! t_amg = t_amg + omp_get_wtime()

     nu = 0.0d0
     !$OMP barrier
     tmp_d = dotp_d(nd,zq,zr,istat,iend)
     !$OMP atomic
     nu = nu + tmp_d
     !$OMP barrier
     beta = nu / rho
     !$OMP barrier
     if(me_omp == 1) rho = nu

     zp(istat:iend) =zq(istat:iend)+beta*zp(istat:iend)
     !$OMP barrier
  enddo

  ! t_oth = omp_get_wtime() - t_oth 
  ! write(*, *) 'Time amg, oth and adotv', t_amg, t_oth-t_amg, t_adotv

endsubroutine cg_ail_seq

! !***gcr_ail
! subroutine gcr_ail(u,aprl,b,amcc,eps,nd,mstep)
!   use m_primapr
!   implicit none
!   integer, intent(in) :: nd, mstep
!   type(primail), intent(in) :: aprl(nd)
!   double precision, intent(in) :: b(nd)
!   double precision, intent(inout) :: u(nd)
!   type(primaprccs), intent(inout) :: amcc
!   double precision, intent(in) :: eps
!   integer in, il
!   double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
!   double precision, allocatable :: zar(:), capap(:)
!   double precision, allocatable, target :: zp(:, :), zap(:, :), zr(:), zkr(:)
! 1000 format(5(a,i10)/)
! 2000 format(5(a,f10.4)/)

!   interface
!      subroutine adotv_ail(zav,aprl,zv,nd)
!        use m_primapr
!        implicit none
!        integer, intent(in) :: nd
!        type(primail), intent(in) :: aprl(:)
!        double precision, intent(in) :: zv(:)
!        double precision, intent(out) :: zav(:)
!      end subroutine adotv_ail
!   end interface

!   print*,'gcr_ail start'
!   allocate(zr(nd),zar(nd),zkr(nd),zp(nd,mstep),zap(nd,mstep),capap(mstep))
!   alpha = 0.0
!   zz=dotp_d(nd, b, b); bnorm=dsqrt(zz);
!   call adotv_ail(zar,aprl,u,nd)
!   zr(:nd)=b(:nd)-zar(:nd)
! !!! proconditioning point
!   ! zp(:nd,1)=zr(:nd)

!   zp(1:nd, 1) = 0.0d0
!   amcc%uc(1)%v => zp(1:nd, 1)
!   amcc%bc(1)%v => zr(1:nd)
!   call vcycle(amcc)
!   ! call vcycle_symmetric(amcc)

!   zrnorm2=dotp_d(nd,zr,zr); zrnorm=dsqrt(zrnorm2)
!   print*,0,zrnorm/bnorm
!   if(zrnorm/bnorm<eps) return
!   call adotv_ail(zap(1:nd,1),aprl,zp(1:nd,1),nd)
!   ! write(*, *) 'Check0', sum(zap(1:nd, 1))
!   do in=1,mstep
!      ! zq(1:nd)=>zap(1:nd,in)
!      znom=dotp_d(nd,zap(1:nd,in),zr); capap(in)=dotp_d(nd,zap(1:nd,in),zap(1:nd,in))
!      ! write(*, *) 'Check1'
!      alpha=znom/capap(in)
!      u(1:nd)=u(1:nd)+alpha*zp(1:nd,in)
!      ! write(*, *) 'Check1'
!      ! zr(1:nd)=zr(1:nd)-alpha*zap(1:nd,in)
!      zr(1:nd)=zr(1:nd)-alpha*zap(1:nd, in)
!      ! write(*, *) 'Check2'
!      zrnomold=zrnorm2
!      zrnorm2=dotp_d(nd,zr,zr); zrnorm=dsqrt(zrnorm2)
!      print*,in,zrnorm/bnorm
!      if(zrnorm/bnorm<eps .or. in==mstep) exit

! !!! proconditioning point
!      ! call adotv_ail(zar,aprl,zr,nd)
!      ! zp(:nd,in+1)=zr(:nd)
!      ! zap(:nd,in+1)=zar(:nd)
!      zkr(1:nd) = 0.0d0
!      amcc%uc(1)%v => zkr(1:nd)
!      amcc%bc(1)%v => zr(1:nd)
!      call vcycle(amcc)
!      ! call vcycle_symmetric(amcc)

!      ! write(*, *) 'Check3'
!      call adotv_ail(zar,aprl,zkr,nd)
!      zp(1:nd,in+1)=zkr(1:nd)
!      zap(1:nd,in+1)=zar(1:nd)
!      ! write(*, *) 'Check1', sum(zr(1:nd)), sum(zkr(1:nd))

!      ! write(*, *) 'Check4'
!      do il=1,in
!         !zq=>zap(:nd,il)
!         znom=dotp_d(nd,zap(1:nd,in),zar)
!         beta=-znom/capap(il)
!         zp(1:nd,in+1) =zp(1:nd,in+1)+beta*zp(1:nd,il)
!         ! zap(:nd,in+1)=zap(:nd,in+1)+beta*zq(:nd)
!         zap(1:nd,in+1)=zap(1:nd,in+1)+beta*zap(1:nd, il)
!      enddo
!      ! write(*, *) 'Check5'
!   enddo
! endsubroutine gcr_ail

! !***gs_gcr_ail
! subroutine gs_gcr_ail(u,aprl,b,eps,nd,mstep)
!   use m_primapr
!   implicit none
!   integer, intent(in) :: nd, mstep
!   type(primail), intent(in) :: aprl(nd)
!   double precision, intent(in) :: b(nd)
!   double precision, intent(inout) :: u(nd)
!   double precision, intent(in) :: eps
!   integer in, il, nitr
!   double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
!   double precision, allocatable :: zr(:), zar(:), capap(:), zp(:, :), zkr(:)
!   double precision, allocatable, target :: zap(:, :)
!   double precision, pointer :: zq(:)
! 1000 format(5(a,i10)/)
! 2000 format(5(a,f10.4)/)

!   interface
!      subroutine adotv_ail(zav,aprl,zv,nd)
!        use m_primapr
!        implicit none
!        integer, intent(in) :: nd
!        type(primail), intent(in) :: aprl(:)
!        double precision, intent(in) :: zv(:)
!        double precision, intent(out) :: zav(:)
!      end subroutine adotv_ail
!   end interface

!   print*,'gcr_ail start'
!   allocate(zr(nd),zar(nd),zkr(nd),zp(nd,mstep),zap(nd,mstep),capap(mstep))
!   nitr=5
!   alpha = 0.0
!   zz=dotp_d(nd, b, b); bnorm=dsqrt(zz);
!   call adotv_ail(zar,aprl,u,nd)
!   zr(:nd)=b(:nd)-zar(:nd)
!   zp(:nd,1)=0.0d0; call gs_ail(zp(:nd,1),aprl,zr,nd,nitr)
!   zrnorm2=dotp_d(nd,zr,zr); zrnorm=dsqrt(zrnorm2)
!   print*,0,zrnorm/bnorm
!   if(zrnorm/bnorm<eps) return
!   call adotv_ail(zap(1:nd,1),aprl,zp(1:nd,1),nd)
!   do in=1,mstep
!      ! write(*, *) 'Check1'
!      zq=>zap(:nd,in)
!      znom=dotp_d(nd,zq,zr); capap(in)=dotp_d(nd,zq,zq)
!      alpha=znom/capap(in)
!      u(:nd)=u(:nd)+alpha*zp(:nd,in)
!      zr(:nd)=zr(:nd)-alpha*zq(:nd)
!      zrnomold=zrnorm2
!      zrnorm2=dotp_d(nd,zr,zr); zrnorm=dsqrt(zrnorm2)
!      print*,in,zrnorm/bnorm
!      if(zrnorm/bnorm<eps .or. in==mstep) then
!         write(*, *) 'Converged', in, zrnorm/bnorm
!         exit      
!      endif
!      ! write(*, *) 'Check2'
!      zkr(:nd)=0.0d0; call gs_ail(zkr,aprl,zr,nd,nitr)
!      ! write(*, *) 'Check1', sum(zr(1:nd)), sum(zkr(1:nd))
!      call adotv_ail(zar,aprl,zkr,nd)
!      zp(:nd,in+1)=zkr(:nd)
!      zap(:nd,in+1)=zar(:nd)
!      ! write(*, *) 'Check3'
!      do il=1,in
!         ! write(*, *) 'Check4', il
!         zq=>zap(:nd,il)
!         znom=dotp_d(nd,zq,zar)
!         beta=-znom/capap(il)
!         ! write(*, *) 'Check5', il
!         zp(1:nd,in+1) =zp(1:nd,in+1)+beta*zp(1:nd,il)
!         ! write(*, *) 'Check6', il
!         ! zap(1:nd,in+1)=zap(1:nd,in+1)+beta*zq(1:nd)
!         zap(1:nd,in+1)=zap(1:nd,in+1)+beta*zap(1:nd, il)
!         ! write(*, *) 'Check7', il
!      enddo
!   enddo
! endsubroutine gs_gcr_ail

!***bicgstab_ail
subroutine bicgstab_ail_seq(u,aprl,b,eps,nd,mstep,time)
  use m_primapr
  implicit none
  integer, intent(in) :: nd, mstep
  type(primail) aprl(nd)
  double precision :: u(nd),b(nd)
  double precision,dimension(:),allocatable :: zr,zshdw,zp,zt,zkp,zakp,zkt,zakt
  double precision, intent(in) :: eps
  double precision, intent(out) :: time

  integer in, il, istat, iend, clr, blc, tmp_i
  integer :: mpinr = 0
  ! double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
  ! double precision, allocatable :: zar(:), capap(:)
  double precision alpha, beta, tmp_d, t_amg, t_oth, t_adotv
  double precision bnorm, zden, zeta, znom, znomold, znorm, zrnorm, zz
  integer me_omp, num_threads, rem, num

  interface
     subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
       use m_primapr
       implicit none
       integer, intent(in) :: nd, stat, end
       type(primail), intent(in) :: aprl(nd)
       double precision, intent(in) :: zv(nd)
       double precision, intent(out) :: zav(nd)
     end subroutine adotv_ail
  end interface

1000 format(5(a,i10)/)
2000 format(5(a,f10.4)/)
  allocate(zr(nd),zshdw(nd),zp(nd),zt(nd),zkp(nd),zakp(nd),zkt(nd),zakt(nd))

  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1
  num_threads = 1
  !$ num_threads = omp_get_num_threads()

  if(mpinr == 1) print*,'bicgstab_ail_seq start'

  call init_bound(nd, num_threads, me_omp, istat, iend)
  
  if(me_omp == 1) then
     alpha = 0.0;  beta = 0.0;  zeta = 0.0;
     zz=dotp_d(nd,b,b,1,nd); bnorm=dsqrt(zz);
     zr(:nd)=b(:nd)
     call adotsub_ail(zr,aprl,u,nd,1,nd)
     zshdw(:nd)=zr(:nd)
     zrnorm=dotp_d(nd,zr,zr,1,nd); zrnorm=dsqrt(zrnorm)
     if(zrnorm/bnorm<eps) return
     write(*, *) 'Check 1', bnorm, zrnorm
     do in=1,mstep
        zp(:nd) =zr(:nd)+beta*(zp(:nd)-zeta*zakp(:nd))
        zkp(:nd)=zp(:nd)
        call adotv_ail(zakp,aprl,zkp,nd,1,nd)
        znom=dotp_d(nd,zshdw,zr,1,nd); zden=dotp_d(nd,zshdw,zakp,1,nd);
        alpha=znom/zden; znomold=znom;
        write(*, *) 'Check 2', znom, zden, alpha
        zt(:nd)=zr(:nd)-alpha*zakp(:nd)
        zkt(:nd)=zt(:nd)
        call adotv_ail(zakt,aprl,zkt,nd,1,nd)
        znom=dotp_d(nd,zakt,zt,1,nd); zden=dotp_d(nd,zakt,zakt,1,nd);
        zeta=znom/zden;
        write(*, *) 'Check 3', znom, zden, zeta
        u(:nd)=u(:nd)+alpha*zkp(:nd)+zeta*zkt(:nd)
        zr(:nd)=zt(:nd)-zeta*zakt(:nd)
        beta=alpha/zeta*dotp_d(nd,zshdw,zr,1,nd)/znomold;
        zrnorm=dotp_d(nd,zr,zr,1,nd); zrnorm=dsqrt(zrnorm)
        write(*, *) 'Check 4', beta, alpha, zeta, dotp_d(nd,zshdw,zr,1,nd), zrnorm
        if(mpinr==0) print*,in,zrnorm/bnorm
        if(zrnorm/bnorm<eps) exit
     enddo
  endif
endsubroutine bicgstab_ail_seq

!***bicgstab_ail
subroutine bicgstab_ail(u,aprl,b,eps,nd,mstep,time)
  use m_primapr
  implicit none
  integer, intent(in) :: nd, mstep
  type(primail) aprl(nd)
  double precision :: u(nd),b(nd)
  ! double precision,dimension(:),allocatable :: zr,zshdw,zp,zt,zkp,zakp,zkt,zakt
  double precision, intent(in) :: eps
  double precision, intent(out) :: time

  integer in, il, istat, iend, clr, blc, tmp_i
  integer :: mpinr = 0
  ! double precision zz, znom, zrnorm, zrnorm2, alpha, beta, bnorm, zrnomold
  ! double precision, allocatable :: zar(:), capap(:)
  double precision alpha, beta, zeta, tmp_d, t_amg, t_oth, t_adotv, tmp_d1, tmp_d2, znomold, zrnorm
  ! double precision bnorm, zden, zeta, znom, znomold, znorm, zrnorm, zz
  integer me_omp, num_threads

  double precision, allocatable, target, save :: zp(:), zt(:), zkp(:), zakp(:), zkt(:), zr(:), zshdw(:), zakt(:)
  double precision, save :: bnorm, zden, znom, znorm, zrnorm_p, zz, val_s
  
  interface
     subroutine adotv_ail(zav,aprl,zv,nd,stat,end)
       use m_primapr
       implicit none
       integer, intent(in) :: nd, stat, end
       type(primail), intent(in) :: aprl(nd)
       double precision, intent(in) :: zv(nd)
       double precision, intent(out) :: zav(nd)
     end subroutine adotv_ail
  end interface

1000 format(5(a,i10)/)
2000 format(5(a,f10.4)/)

  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1
  num_threads = 1
  !$ num_threads = omp_get_num_threads()

  if(me_omp == 1) print*,'bicgstab_ail start'

  call init_bound(nd, num_threads, me_omp, istat, iend)

!$omp master
  allocate(zr(nd),zshdw(nd),zp(nd),zt(nd),zkp(nd),zakp(nd),zkt(nd),zakt(nd))
  zz = 0.0d0
  zrnorm_p = 0.0d0
  znom = 0.0d0
  zden = 0.0d0
!$omp end master
!$omp barrier

  time = omp_get_wtime()
  
  alpha = 0.0;  beta = 0.0;  zeta = 0.0;
  tmp_d=dotp_d(nd,b,b,istat,iend)
!$omp atomic
  zz = zz + tmp_d
!$omp barrier  
  if(me_omp == 1) bnorm=dsqrt(zz)
  zr(istat:iend)=b(istat:iend)
  call adotsub_ail(zr,aprl,u,nd,istat,iend)
  zshdw(istat:iend)=zr(istat:iend)
  tmp_d1 = dotp_d(nd,zr,zr,istat,iend)
!$omp atomic
  zrnorm_p = zrnorm_p + tmp_d1
!$omp barrier
  zrnorm = dsqrt(zrnorm_p)
!$omp barrier  
  if(zrnorm/bnorm<eps) return
  ! if(me_omp == 1) write(*, *) 'Check 1', bnorm, zrnorm
  do in=1,mstep
     zp(istat:iend) =zr(istat:iend)+beta*(zp(istat:iend)-zeta*zakp(istat:iend))
     zkp(istat:iend)=zp(istat:iend)
!$omp barrier
     call adotv_ail(zakp,aprl,zkp,nd,istat,iend)
     tmp_d1=dotp_d(nd,zshdw,zr,istat,iend)
     tmp_d2=dotp_d(nd,zshdw,zakp,istat,iend)
!$omp atomic
     znom = znom + tmp_d1
!$omp atomic
     zden = zden + tmp_d2
!$omp barrier
     alpha=znom/zden
     if(me_omp == 1) write(*, *) 'Check 2', znom, zden, alpha
     znomold=znom
     zt(istat:iend)=zr(istat:iend)-alpha*zakp(istat:iend)
     zkt(istat:iend)=zt(istat:iend)
     if(me_omp == 1) then
        znom = 0.0d0
        zden = 0.0d0
        val_s = 0.0d0
        zrnorm_p = 0.0d0
     endif
!$omp barrier
     call adotv_ail(zakt,aprl,zkt,nd,istat,iend)
     tmp_d1=dotp_d(nd,zakt,zt,istat,iend)
     tmp_d2=dotp_d(nd,zakt,zakt,istat,iend)
!$omp atomic
     znom = znom + tmp_d1
!$omp atomic
     zden = zden + tmp_d2
!$omp barrier
     zeta=znom/zden;
     ! if(me_omp == 1) write(*, *) 'Check 3', znom, zden, zeta, alpha
     u(istat:iend)=u(istat:iend)+alpha*zkp(istat:iend)+zeta*zkt(istat:iend)
     zr(istat:iend)=zt(istat:iend)-zeta*zakt(istat:iend)
     tmp_d1 = dotp_d(nd,zshdw,zr,istat,iend)
     tmp_d2 = dotp_d(nd,zr,zr,istat,iend)
!$omp atomic
     val_s = val_s + tmp_d1
!$omp atomic
     zrnorm_p = zrnorm_p + tmp_d2
!$omp barrier    
     beta=alpha/zeta*val_s/znomold;
     zrnorm=dsqrt(zrnorm_p)
     if(me_omp == 1) write(*, *) 'Check 4', beta, alpha, zeta, val_s, znomold, zrnorm, znom, zden
     ! write(*, *) 'Check 5', me_omp, tmp_d1, dotp_d(nd,zshdw,zshdw,istat,iend), dotp_d(nd,zr,zr,istat,iend)
     if(me_omp == 1) then
        print*,in,zrnorm/bnorm
        znom = 0.0d0
        zden = 0.0d0
     endif
!$omp barrier    
     if(zrnorm/bnorm<eps) exit
  enddo

  time = omp_get_wtime() - time
  
end subroutine bicgstab_ail

! !***gs_bicgstab_ail
! subroutine gs_bicgstab_ail(u,aprl,b,eps,nd,mstep)
!   use m_primapr
!   implicit none
!   type(primail) aprl(:)
!   double precision :: u(:),b(:)
!   double precision,dimension(:),allocatable :: zr,zshdw,zp,zt,zkp,zakp,zkt,zakt
! 1000 format(5(a,i10)/)
! 2000 format(5(a,f10.4)/)
!   if(mpinr==0) print*,'gs_bicgstab_ail start'
!   allocate(zr(nd),zshdw(nd),zp(nd),zt(nd),zkp(nd),zakp(nd),zkt(nd),zakt(nd))
!   alpha = 0.0;  beta = 0.0;  zeta = 0.0;
!   zz=dotp_d(nd, b, b); bnorm=dsqrt(zz);
!   zr(:nd)=b(:nd)
!   call adotsub_ail(zr,aprl,u,nd)
!   zshdw(:nd)=zr(:nd)
!   zrnorm=dotp_d(nd,zr,zr); zrnorm=dsqrt(zrnorm)
!   if(zrnorm/bnorm<eps) return
!   nitr=5
!   do in=1,mstep
!      zp(:nd) =zr(:nd)+beta*(zp(:nd)-zeta*zakp(:nd))
!      zkp(:)=0.0d0; call gs_ail(zkp,aprl,zp,nd,nitr)
!      call adotv_ail(zakp,aprl,zkp,nd)
!      znom=dotp_d(nd,zshdw,zr); zden=dotp_d(nd,zshdw,zakp);
!      alpha=znom/zden; znomold=znom;
!      zt(:nd)=zr(:nd)-alpha*zakp(:nd)
!      zkt(:)=0.0d0; call gs_ail(zkt,aprl,zt,nd,nitr)
!      call adotv_ail(zakt,aprl,zkt,nd)
!      znom=dotp_d(nd,zakt,zt); zden=dotp_d(nd,zakt,zakt);
!      zeta=znom/zden;
!      u(:nd)=u(:nd)+alpha*zkp(:nd)+zeta*zkt(:nd)
!      zr(:nd)=zt(:nd)-zeta*zakt(:nd)
!      beta=alpha/zeta*dotp_d(nd,zshdw,zr)/znomold;
!      zrnorm=dotp_d(nd,zr,zr); zrnorm=dsqrt(zrnorm)
!      if(mpinr==0) print*,in,zrnorm/bnorm
!      if(zrnorm/bnorm<eps) exit
!   enddo
! endsubroutine gs_bicgstab_ail

