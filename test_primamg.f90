Program test_primamg
 use m_primapr
 ! use m_primamg
 implicit real*8(a-h,o-z)
 
!*** type :: acrs
type :: acrs
 integer nd
 real*8,pointer :: aval(:)=>null()
 real*8,pointer :: bval(:)=>null()
 integer,pointer :: lrow_ptr(:)=>null()
 integer,pointer :: lcol_ind(:)=>null()
end type acrs

!*** main
 type(acrs) amcrs
 type(primapr) amcf
 type(primaprccs) :: amcc
 real*8,dimension(:),pointer :: zx,res,zb
 character*32 logfile
 character*256 input
 double precision, allocatable :: time(:)
 
 interface
    subroutine first_touch(amcc,lcol_ind,aval)
      use m_primapr
      type(primaprccs), intent(inout) :: amcc
      integer, pointer :: lcol_ind(:)
      double precision, pointer :: aval(:)
    end subroutine first_touch

    subroutine read_original_mm(Aval, col_ind, row_ptr, Bval, nd)
      integer, intent(in) :: nd
      integer, pointer, intent(out) :: col_ind(:), row_ptr(:)
      real(8), pointer, intent(inout) :: Aval(:), Bval(:)
    end subroutine read_original_mm

 end interface

 allocate(amcc%param(amcc%num_params))

 amcc%param(1)  = 100 !Number of grid-points on coarsest grid.
 amcc%param(2)  = 10  !Maximum level for AMG
 ! amcc%param(3)  = 0.2d-0 !Parameter for aggregation1
 ! amcc%param(4)  = 0.8d0  !Parameter for aggregation2. Condition of strong-connect is param(3) * param(4)**(ig-1). ig is the level.
 amcc%param(3)  = 2.0d-1  !Parameter for aggregation.
 amcc%param(4)  = 8.0d-1  !Parameter for aggregation. Condition of strong-connect is param(3) * param(4)**(ig-1). ig is the level.
 ! amcc%param(3)  = 0.8d-1 !Parameter for aggregation.
 ! amcc%param(4)  = 0.5d0  !Parameter for aggregation. Condition of strong-connect is param(3) * param(4)**(ig-1). ig is the level.
 ! amcc%param(3)  = 0.5d-2 !Parameter for aggregation.
 ! amcc%param(4)  = 0.2d0  !Parameter for aggregation. Condition of strong-connect is param(3) * param(4)**(ig-1). ig is the level.
 amcc%param(5)  = 2.0d0/3.0d0 !Parameter for weighted-Jacobi. This is used at making restriction matrix.
 amcc%param(6)  = 1.0d0 !Parameter for making prolongation matrix. "Prolongation matrix" = param(6) * "transposed restriction matrix".
 amcc%param(7)  = 1 !Maximum number of iteration for Preconditioning(AMG).
 amcc%param(8)  = 1.0d-7 !Threshold of relative residual norm.
 amcc%param(9)  = 1 !Number of iteration for pre-smoothing.
 amcc%param(10) = 1 !Number of iteration for post-smoothing.
 amcc%param(11) = 1 !Number of iteration for inner pre-smoothing(mBMC).
 amcc%param(12) = 1 !Number of iteration for inner post-smoothing(mBMC).
 amcc%param(13) = 100000 !Maximum number of iteration for coarsest-sover.
 amcc%param(14) = 1.0d-9 !Threshold of relative residual norm for coarsest-sover.
 amcc%param(15) = 256.0d3 !L2 cache size
 amcc%param(16) = 32.0d3 !L1 cache size
 amcc%param(17) = 0.80d0 !Deciminal of cache size for using modified block multi-color.
 amcc%param(18) = 1.0d0 !Using dense matrix solver on the coarasest grid. 1:yes, 0:no
 amcc%param(19) = 2.0d0 !Choosing solver 1.0d0:CG 2.0d0:MGCG 3.0d0:BiCGStab
 amcc%param(20) = 0.0d0 !0.0d0 Using parameters 3 and 4, 1.0d0:Dynamic parameter for aggregation
 amcc%param(21) = 30.0d-2 !Threshold for dynamic parameter of aggregation.
 
 nargs=command_argument_count()
 if(nargs >= 5) then
    if(nargs == 5) then
       ival = 4
    elseif(nargs == 6) then
       ival = 5
    endif
    call get_command_argument(ival, input)
    read(input ,*) amcc%param(3)
    call get_command_argument(ival+1, input)
    read(input ,*) amcc%param(4)
 endif
 write(*, *) 'Check aggregation params', amcc%param(3:4)
 
 call get_command_argument(2, input)
 
 if(input(1:1) == "0") then
    call read_original_mm(amcrs%aval, amcrs%lcol_ind, amcrs%lrow_ptr, amcrs%bval, amcrs%nd)
 else
    call read_data_matmarcket2crs(amcrs)
 endif
 
 nd=amcrs%nd; print*,'nd=',nd
 zz=sum(amcrs%bval(:nd)*amcrs%bval(:nd)); bnorm=dsqrt(zz)
 print*,'bnorm=',bnorm
 call crs2apr(amcrs,amcf)

 call get_command_argument(1, input)
 read(input ,*) amcc%param(11)
 amcc%param(12) = amcc%param(11)

 !$OMP parallel default(none) shared(amcrs,amcf,amcc,zx,eps,nitr,zb) firstprivate(nd, me_omp) private(t_stat, t_end, i, time)

 me_omp = 1
 !$ me_omp = omp_get_thread_num() + 1

 if(int(amcc%param(19)) == 2) then
    !$OMP master
    call construct_corse_matrices(amcf,amcc)
    ! time = 0.0d0
    !$OMP end master
    !$OMP barrier
    
    call first_touch2(amcc,amcrs%lcol_ind,amcrs%aval)
    !$OMP barrier

    ! if(me_omp == 1) then
    !    allocate(zb(nd))
    !    do i = 1, nd
    !       zb(i) = amcrs%bval(i)
    !    enddo
    !    amcc%uc(1)%v(1:nd) => zx (1:nd)
    !    amcc%bc(1)%v(1:nd) => zb (1:nd)
    ! endif
    ! else
 endif
 if(me_omp == 1) then
    allocate(zx(nd))
    zx(1:nd)=0.0d0
 endif
 ! endif
 !$OMP barrier

 allocate(time(amcc%nm+7))
 time = 0.0d0
 
 !$OMP barrier
 if(me_omp == 1)t_stat = omp_get_wtime()
 ! nitr=1000; call gs_ail(zx,amcf%aprl,amcrs%bval,nd,nitr)
 ! eps=1.0e-6; nitr=100; call gs_bicgstab_ail(zx,amcf%aprl,amcrs%bval,eps,nd,nitr)
 ! eps=1.0e-4; nitr=100; call gcr_ail(zx,amcf%aprl,amcrs%bval,amcc,eps,nd,nitr)
 ! eps=1.0e-4; nitr=200; call gs_gcr_ail(zx,amcf%aprl,amcrs%bval,eps,nd,nitr)
 if(int(amcc%param(19)) == 1) then
    eps=amcc%param(8); nitr=100000; call cg_ail(zx,amcf%aprl,amcrs%bval,eps,nd,nitr)
 elseif(int(amcc%param(19)) == 2) then
    eps=amcc%param(8); nitr=200; call mgcg_ail(zx,amcf%aprl,amcrs%bval,amcc,eps,nd,nitr,time) !MGCG
 elseif(int(amcc%param(19)) == 3) then
    eps=amcc%param(8); nitr=20; call bicgstab_ail(zx,amcf%aprl,amcrs%bval,eps,nd,nitr,time(1))
 endif
 ! call vcycle_symmetric(amcc, time)
 ! !$OMP master
 ! eps=1.0e-7; nitr=200; call cg_ail_seq(zx,amcf%aprl,amcrs%bval,amcc,eps,nd,nitr,time)
 ! !$OMP end master
 !$OMP barrier
 if(int(amcc%param(19)) == 2) then
 if(me_omp == 1)then
    t_end = omp_get_wtime()
    write(*, *) 'Time of AMG-CG is ', t_end - t_stat
    write(*, '(a45, f10.4, a48, f10.4)') 'Time of first-time smoothing on 1st stage is ', time(1), '. Time of the other smoothings on 1st stage is ', time(2)-time(1)
    do i = 1, amcc%nm
       write(*, *) 'Time of ', i, 'th stage is ', time(2+i)
    enddo
    write(*, *) 'Time of the other part in AMG is ', time(amcc%nm+7)-sum(time(3:amcc%nm+2))
    write(*, *) 'Time of cg part is ', t_end-t_stat - time(amcc%nm+7)
 endif
 write(*, *) 'Wait time of thread ', me_omp, time(3)-time(amcc%nm+3)-time(amcc%nm+4)-time(amcc%nm+5)-time(amcc%nm+6)
 write(*, *) 'Time of all amg step : me_omp, smg, residual, restriction, prolongation', me_omp, time(amcc%nm+3), time(amcc%nm+4), time(amcc%nm+5), time(amcc%nm+6)
 endif
 
 !$OMP end parallel
 
 allocate(res(nd))
 do i = 1, nd
    res(i)=amcrs%bval(i)
 enddo

 call adotsub_ail(res,amcf%aprl,zx,nd,1,nd)
 zrnorm=sum(res(1:nd)*res(1:nd)); zrnorm=dsqrt(zrnorm)
 print*,'zrnorm=',zrnorm
 print*,'log10(zrnorm/bnorm)=',log10(zrnorm/bnorm)
 print*,'zrnorm/bnorm=',zrnorm/bnorm

 ! amcc%bc(1)%v(1:nd) => amcrs%bval(1:nd)
 ! allocate(amcc%uc(1)%v(1:nd))
 ! amcc%uc(1)%v = 0.0d0
 ! call vcycle(amcc)
 
contains
!***crs2apr
subroutine crs2apr(amcrs,amc)
 type(acrs) amcrs
 type(primapr) amc
 
 nd=amcrs%nd; amc%nd=nd
 allocate(amc%aprl(nd))
 do il=1,nd
   amc%aprl(il)%ndl=amcrs%lrow_ptr(il+1)-amcrs%lrow_ptr(il)
   amc%aprl(il)%avl=>amcrs%aval(amcrs%lrow_ptr(il):amcrs%lrow_ptr(il+1)-1)
   amc%aprl(il)%lct=>amcrs%lcol_ind(amcrs%lrow_ptr(il):amcrs%lrow_ptr(il+1)-1)
   it=amc%aprl(il)%lct(1)
   if(il/=it)then
     do itt=2,amc%aprl(il)%ndl; it=amc%aprl(il)%lct(itt)
       if(il==it)then
         ztmp=amc%aprl(il)%avl(itt); amc%aprl(il)%avl(itt)=amc%aprl(il)%avl(1); amc%aprl(il)%avl(1)=ztmp
         amc%aprl(il)%lct(itt)=amc%aprl(il)%lct(1); amc%aprl(il)%lct(1)=il
       endif
     enddo
   endif
   amc%aprl(il)%ado=1.0d0/amc%aprl(il)%avl(1)
 enddo
 amc%nonzero = amcrs%lrow_ptr(nd+1)-1
 return
 print*,amc%nd
 print*,amc%aprl(nd)%ndl
 print*,amc%aprl(nd)%avl
 print*,amc%aprl(nd)%lct
 print*,amc%aprl(nd)%ado
 print*,amc%aprl(25)%ndl
 print*,amc%aprl(25)%avl
 print*,amc%aprl(25)%lct
 print*,amc%aprl(25)%ado

endsubroutine

!***read_data_matmarcket2crs
subroutine read_data_matmarcket2crs(amc)
 type(acrs) amc
 real*8,allocatable :: za(:)
 integer,allocatable :: lnd(:),ll(:),lt(:),lpt(:)
 character*256 value
 character(10000) :: oneline
 
 nargs=command_argument_count()
 write(*, *) nargs
 iunit = 10
 if(nargs < 3 .and. 7 < nargs)then
    ! open(iunit, file =  "input.txt", action = 'read', pad = 'yes', iostat = ierr )
    write(*, *) 'Error. Number of arguments is many or less.  Please input number of iteration of smoothing, type(symmetric or asymmetric), matrixdata and right-hand vector data(If this is separated from matrixdata).'
    stop
 else
   number=2
   call get_command_argument(number,value,nlength,nstatus)
   value=trim(value)
   if(value == 'symmetric') then
      iflag = 1
   elseif(value == 'asymmetric') then
      iflag = 2
   else
      write(*, *) 'Second argument is wrong. Please input "symmetric" or "asymmetric".'
      stop
   endif
   number=3
   call get_command_argument(number,value,nlength,nstatus)
   value=trim(value)
   open(iunit, file = value, action = 'read', pad = 'yes', iostat = ierr )

   if(nargs == 4 .or. nargs == 6) then
      iright = 4
   else
      iright = 0
   endif
 endif
 if(ierr.ne.0) then
   print*, 'input.txt does not exists'
 endif

  do while(.true.)
     read(iunit, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo
 
 read(oneline, *) amc%nd,nd,nss
 ns=2*nss-nd
 print*,amc%nd,nd,nss,ns
 allocate(za(nss),ll(nss),lt(nss),lnd(nd),lpt(nd+1))
 allocate(amc%aval(ns),amc%lcol_ind(ns),amc%lrow_ptr(nd+1),amc%bval(nd))
    
 if(iflag == 1) then
    lnd(:)=0
    do is=1,nss
       read(iunit,*) lt(is),ll(is),za(is)
       lnd(ll(is))=lnd(ll(is))+1
       if(lt(is)==ll(is)) cycle
       lnd(lt(is))=lnd(lt(is))+1
    enddo
    if(iright /= 0) close(iunit)
    
    amc%lrow_ptr(1)=1; lpt(1)=1
    do il=1,nd
       amc%lrow_ptr(il+1)=amc%lrow_ptr(il)+lnd(il)
       lpt(il+1)=lpt(il)+lnd(il)
    enddo
    if(amc%lrow_ptr(nd+1)/=ns+1)then
       print*,'ERROR!!; read_data_matmarcket2crs_1'
       stop
    endif
    do is=1,nss
       il=ll(is); it=lt(is)
       amc%aval(lpt(il))=za(is); amc%lcol_ind(lpt(il))=it
       lpt(il)=lpt(il)+1
       if(il==it) cycle
       amc%aval(lpt(it))=za(is); amc%lcol_ind(lpt(it))=il
       lpt(it)=lpt(it)+1
    enddo
    print*,'lpt(it)=',lpt(it)
    deallocate(za,ll,lt,lnd)
 else
    lnd(:) = 0
    ! icount = 1
    do is=1,nss
       read(iunit,*) lt(is),ll(is),za(is)
       lnd(lt(is))=lnd(lt(is))+1
       ! read(iunit,*) lt(icount),ll(icount),za(icount)
       ! if(za(icount) == 0.0d0) cycle
       ! lnd(lt(icount))=lnd(lt(icount))+1
        !icount = icount + 1
    enddo
    if(iright /= 0) close(iunit)

    amc%lrow_ptr(1)=1
    do il=1,nd
       amc%lrow_ptr(il+1)=amc%lrow_ptr(il)+lnd(il)
       lpt(il) = amc%lrow_ptr(il)
    enddo
    ! lpt(:) = 1
    do is=1,nss
       it = lt(is)
       amc%aval(lpt(it))=za(is); amc%lcol_ind(lpt(it))=ll(is)
       lpt(it)=lpt(it)+1
    enddo
 endif

 write(*, *) 'check iright', iright, is
 
 if(iright /= 0) then
    call get_command_argument(iright,value,nlength,nstatus)
    value=trim(value)
    open(iunit, file = value, action = 'read', pad = 'yes', iostat = ierr )
    if(ierr.ne.0) then
       print*, 'input.txt does not exists'
    endif
    read(iunit, *) idum1, idum2
    do il=1,nd
       ! write(*, *) il
       read(iunit,*) amc%bval(il)
    enddo
    ! write(*, *) 'test'
 else
    amc%bval(:) = 1.0d0
 endif
 close(iunit)

endsubroutine
 
end program
 
