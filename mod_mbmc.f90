!C*****************************************************************************************************
!C Created by Masatoshi Kawai.
!C Latest updated date was 2014/9/12.
!C This is the program for ordering.
!C*****************************************************************************************************
module mod_mbmc
  !$ use omp_lib

  type :: ordering_elem
     integer stat, end
  end type ordering_elem

  type :: ordering_color
     integer b_num
     type(ordering_elem), pointer :: iblock(:) => null(), thread(:) => null()
     ! type(ordering_elem), allocatable :: 
  end type ordering_color

  type :: ordering
     integer c_num, num_threads
     type(ordering_color), allocatable :: color(:)
     type(ordering_elem), pointer :: basep_block(:), basep_thread(:)
     type(ordering_elem), pointer :: thread(:), thread_rmnd(:)
  end type ordering

  type list_block_elem
     integer ndl
     integer, pointer :: list(:) => null()
  end type list_block_elem

  type list_block
     integer c_num
     type(list_block_elem), allocatable :: row(:)
     integer, pointer :: basep_list(:)
  end type list_block

  type elements
     integer ndl
     integer, allocatable :: lct(:), line(:)
     double precision, allocatable :: val(:)
  end type elements

  type neighbor_mat
     integer nd, total
     type(elements), allocatable :: row(:)
  end type neighbor_mat

end module mod_mbmc

module metis_interface

  use iso_c_binding

  implicit none

!If you use Metis, please copy below declaration commented out to your program and add a line "use metis_interface" before implicit declaration.
  ! integer(c_int) nvtxs, ncon, nparts
  ! integer(c_int), allocatable :: xadj(:), adjncy(:), objval(:), part(:)
  ! type(c_ptr) vwgt, vsize, adjwgt, tpwgts,  ubvec, opts
  ! integer(c_int) err !err is for return from metis. 1 indicates returned normally. -1 indicates an input error. &
                     !-2 indicates that it could not allocate the requred memory. -3 indicates some other type of error. Metis version is 5.1.0.

  interface
     integer(c_int) function METIS_PartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec &
                                               , opts, objval, part) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int),               intent(in)  :: nvtxs, ncon !nvtxs is the number of vetices. ncon is the number of balancing constraints.
       integer(c_int), dimension(*), intent(in)  :: xadj, adjncy !xadj is the vector of row pointer. adjncy is the adjancy list.
       type(c_ptr),                  value       :: vwgt !The weight of the vertices.
       type(c_ptr),                  value       :: vsize !The size of the vertices for computing the total communication volume.
       type(c_ptr),                  value       :: adjwgt !The weight of the edges.
       integer(c_int),               intent(in)  :: nparts !The number of parts to partition the graph.
       type(c_ptr),                  value       :: tpwgts !This specifies the desired weight for each partition and constraint.
       type(c_ptr),                  value       :: ubvec  !This specifies the allowed special tolerance for each constraint.
       type(c_ptr),                  value       :: opts !Defined value "METIS_NOPTIONS" in the metis.h is 40. Metis's version is 5.1.0.
       ! integer(c_int), dimension(*), intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int),               intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int), dimension(*), intent(out) :: part !This is a vector stores the patition vector of the graph.
     end function METIS_PartGraphKway

     integer(c_int) function METIS_PartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec &
                                               , opts, objval, part) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int),               intent(in)  :: nvtxs, ncon !nvtxs is the number of vetices. ncon is the number of balancing constraints.
       integer(c_int), dimension(*), intent(in)  :: xadj, adjncy !xadj is the vector of row pointer. adjncy is the adjancy list.
       type(c_ptr),                  value       :: vwgt !The weight of the vertices.
       type(c_ptr),                  value       :: vsize !The size of the vertices for computing the total communication volume.
       type(c_ptr),                  value       :: adjwgt !The weight of the edges.
       integer(c_int),               intent(in)  :: nparts !The number of parts to partition the graph.
       type(c_ptr),                  value       :: tpwgts !This specifies the desired weight for each partition and constraint.
       type(c_ptr),                  value       :: ubvec  !This specifies the allowed special tolerance for each constraint.
       type(c_ptr),                  value       :: opts !Defined value "METIS_NOPTIONS" in the metis.h is 40. Metis's version is 5.1.0.
       ! integer(c_int), dimension(*), intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int),               intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int), dimension(*), intent(out) :: part !This is a vector stores the patition vector of the graph.
     end function METIS_PartGraphRecursive

     integer(c_int) function Metis_SetDefaultOptions(opts) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int), dimension(0:39), intent(out)  :: opts
     end function Metis_SetDefaultOptions
  end interface

end module metis_interface
