!C*****************************************************************************************************
!C Created by Masatoshi Kawai.
!C Latest updated date was 2014/9/12.
!C The algorithm of this aggregation is written in the 
!C "Algebraic Multigrid Method by Smoothed Aggregation for Second and Fourth Order Eliptic Problems".
!C*****************************************************************************************************

subroutine gen_DC_mat(Apr, DCmat, param1, param2, flag_dynamic, threshold, ig)
  use m_primapr
  implicit none
  type(primapr), intent(in) :: Apr
  type(primdc), intent(out) :: DCmat
  double precision, intent(in) :: param1, param2, threshold
  integer, intent(in) :: ig
  logical, intent(in) :: flag_dynamic
  ! double precision, allocatable :: tmp_dc()

  type(primdc_col_elem), allocatable :: dc_tmp(:)
  integer, allocatable :: tmp(:)
  logical, allocatable :: logic_dc(:), logic_st2(:)
  integer num_row_dc, count, count1
  integer i, j, k, sum_st1, sum_st2, sum_st3
  logical flag
  double precision :: param
  double precision, allocatable :: list_params(:)

  interface
     recursive subroutine q_sort_d(val, left, right)
       implicit none
       double precision, intent(inout) :: val(:)
       integer, intent(in) :: left, right
     end subroutine q_sort_d
  end interface
  
  if(flag_dynamic) then
     allocate(list_params(Apr%nonzero-Apr%nd))
     count = 1
     do j = 1, Apr%nd
        do i = 2, Apr%Aprl(j)%ndl
           list_params(count) = dabs(Apr%Aprl(j)%avl(i)) / sqrt(Apr%Aprl(j)%avl(1) * Apr%Aprl(Apr%Aprl(j)%lct(i))%avl(1))
           ! write(*, *) 'Check params', list_params(count)
           count = count + 1
        enddo
     enddo
     write(*, *) 'Check count nd', count, Apr%nonzero-Apr%nd
     call q_sort_d(list_params, 1, Apr%nonzero-Apr%nd)
     write(*, *) 'Fin sort'
     do i = 1, Apr%nonzero-Apr%nd-1
        if(list_params(i) > list_params(i+1)) then
           write(*, *) 'Bug in sorting routine'
           stop
        endif
     enddo
     param = list_params(int((Apr%nonzero-Apr%nd) * (1.0d0-threshold)))
  else
     param = param1 * (param2 ** (dble(ig-1)))
  endif
  write(*, '(a50, E10.3, i9, i3)') 'Aggregate parameter, number of nodes and level is ', param, Apr%nd, ig
  allocate(DCmat%basep_val(Apr%nd), logic_dc(Apr%nd), dc_tmp(Apr%nd), tmp(Apr%nd), DCmat%rev_ind(Apr%nd), logic_st2(Apr%nd))
  DCmat%basep_val(:) = 0
  logic_dc(:) = .true.

  !C*****step 1***********************************************
  !C Choosing a root node and aggregate the neighbor nodes.
  num_row_dc = 0
  sum_st1 = 0
  DCmat%ndl_rm = 0
  do j = 1, Apr%nd
     if(.not. logic_dc(j)) cycle
     if(Apr%aprl(j)%ndl == 1) then
        DCmat%ndl_rm = DCmat%ndl_rm + 1
        cycle
     endif
     count = 0
     ! write(*, *) 'Check2', j, Apr%aprl(j)%avl(:), logic_dc(Apr%Aprl(j)%lct(:)), Apr%Aprl(j)%lct(:)
     do i = 2, Apr%Aprl(j)%ndl
        if( dabs(Apr%Aprl(j)%avl(i)) >= param*sqrt(Apr%Aprl(j)%avl(1) * Apr%Aprl(Apr%Aprl(j)%lct(i))%avl(1)) ) then
           ! write(*, *) 'Check4', j, i, Apr%aprl(j)%avl(i), Apr%Aprl(j)%avl(1), Apr%Aprl(Apr%Aprl(j)%lct(i))%avl(1), param*sqrt(Apr%Aprl(j)%avl(1) * Apr%Aprl(Apr%Aprl(j)%lct(i))%avl(1))
           if(.not. logic_dc(Apr%Aprl(j)%lct(i)) ) exit
           ! if(j == 9) write(*, *) 'Check4', count, Apr%Aprl(j)%ndl
           count = count + 1
           tmp(count) = Apr%Aprl(j)%lct(i)
           ! write(*, *) 'check', num_row_dc, Apr%Aprl(j)%lct(i)
        endif
     enddo
     if(i == Apr%Aprl(j)%ndl + 1) then
        ! write(*, *) 'Check2', j
        num_row_dc = num_row_dc + 1
        dc_tmp(num_row_dc)%ndl_st1 = count+1
        dc_tmp(num_row_dc)%ndl_st3 = 0
        allocate(dc_tmp(num_row_dc)%val_st1(count+1))
        dc_tmp(num_row_dc)%val_st1(1) = j
        dc_tmp(num_row_dc)%val_st1(2:count+1) = tmp(1:count)
        logic_dc(j) = .false.
        DCmat%rev_ind(j) = num_row_dc
        do i = 1, count
           logic_dc( tmp(i) ) = .false.
           DCmat%rev_ind( tmp(i) ) = num_row_dc
           ! if(num_row_dc == 1) write(*, *) 'Check2', i, tmp(i)
        enddo
        sum_st1 = sum_st1 + count + 1
     endif
  enddo
  
  ! write(*, *) 'Check3', dc_tmp(1:num_row_dc)%ndl_st1

  write(*, '(a39, i9, a15, i9, a8, i9, a16, i9)') 'Number of Aggregated nodes on step1 is ', &
     sum_st1, ', All nodes is ', apr%nd, ', Remained nodes is ', apr%nd-sum_st1, ', Root nodes is ', num_row_dc

  !C*****step2************************************************
  !C Choosing the other nodes which is neighbor to the aggregated nodes.

  ! dc_tmp(:)%ndl_ar = 0
  dc_tmp(:)%ndl_st2 = 0
  tmp(:) = 0
  logic_st2(:) = .true.
  sum_st2 = 0
  do j = 1, Apr%nd
     if(.not. logic_dc(j)) cycle
     ! if(Apr%aprl(j)%ndl == 1) cycle
     do i = 2, Apr%Aprl(j)%ndl
        if( dabs(Apr%Aprl(j)%avl(i)) >= param*sqrt(Apr%Aprl(j)%avl(1) * Apr%Aprl(Apr%Aprl(j)%lct(i))%avl(1)) ) then
           if((.not. logic_dc(Apr%Aprl(j)%lct(i))) .and. logic_st2(Apr%Aprl(j)%lct(i)) ) then
              ! if(j == 4293) write(*, *) 'Check4', Apr%Aprl(j)%lct(i), DCmat%rev_ind(Apr%Aprl(j)%lct(i)), dc_tmp(DCmat%rev_ind(Apr%Aprl(j)%lct(i)))%ndl_st2
              ! write(*, *) 'Check6', j, i, Apr%Aprl(j)%lct(i), DCmat%rev_ind(Apr%Aprl(j)%lct(i))
              dc_tmp(DCmat%rev_ind(Apr%Aprl(j)%lct(i)))%ndl_st2 = dc_tmp(DCmat%rev_ind(Apr%Aprl(j)%lct(i)))%ndl_st2 + 1
              DCmat%rev_ind(j) = DCmat%rev_ind(Apr%Aprl(j)%lct(i))
              tmp(j) = DCmat%rev_ind(Apr%Aprl(j)%lct(i))
              logic_dc(j) = .false.
              logic_st2(j) = .false. ! Add this for debug. Should check.
              sum_st2 = sum_st2 + 1
              exit
           endif
        endif
     enddo
  enddo

  do i = 1, num_row_dc
     ! if(i == 80114) write(*, *) 'Check5', i, dc_tmp(i)%ndl_st2
     if(dc_tmp(i)%ndl_st2 == 0) cycle
     ! write(*, *) 'Check2', i, dc_tmp(i)%ndl_st2
     allocate( dc_tmp(i)%val_st2(dc_tmp(i)%ndl_st2) )
     dc_tmp(i)%val_st2(:) = 0
  enddo
  do j = 1, Apr%nd
     if(tmp(j) == 0) cycle
     ! write(*, *) 'Check3', j
     do i = 1, dc_tmp(tmp(j))%ndl_st2
        if(dc_tmp(tmp(j))%val_st2(i) == 0) then
           dc_tmp(tmp(j))%val_st2(i) = j
           exit
        endif
     enddo
  enddo

  write(*, '(a39 i9, a15, i9, a8, i9)') 'Number of Aggregated nodes on step2 is ', &
     sum_st2, ', All nodes is ', apr%nd, ', Remained nodes is ', apr%nd-sum_st1-sum_st2

  !C*****step3************************************************
  !C Choosing the other nodes which is neighbor to the aggregated nodes.
  !C In addition, count the nodes which is neighbor from the each aggregated blocks.

  sum_st3 = 0
  DCmat%val_rm(1:DCmat%ndl_rm) => DCmat%basep_val(Apr%nd-DCmat%ndl_rm+1:Apr%nd)
  count1 = 1
  do j = 1, Apr%nd
     if(.not. logic_dc(j)) cycle
     if(Apr%aprl(j)%ndl == 1) then
        DCmat%val_rm(count1) = j
        count1 = count1 + 1
        cycle
     endif
     if(Apr%aprl(j)%ndl /= 1) write(*, *) 'Check2. This is not only', j
     num_row_dc = num_row_dc + 1
     count = 1
     logic_dc(j) = .false.
     sum_st3 = sum_st3 + 1
     tmp(1) = j
     do i = 2, Apr%aprl(j)%ndl
        if( dabs(Apr%Aprl(j)%avl(i)) >= param*sqrt(Apr%Aprl(j)%avl(1) * Apr%Aprl(Apr%Aprl(j)%lct(i))%avl(1)) ) then
           count = count + 1
           tmp(count) = Apr%Aprl(j)%lct(i)
           logic_dc(Apr%Aprl(j)%lct(i)) = .false.
           sum_st3 = sum_st3 + 1
        endif
     enddo
     dc_tmp(num_row_dc)%ndl_st3 = count
     dc_tmp(num_row_dc)%ndl_st1 = 0
     dc_tmp(num_row_dc)%ndl_st2 = 0
     allocate(dc_tmp(num_row_dc)%val_st3(count))
     dc_tmp(num_row_dc)%val_st3(1) = j
     dc_tmp(num_row_dc)%val_st3(2:count) = tmp(2:count)
     do i = 1, count
        DCmat%rev_ind(tmp(i)) = num_row_dc
     enddo
  enddo

  write(*, '(a39, i9, a15, i9, a8, i9, a16, i9)') 'Number of Aggregated nodes on step3 is ', &
     sum_st3, ', All nodes is ', apr%nd, ', Remained nodes is ', apr%nd-sum_st1-sum_st2-sum_st3, ', Root nodes is ', num_row_dc
  write(*, '(a15, i9, a20, i9)') 'Remove node is ', DCmat%ndl_rm, ', Remained nodes is ', apr%nd-sum_st1-sum_st2-sum_st3-DCmat%ndl_rm

  !C*****Finilize************************************************
  !C Reallocate DC_mat and deallocate all temporary arrays.

  DCmat%nd = num_row_dc
  allocate(DCmat%row(num_row_dc))
  count = 1
  do i = 1, num_row_dc
     count1 = count

     DCmat%row(i)%ndl_st1 = dc_tmp(i)%ndl_st1
     if(dc_tmp(i)%ndl_st1 /= 0) then
        DCmat%row(i)%val_st1(1:dc_tmp(i)%ndl_st1) => DCmat%basep_val(count:count+dc_tmp(i)%ndl_st1-1)
        count = count+dc_tmp(i)%ndl_st1
        DCmat%row(i)%val_st1(1:dc_tmp(i)%ndl_st1) = dc_tmp(i)%val_st1(1:dc_tmp(i)%ndl_st1)
        deallocate(dc_tmp(i)%val_st1)
     endif

     DCmat%row(i)%ndl_st2 = dc_tmp(i)%ndl_st2
     if(dc_tmp(i)%ndl_st2 /= 0) then
        DCmat%row(i)%val_st2(1:dc_tmp(i)%ndl_st2) => DCmat%basep_val(count:count+dc_tmp(i)%ndl_st2-1)
        count = count+dc_tmp(i)%ndl_st2
        DCmat%row(i)%val_st2(1:dc_tmp(i)%ndl_st2) = dc_tmp(i)%val_st2(1:dc_tmp(i)%ndl_st2)
        deallocate(dc_tmp(i)%val_st2)
     endif

     DCmat%row(i)%ndl_st3 = dc_tmp(i)%ndl_st3
     if(dc_tmp(i)%ndl_st3 /= 0) then
        DCmat%row(i)%val_st3(1:dc_tmp(i)%ndl_st3) => DCmat%basep_val(count:count+dc_tmp(i)%ndl_st3-1)
        count = count+dc_tmp(i)%ndl_st3
        DCmat%row(i)%val_st3(1:dc_tmp(i)%ndl_st3) = dc_tmp(i)%val_st3(1:dc_tmp(i)%ndl_st3)
        deallocate(dc_tmp(i)%val_st3)
     endif

     DCmat%row(i)%ndl_all = DCmat%row(i)%ndl_st1 + DCmat%row(i)%ndl_st2 + DCmat%row(i)%ndl_st3
     DCmat%row(i)%val_all(1:DCmat%row(i)%ndl_all) => DCmat%basep_val(count1:count1+DCmat%row(i)%ndl_all-1)
  enddo

  deallocate(dc_tmp, tmp, logic_dc)

end subroutine gen_dc_mat

!***construct_transfer_functions
subroutine construct_transfer_functions(Apr,tFC,tCF,DCmat,weight_jc,weight_prl,param1,param2,ig)
  use m_primapr
  implicit none
  type(primapr),intent(out) :: tFC,tCF
  type(primapr),intent(in) :: Apr
  type(primdc),intent(inout) :: DCmat
  double precision, intent(in) :: weight_jc, weight_prl, param1, param2
  integer, intent(in) :: ig
  integer, allocatable :: tmp_int(:), tmp_count(:)
  ! integer, allocatable, save :: tmp_int_save(:)
  logical, allocatable :: tmp_logic(:)
  double precision, allocatable :: tmp_real(:)
  integer i, j, k, count1, count2, count3, it, jt, rev_agind, me_omp
  double precision param, ado
  double precision :: time(3)
  !double precision :: weight = 1.0d0
  
  interface

     recursive subroutine q_sort(lct, left, right)
       integer, intent(inout) :: lct(:)
       integer, intent(in) :: left, right
     end subroutine q_sort

     logical function cond_neig_node(bind_own, bind_neig, logic, val1, val2, val3, pivot)
       implicit none
       integer, intent(in) :: bind_own, bind_neig
       double precision, intent(in) :: val1, val2, val3, pivot
       logical, intent(in) :: logic
     end function cond_neig_node

  end interface

  me_omp = 1
  !$ me_omp = omp_get_thread_num()+1

  param = param1 * (param2 ** (dble(ig-1)))
  ! param = param1 * param2 ** (1.0d0/dble(ig))

  allocate(tmp_real(Apr%nd), tmp_int(Apr%nd))

  ! !$OMP master
  !Count the number of increased neighbor nodes on restriction matrix.
  allocate(tmp_logic(Apr%nd))
  tmp_logic(:) = .true.
  DCmat%row(:)%ndl_ar = 0
  do j = 1, Apr%nd
     count1 = 0
     do i = 2, Apr%aprl(j)%ndl
        it = Apr%aprl(j)%lct(i)
        rev_agind = DCmat%rev_ind(it)
        if(cond_neig_node(DCmat%rev_ind(j), rev_agind, tmp_logic(rev_agind), Apr%aprl(j)%avl(i), Apr%aprl(j)%avl(1), Apr%aprl(it)%avl(1) &
           ! , param)) then
             , 0.0d0)) then
           DCmat%row(rev_agind)%ndl_ar = DCmat%row(rev_agind)%ndl_ar + 1
           tmp_logic(rev_agind) = .false.
           count1 = count1 + 1
           tmp_int(count1) = rev_agind
        endif
     enddo
     do i = 1, count1
        tmp_logic(tmp_int(i)) = .true.
     enddo
  enddo
  ! write(*, *) 'Check2', sum(DCmat%row(:)%ndl_ar)
  allocate(DCmat%basep_ar(sum(DCmat%row(:)%ndl_ar)))

  !Allocate base pointer for translation matrices(tCF,tFC)
  tFC%nd = DCmat%nd
  tCF%nd = Apr%nd
  allocate(tFC%aprl(tFC%nd))
  count1 = 0
  do i = 1, DCmat%nd
     tFC%aprl(i)%ndl = Dcmat%row(i)%ndl_st1 + Dcmat%row(i)%ndl_st2 + Dcmat%row(i)%ndl_st3 + Dcmat%row(i)%ndl_ar 
     count1 = count1 + tFC%aprl(i)%ndl
  enddo
  allocate(tFC%basep_lct(count1), tFC%basep_avl(count1))
  allocate(tCF%aprl(tCF%nd), tCF%basep_lct(count1), tCF%basep_avl(count1))
  tFC%nonzero = count1
  tCF%nonzero = count1
  write(*, *) 'Number of nonzero', tFC%nonzero

  !Assign addresses to pointers and column index to array "lct"
  count1 = 1
  do i = 1, DCmat%nd
     tFC%aprl(i)%lct(1:tFC%aprl(i)%ndl) => tFC%basep_lct(count1:count1+tFC%aprl(i)%ndl-1)
     tFC%aprl(i)%avl(1:tFC%aprl(i)%ndl) => tFC%basep_avl(count1:count1+tFC%aprl(i)%ndl-1)
     count1 = count1 + tFC%aprl(i)%ndl

     ! if(DCmat%row(i)%ndl_st1 /= 0)tFC%aprl(i)%lct(1:DCmat%row(i)%ndl_st1) = DCmat%row(i)%val_st1(:)
     ! count2 = DCmat%row(i)%ndl_st1 + 1
     ! if(DCmat%row(i)%ndl_st2 /= 0)tFC%aprl(i)%lct(count2:count2+DCmat%row(i)%ndl_st2-1) = DCmat%row(i)%val_st2(:)
     ! count2 = DCmat%row(i)%ndl_st1 + DCmat%row(i)%ndl_st2 + 1
     ! if(DCmat%row(i)%ndl_st3 /= 0)tFC%aprl(i)%lct(count2:count2+DCmat%row(i)%ndl_st3-1) = DCmat%row(i)%val_st3(:)

     ! tmp_int(i) = DCmat%row(i)%ndl_st1 + DCmat%row(i)%ndl_st2 + DCmat%row(i)%ndl_st3 + 1
  enddo
  ! !$OMP end master
  ! !$OMP barrier

  ! !$OMP do private(i)
  do i = 1, DCmat%nd
     tFC%aprl(i)%lct(1:DCmat%row(i)%ndl_all) = DCmat%row(i)%val_all(:)
  enddo
  ! !$OMP enddo
  ! !$OMP barrier

  ! !$OMP master
  !Assing index of neighbor nodes.
  allocate(tmp_count(DCmat%nd))
  ! tmp_count(:) = 1
  count3 = 1
  do i = 1, DCmat%nd
     tmp_count(i) = DCmat%row(i)%ndl_st1 + DCmat%row(i)%ndl_st2 + DCmat%row(i)%ndl_st3 + 1
     DCmat%row(i)%val_ar => DCmat%basep_ar(count3:count3+DCmat%row(i)%ndl_ar-1)
     count3 = count3+DCmat%row(i)%ndl_ar
  enddo
  do j = 1, Apr%nd
     count1 = 0
     do i = 2, Apr%aprl(j)%ndl
        it = Apr%aprl(j)%lct(i)
        rev_agind = DCmat%rev_ind(it)
        if(cond_neig_node(DCmat%rev_ind(j), rev_agind, tmp_logic(rev_agind), Apr%aprl(j)%avl(i), Apr%aprl(j)%avl(1), Apr%aprl(it)%avl(1)&
           ! , param)) then
             , 0.0d0)) then
           tFC%aprl(rev_agind)%lct(tmp_count(rev_agind)) = j
           tmp_count(rev_agind) = tmp_count(rev_agind) + 1

           tmp_logic(rev_agind) = .false.
           count1 = count1 + 1
           tmp_int(count1) = rev_agind
        endif
     enddo
     do i = 1, count1
        tmp_logic(tmp_int(i)) = .true.
     enddo
  enddo
  do i = 1, DCmat%nd
     it = DCmat%row(i)%ndl_all + 1
     DCmat%row(i)%val_ar(1:DCmat%row(i)%ndl_ar) = tFC%aprl(i)%lct(it:it+DCmat%row(i)%ndl_ar-1)
  enddo
  ! !$OMP end master
  ! !$OMP barrier

  ! write(*, *) tFC%basep_lct
  ! write(*, *)
  ! write(*, *) tmp_count(:)
  ! write(*, *)
  ! write(*, *) tFC%aprl(:)%ndl 

  !Sort column of tFC
  ! !$OMP do private(i)
  do i = 1, tFC%nd
     ! write(*, *) 'Check1', tFC%aprl(i)%ndl
     if(tFC%aprl(i)%ndl /= 1) call q_sort(tFC%aprl(i)%lct, 1, tFC%aprl(i)%ndl) !We don't have to swap avl because of avl stil have any data.
  enddo
  ! !$OMP enddo
  ! !$OMP barrier

  !Smoothing restriction operator (w-Jc)
  ! time(:) = 0.0d0
  ! !$OMP do private(tmp_int, i, it, j, jt, k, ado, tmp_real)
  ! !$OMP master
  do k = 1, tFC%nd
     if(k == 1)  tmp_real(:) = 0.0d0

     do j = 1, DCmat%row(k)%ndl_all
        tmp_real(DCmat%row(k)%val_all(j)) = 1.0d0
        tmp_int(j) = DCmat%row(k)%val_all(j)
     enddo
     
     ! call cpu_time(time(3))
     ! time(1) = time(1) + time(3)
     ! time(2) = time(2) - time(3)
     do j = 1, tFC%aprl(k)%ndl
        tFC%aprl(k)%avl(j) = 0.0d0
        jt = tFC%aprl(k)%lct(j)
        ado = Apr%aprl(jt)%avl(1)
        do i = 2, Apr%aprl(jt)%ndl
           
           ! if(dabs(Apr%Aprl(jt)%avl(i)) >= param*sqrt(Apr%Aprl(jt)%avl(1) * Apr%Aprl(Apr%Aprl(jt)%lct(i))%avl(1))) then
           ! if(cond_neig_node(DCmat%rev_ind(j), rev_agind, .true., Apr%aprl(j)%avl(i), Apr%aprl(j)%avl(1), Apr%aprl(it)%avl(1)&
           !      , 0.0d0)) then
              ! , param)) then
              tFC%aprl(k)%avl(j) = tFC%aprl(k)%avl(j) - Apr%aprl(jt)%avl(i) * tmp_real( Apr%aprl(jt)%lct(i) )
           ! else
           !    ado = ado - Apr%Aprl(jt)%avl(i)
           ! endif

        enddo
        tFC%aprl(k)%avl(j) = (1.0d0 - weight_jc) * tmp_real( Apr%aprl(jt)%lct(1) ) +  weight_jc / ado * tFC%aprl(k)%avl(j) !+ (1-weight)*tmp_real( Apr%aprl( tFC%aprl(i)%lct(j) )%diag_ind )
     enddo
     !
     !
     !Adding test from here
     ! tFC%aprl(k)%avl(:) = 1.0d0/sum(tFC%aprl(k)%avl(:)) * tFC%aprl(k)%avl(:)
     !Till here
     ! call cpu_time(time(3))
     ! time(2) = time(2) + time(3)
     ! time(1) = time(1) - time(3)
     do j = 1, DCmat%row(k)%ndl_all !count1-1
        tmp_real(tmp_int(j)) = 0.0d0
     enddo
     ! call cpu_time(time(3))
     ! time(1) = time(1) + time(3)
  enddo
  ! !$OMP master
  ! !$OMP end do
  ! !$OMP barrier
  ! write(*, *) 'Check time.', time(1), time(2)

  !Transpose the restriction matrix to make prolongation matrix.
  ! !$OMP master
  tCF%nd = Apr%nd
  tCF%aprl(:)%ndl = 0
  do i = 1, tCF%nonzero
     tCF%aprl(tFC%basep_lct(i))%ndl = tCF%aprl(tFC%basep_lct(i))%ndl + 1 !Count number of nonzero element on each row.
  enddo
  count1 = 1
  do i = 1, tCF%nd
     tCF%aprl(i)%lct => tCF%basep_lct(count1:count1+tCF%aprl(i)%ndl-1) !Assing address to pointer "lct" and "avl".
     tCF%aprl(i)%avl => tCF%basep_avl(count1:count1+tCF%aprl(i)%ndl-1)
     count1 = count1 + tCF%aprl(i)%ndl
  enddo
  ! !$OMP end master
  ! !$OMP barrier
  tmp_int = 1
  ! write(*, *) 'Check1, rest, prolo', tFC%nd, tCF%nd, associated(tFC%aprl(1)%avl)

  ! if(me_omp == 1) allocate(tmp_int_save(tCF%nd))
  ! !$OMP barrier

  ! !$OMP do private(i, j, k) firstprivate(weight_prl)
  ! !$OMP master
  do k = 1, tFC%nd
     do j = 1, tFC%aprl(k)%ndl
        i = tFC%aprl(k)%lct(j)
        ! write(*, *) 'Check2', k, j, i
        tCF%aprl(i)%lct(tmp_int(i)) = k                  !Assing nonzero element and column index to each array.
        tCF%aprl(i)%avl(tmp_int(i)) = weight_prl * tFC%aprl(k)%avl(j)
        ! !$OMP atomic
        tmp_int(i) = tmp_int(i) + 1
     enddo
  enddo
  ! !$OMP end master

  !Adding test from here
  ! do i = 1, tCF%nd
  !    tCF%aprl(i)%avl(:) = 1.0d0/sum(tCF%aprl(i)%avl(:)) * tCF%aprl(i)%avl(:)
  ! enddo
  !Till here

  ! !$OMP enddo
  ! !$OMP barrier
  ! if(me_omp == 1) deallocate(tmp_int_save)
  ! !$OMP barrier
  
endsubroutine

logical function cond_neig_node(bind_own, bind_neig, logic, val1, val2, val3, pivot)
  implicit none
  integer, intent(in) :: bind_own, bind_neig
  double precision, intent(in) :: val1, val2, val3, pivot
  logical, intent(in) :: logic

  if(bind_own /= bind_neig .and. dabs(val1) >= pivot*sqrt(val2*val3) .and. logic) then
     cond_neig_node = .true.
  else
     cond_neig_node = .false.
  endif

  ! if(dabs(Apr%Aprl(jt)%avl(k)) < param*sqrt(Apr%Aprl(jt)%avl(1) * Apr%Aprl(Apr%Aprl(jt)%lct(k))%avl(1))) then

end function cond_neig_node

recursive subroutine q_sort(lct, left, right)
  implicit none
  integer, intent(inout) :: lct(:)
  integer, intent(in) :: left, right
  integer i, j, pivot, tmp_i
  double precision tmp_d

  ! write(*, *) 'Check2', left, right
  
  pivot = lct(left)
  i = left
  j = right

  do

     do while(lct(i) < pivot)
        i = i + 1
     enddo
     
     do while(pivot < lct(j))
        j = j - 1
     enddo

     if(i >= j) exit

     tmp_i = lct(i)
     lct(i) = lct(j)
     lct(j) = tmp_i

     i = i + 1
     j = j - 1

  enddo

  ! write(*, *) 'Check3', left, right
  if (left  < i - 1) call q_sort(lct, left , i - 1)
  if (j + 1 < right) call q_sort(lct, j + 1, right)

end subroutine q_sort

recursive subroutine q_sort_d(val, left, right)
  implicit none
  double precision, intent(inout) :: val(:)
  integer, intent(in) :: left, right
  integer i, j
  double precision tmp_d, pivot

  ! write(*, *) 'Check2', left, right
  
  pivot = val(left)
  i = left
  j = right

  do

     do while(val(i) < pivot .and. i < right)
        i = i + 1
     enddo
     
     do while(pivot < val(j) .and. j > left)
        j = j - 1
     enddo

     if(i >= j) exit

     tmp_d = val(i)
     val(i) = val(j)
     val(j) = tmp_d

     i = i + 1
     j = j - 1

  enddo

  ! write(*, *) 'Check3', left, right
  if (left  < i - 1) call q_sort_d(val, left , i - 1)
  if (j + 1 < right) call q_sort_d(val, j + 1, right)

end subroutine q_sort_d

!***galerkin_operator
subroutine galerkin_operator(amcc,tfc,tcf,amcf)
  use m_primapr
  implicit none
  ! Make the triple product of matrices in the CRS form
  ! ic represents the ordinal number of row for the derived matrix
  ! jc represents the ordinal number of column for the derived matrix
  type(primapr), intent(in) :: tfc,tcf,amcf
  type(primapr), intent(inout) :: amcc
  integer, allocatable :: lcw(:), tmp_int(:)
  double precision zaw, zw
  integer ndf, ndc, icount, ic, is, i, j, k, it, jt, jc, iw, me_omp
  double precision time_s, time_e

   me_omp = 1
  !$ me_omp = omp_get_thread_num()+1
   ndf=amcf%nd; ndc=tfc%nd
   allocate(lcw(ndc))

  ! !$OMP master
  amcc%nd=ndc
  ! allocate(lcw(ndc),lctw(ndc),avlw(ndc),amcc%aprl(ndc))
  allocate(amcc%aprl(ndc),tmp_int(ndc))
  
  ! call cpu_time(time_s)
  icount = 0
  lcw(1:ndc) = 0
  do ic=1,ndc
     is=0!;lcw(:)=0
     do i=1,tfc%aprl(ic)%ndl
        it=tfc%aprl(ic)%lct(i)
        do j=1,amcf%aprl(it)%ndl
           jt=amcf%aprl(it)%lct(j)
           do k=1,tcf%aprl(jt)%ndl
              jc=tcf%aprl(jt)%lct(k)
              if(lcw(jc)==0) then
                 is=is+1
                 lcw(jc)=is
                 tmp_int(is) = jc
              endif
           enddo
        enddo
     enddo
     do i = 1, is
        lcw( tmp_int(i) ) = 0
     enddo
     amcc%aprl(ic)%ndl=is
     icount=icount+is
  enddo
  ! call cpu_time(time_e)
  ! write(*, *) 'Galerkin 1st', time_e - time_s

  amcc%nonzero=icount
  allocate(amcc%basep_avl(icount), amcc%basep_lct(icount))

  icount = 1
  do ic = 1, ndc
     amcc%aprl(ic)%avl(1:amcc%aprl(ic)%ndl)=>amcc%basep_avl(icount:icount+amcc%aprl(ic)%ndl-1)
     amcc%aprl(ic)%lct(1:amcc%aprl(ic)%ndl)=>amcc%basep_lct(icount:icount+amcc%aprl(ic)%ndl-1)
     icount=icount+amcc%aprl(ic)%ndl
  enddo

  ! !$OMP end master
  ! !$OMP barrier

  ! call cpu_time(time_s)
  ! !$OMP do private(i, j, k, ic, is, it, jt ,jc, zaw, iw, zw, lcw)
  do ic=1,ndc
     if(ic == 1) lcw(1:ndc) = 0
     is=0
     do i=1,tfc%aprl(ic)%ndl
        it=tfc%aprl(ic)%lct(i)
        do j=1,amcf%aprl(it)%ndl
           jt=amcf%aprl(it)%lct(j)
           do k=1,tcf%aprl(jt)%ndl
              jc=tcf%aprl(jt)%lct(k)
              zaw=tfc%aprl(ic)%avl(i)*amcf%aprl(it)%avl(j)*tcf%aprl(jt)%avl(k)
              if(lcw(jc)==0)then
                 is=is+1
                 lcw(jc)=is
                 amcc%aprl(ic)%avl(is)=zaw
                 amcc%aprl(ic)%lct(is)=jc
              else
                 amcc%aprl(ic)%avl(lcw(jc))=amcc%aprl(ic)%avl(lcw(jc))+zaw
              endif
           enddo
        enddo
     enddo
     do i = 1, is
        lcw( amcc%aprl(ic)%lct(i) ) = 0
     enddo
     do i=1,is
        if(amcc%aprl(ic)%lct(i) == ic) then
           iw=amcc%aprl(ic)%lct(1)
           zw=amcc%aprl(ic)%avl(1)
           amcc%aprl(ic)%lct(1)=ic
           amcc%aprl(ic)%avl(1)=amcc%aprl(ic)%avl(i)
           amcc%aprl(ic)%lct(i)=iw
           amcc%aprl(ic)%avl(i)=zw
           amcc%aprl(ic)%ado = 1.0d0 / amcc%aprl(ic)%avl(1)
           exit
        endif
     enddo
  enddo
  ! !$OMP end do
  ! !$OMP end master
  ! !$OMP barrier

  ! call cpu_time(time_e)
  ! write(*, *) 'Galerkin 2nd', time_e - time_s

  if(me_omp == 1) deallocate(lcw)

end subroutine galerkin_operator

subroutine first_touch(amcc,lcol_ind,aval)
  use m_primapr
  implicit none
  type(primaprccs), intent(inout) :: amcc
  integer, pointer :: lcol_ind(:)
  double precision, pointer :: aval(:)
  type(primapr), save :: tmp_apr_reord, tmp_apr
  integer, allocatable, save :: tmp_i(:), tmp_diag(:)
  integer i, j, k, ig, stat, end, count, me_omp, clr, blc, diag, it, nd
  
  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1

  do ig = 1, amcc%nm-2
     !$OMP master
     allocate(tmp_i(amcc%Apr(1)%nd), tmp_diag(amcc%Apr(1)%nd))
     tmp_i(1) = 1
     allocate(tmp_apr_reord%aprl(amcc%Apr(ig)%nd))
     do i = 1, amcc%Apr(ig)%nd
        tmp_apr_reord%aprl(i) = amcc%Apr_reord(ig)%aprl(i)
        tmp_diag(i) = amcc%Apr_reord(ig)%aprl(i)%lct(1)
        if(i /= amcc%Apr(ig)%nd) tmp_i(i+1) = tmp_i(i) + amcc%Apr(ig)%aprl(i)%ndl
     enddo
     deallocate(amcc%Apr_reord(ig)%aprl)

     allocate(tmp_apr_reord%basep_avl(amcc%Apr(ig)%nonzero), tmp_apr_reord%basep_lct(amcc%Apr(ig)%nonzero))
     if(ig == 1) then
        do i = 1, amcc%Apr(ig)%nonzero
           tmp_apr_reord%basep_lct(i) = lcol_ind(i)!amcc%Apr(ig)%basep_lct(:)
           tmp_apr_reord%basep_avl(i) = aval(i)!amcc%Apr(ig)%basep_avl(:)
        enddo
        deallocate(lcol_ind, aval)
     else
        do i = 1, amcc%Apr(ig)%nonzero
           tmp_apr_reord%basep_avl(i) = amcc%Apr(ig)%basep_avl(i)
           tmp_apr_reord%basep_lct(i) = amcc%Apr(ig)%basep_lct(i)
        enddo
        deallocate(amcc%Apr(ig)%basep_lct, amcc%Apr(ig)%basep_avl)
     endif
     
     do i = 1, amcc%Apr(ig)%nd
        it = tmp_apr_reord%aprl(i)%ndl
        diag = tmp_diag(i)
        tmp_apr_reord%aprl(i)%lct(1:it) => tmp_apr_reord%basep_lct(tmp_i(diag):tmp_i(diag)+it-1)
        tmp_apr_reord%aprl(i)%avl(1:it) => tmp_apr_reord%basep_avl(tmp_i(diag):tmp_i(diag)+it-1)
     enddo

     if(ig == 1) then
        allocate(aval(amcc%Apr(ig)%nonzero), lcol_ind(amcc%Apr(ig)%nonzero))
     else
        allocate(amcc%Apr(ig)%basep_avl(amcc%Apr(ig)%nonzero), amcc%Apr(ig)%basep_lct(amcc%Apr(ig)%nonzero))
     endif
     allocate(amcc%Apr_reord(ig)%aprl(amcc%Apr_reord(ig)%nd))
     !$OMP end master
     !$OMP barrier
     
     do clr = 1, amcc%order(ig)%c_num
        do blc = amcc%order(ig)%color(clr)%thread(me_omp)%stat, amcc%order(ig)%color(clr)%thread(me_omp)%end
           stat = amcc%order(ig)%color(clr)%iblock(blc)%stat
           end = amcc%order(ig)%color(clr)%iblock(blc)%end
           do i = stat, end
              amcc%Apr_reord(ig)%aprl(i) = tmp_apr_reord%aprl(i)
              it = amcc%Apr_reord(ig)%aprl(i)%ndl
              diag = tmp_diag(i)
              if(ig == 1) then
                 amcc%Apr(ig)%aprl(diag)%lct(1:it) => lcol_ind(tmp_i(diag):tmp_i(diag)+it-1)
                 amcc%Apr(ig)%aprl(diag)%avl(1:it) => aval(tmp_i(diag):tmp_i(diag)+it-1)
                 amcc%Apr_reord(ig)%aprl(i)%lct(1:it) => lcol_ind(tmp_i(diag):tmp_i(diag)+it-1)
                 amcc%Apr_reord(ig)%aprl(i)%avl(1:it) => aval(tmp_i(diag):tmp_i(diag)+it-1)
              else
                 amcc%Apr(ig)%aprl(diag)%lct(1:it) => amcc%Apr(ig)%basep_lct(tmp_i(diag):tmp_i(diag)+it-1)
                 amcc%Apr(ig)%aprl(diag)%avl(1:it) => amcc%Apr(ig)%basep_avl(tmp_i(diag):tmp_i(diag)+it-1)
                 amcc%Apr_reord(ig)%aprl(i)%lct(1:it) => amcc%Apr(ig)%basep_lct(tmp_i(diag):tmp_i(diag)+it-1)
                 amcc%Apr_reord(ig)%aprl(i)%avl(1:it) => amcc%Apr(ig)%basep_avl(tmp_i(diag):tmp_i(diag)+it-1)
              endif
              amcc%Apr_reord(ig)%aprl(i)%lct(1:it) = tmp_apr_reord%aprl(i)%lct(1:it)
              amcc%Apr_reord(ig)%aprl(i)%avl(1:it) = tmp_apr_reord%aprl(i)%avl(1:it)
           enddo
        enddo
     enddo
     stat = amcc%order(ig)%thread_rmnd(me_omp)%stat
     end = amcc%order(ig)%thread_rmnd(me_omp)%end
     ! write(*, *) 'FT', stat, end, ig
     do i = stat, end
        amcc%Apr_reord(ig)%aprl(i) = tmp_apr_reord%aprl(i)
        it = amcc%Apr_reord(ig)%aprl(i)%ndl
        diag = tmp_diag(i)
        if(ig == 1) then
           amcc%Apr(ig)%aprl(diag)%lct(1:it) => lcol_ind(tmp_i(diag):tmp_i(diag)+it-1)
           amcc%Apr(ig)%aprl(diag)%avl(1:it) => aval(tmp_i(diag):tmp_i(diag)+it-1)
           amcc%Apr_reord(ig)%aprl(i)%lct(1:it) => lcol_ind(tmp_i(diag):tmp_i(diag)+it-1)
           amcc%Apr_reord(ig)%aprl(i)%avl(1:it) => aval(tmp_i(diag):tmp_i(diag)+it-1)
        else
           amcc%Apr(ig)%aprl(diag)%lct(1:it) => amcc%Apr(ig)%basep_lct(tmp_i(diag):tmp_i(diag)+it-1)
           amcc%Apr(ig)%aprl(diag)%avl(1:it) => amcc%Apr(ig)%basep_avl(tmp_i(diag):tmp_i(diag)+it-1)
           amcc%Apr_reord(ig)%aprl(i)%lct(1:it) => amcc%Apr(ig)%basep_lct(tmp_i(diag):tmp_i(diag)+it-1)
           amcc%Apr_reord(ig)%aprl(i)%avl(1:it) => amcc%Apr(ig)%basep_avl(tmp_i(diag):tmp_i(diag)+it-1)
        endif
        amcc%Apr_reord(ig)%aprl(i)%lct(1:it) = tmp_apr_reord%aprl(i)%lct(1:it)
        amcc%Apr_reord(ig)%aprl(i)%avl(1:it) = tmp_apr_reord%aprl(i)%avl(1:it)        
     enddo
     !$OMP barrier

     if(me_omp == 1) deallocate(tmp_apr_reord%basep_lct, tmp_apr_reord%basep_avl, tmp_apr_reord%aprl, tmp_diag, tmp_i)

     !$OMP master
     allocate(tmp_i(amcc%tFC(1)%nd))
     tmp_i(1) = 1
     allocate(tmp_apr%aprl(amcc%tFC(ig)%nd))
     do i = 1, amcc%tFC(ig)%nd
        tmp_apr%aprl(i) = amcc%tFC(ig)%aprl(i)
        if(i /= amcc%tFC(ig)%nd) tmp_i(i+1) = tmp_i(i) + amcc%tFC(ig)%aprl(i)%ndl
     enddo
     deallocate(amcc%tFC(ig)%aprl)

     allocate(tmp_apr%basep_avl(amcc%tFC(ig)%nonzero), tmp_apr%basep_lct(amcc%tFC(ig)%nonzero))
     do i = 1, amcc%tFC(ig)%nonzero
        tmp_apr%basep_avl(i) = amcc%tFC(ig)%basep_avl(i)
        tmp_apr%basep_lct(i) = amcc%tFC(ig)%basep_lct(i)
     enddo
     deallocate(amcc%tFC(ig)%basep_lct, amcc%tFC(ig)%basep_avl)
     
     do i = 1, amcc%tFC(ig)%nd
        it = tmp_apr%aprl(i)%ndl
        tmp_apr%aprl(i)%lct(1:it) => tmp_apr%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        tmp_apr%aprl(i)%avl(1:it) => tmp_apr%basep_avl(tmp_i(i):tmp_i(i)+it-1)
     enddo
     allocate(amcc%tFC(ig)%basep_avl(amcc%tFC(ig)%nonzero), amcc%tFC(ig)%basep_lct(amcc%tFC(ig)%nonzero))
     allocate(amcc%tFC(ig)%aprl(amcc%tFC(ig)%nd))
     !$OMP end master
     !$OMP barrier

     stat = amcc%order(ig+1)%thread(me_omp)%stat
     end = amcc%order(ig+1)%thread(me_omp)%end
     do i = stat, end
        amcc%tFC(ig)%aprl(i) = tmp_apr%aprl(i)
        it = amcc%tFC(ig)%aprl(i)%ndl
        amcc%tFC(ig)%aprl(i)%lct(1:it) => amcc%tFC(ig)%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        amcc%tFC(ig)%aprl(i)%avl(1:it) => amcc%tFC(ig)%basep_avl(tmp_i(i):tmp_i(i)+it-1)
        amcc%tFC(ig)%aprl(i)%lct(1:it) = tmp_apr%aprl(i)%lct(1:it)
        amcc%tFC(ig)%aprl(i)%avl(1:it) = tmp_apr%aprl(i)%avl(1:it)        
     enddo
     !$OMP barrier

     if(me_omp == 1) deallocate(tmp_apr%basep_lct, tmp_apr%basep_avl, tmp_apr%aprl, tmp_i)

     !$OMP master
     allocate(tmp_i(amcc%tCF(1)%nd))
     tmp_i(1) = 1
     allocate(tmp_apr%aprl(amcc%tCF(ig)%nd))
     do i = 1, amcc%tCF(ig)%nd
        tmp_apr%aprl(i) = amcc%tCF(ig)%aprl(i)
        if(i /= amcc%tCF(ig)%nd) tmp_i(i+1) = tmp_i(i) + amcc%tCF(ig)%aprl(i)%ndl
     enddo
     deallocate(amcc%tCF(ig)%aprl)

     allocate(tmp_apr%basep_avl(amcc%tCF(ig)%nonzero), tmp_apr%basep_lct(amcc%tCF(ig)%nonzero))
     do i = 1, amcc%tFC(ig)%nonzero
        tmp_apr%basep_avl(i) = amcc%tCF(ig)%basep_avl(i)
        tmp_apr%basep_lct(i) = amcc%tCF(ig)%basep_lct(i)
     enddo
     deallocate(amcc%tCF(ig)%basep_lct, amcc%tCF(ig)%basep_avl)
     
     do i = 1, amcc%tCF(ig)%nd
        it = tmp_apr%aprl(i)%ndl
        tmp_apr%aprl(i)%lct(1:it) => tmp_apr%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        tmp_apr%aprl(i)%avl(1:it) => tmp_apr%basep_avl(tmp_i(i):tmp_i(i)+it-1)
     enddo
     allocate(amcc%tCF(ig)%basep_avl(amcc%tCF(ig)%nonzero), amcc%tCF(ig)%basep_lct(amcc%tCF(ig)%nonzero))
     allocate(amcc%tCF(ig)%aprl(amcc%tCF(ig)%nd))
     !$OMP end master
     !$OMP barrier

     stat = amcc%order(ig)%thread(me_omp)%stat
     end = amcc%order(ig)%thread(me_omp)%end
     do i = stat, end
        amcc%tCF(ig)%aprl(i) = tmp_apr%aprl(i)
        it = amcc%tCF(ig)%aprl(i)%ndl
        amcc%tCF(ig)%aprl(i)%lct(1:it) => amcc%tCF(ig)%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        amcc%tCF(ig)%aprl(i)%avl(1:it) => amcc%tCF(ig)%basep_avl(tmp_i(i):tmp_i(i)+it-1)
        amcc%tCF(ig)%aprl(i)%lct(1:it) = tmp_apr%aprl(i)%lct(1:it)
        amcc%tCF(ig)%aprl(i)%avl(1:it) = tmp_apr%aprl(i)%avl(1:it)        
     enddo
     !$OMP barrier

     if(me_omp == 1) deallocate(tmp_apr%basep_lct, tmp_apr%basep_avl, tmp_apr%aprl, tmp_i)

  enddo

end subroutine first_touch

subroutine first_touch2(amcc,lcol_ind,aval)
  use m_primapr
  implicit none
  type(primaprccs), intent(inout) :: amcc
  integer, pointer :: lcol_ind(:)
  double precision, pointer :: aval(:)
  type(primapr), save :: tmp_apr_reord, tmp_apr
  integer, allocatable, save :: tmp_i(:), tmp_diag(:)
  integer i, j, k, ig, stat, end, count, me_omp, clr, blc, diag, it, nd
  
  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1

  do ig = 1, amcc%nm-2

     !$OMP master
     allocate(tmp_apr_reord%aprl(amcc%Apr_reord(ig)%nd), tmp_i(amcc%Apr_reord(ig)%nd), tmp_diag(amcc%Apr_reord(ig)%nd))
     tmp_i(1) = 1
     do i = 1, amcc%Apr_reord(ig)%nd
        tmp_apr_reord%aprl(i) = amcc%Apr_reord(ig)%aprl(i)
        tmp_diag(i) = amcc%Apr_reord(ig)%aprl(i)%lct(1)
        if(i /= amcc%Apr_reord(ig)%nd) tmp_i(i+1) = tmp_i(i) + amcc%Apr_reord(ig)%aprl(i)%ndl
     enddo
     deallocate(amcc%Apr_reord(ig)%aprl)
     allocate(amcc%Apr_reord(ig)%aprl(amcc%Apr_reord(ig)%nd), amcc%Apr_reord(ig)%basep_avl(amcc%Apr(ig)%nonzero), &
              amcc%Apr_reord(ig)%basep_lct(amcc%Apr(ig)%nonzero))
     !$OMP end master
     !$OMP barrier
     
     do clr = 1, amcc%order(ig)%c_num
        do blc = amcc%order(ig)%color(clr)%thread(me_omp)%stat, amcc%order(ig)%color(clr)%thread(me_omp)%end
           stat = amcc%order(ig)%color(clr)%iblock(blc)%stat
           end = amcc%order(ig)%color(clr)%iblock(blc)%end
           do i = stat, end
              amcc%Apr_reord(ig)%aprl(i) = tmp_apr_reord%aprl(i)
              it = amcc%Apr_reord(ig)%aprl(i)%ndl
              diag = tmp_diag(i)
              amcc%Apr_reord(ig)%aprl(i)%lct(1:it) => amcc%Apr_reord(ig)%basep_lct(tmp_i(i):tmp_i(i)+it-1)
              amcc%Apr_reord(ig)%aprl(i)%avl(1:it) => amcc%Apr_reord(ig)%basep_avl(tmp_i(i):tmp_i(i)+it-1)
              do j = 1, it
                 amcc%Apr_reord(ig)%aprl(i)%lct(j) = amcc%Apr(ig)%aprl(diag)%lct(j)
                 amcc%Apr_reord(ig)%aprl(i)%avl(j) = amcc%Apr(ig)%aprl(diag)%avl(j)
              enddo
           enddo
        enddo
     enddo
     stat = amcc%order(ig)%thread_rmnd(me_omp)%stat
     end = amcc%order(ig)%thread_rmnd(me_omp)%end
     ! write(*, *) 'FT', stat, end, ig
     do i = stat, end
        amcc%Apr_reord(ig)%aprl(i) = tmp_apr_reord%aprl(i)
        it = amcc%Apr_reord(ig)%aprl(i)%ndl
        diag = tmp_diag(i)
        amcc%Apr_reord(ig)%aprl(i)%lct(1:1) => amcc%Apr_reord(ig)%basep_lct(tmp_i(i):tmp_i(i))
        amcc%Apr_reord(ig)%aprl(i)%avl(1:1) => amcc%Apr_reord(ig)%basep_avl(tmp_i(i):tmp_i(i))
        amcc%Apr_reord(ig)%aprl(i)%lct(1) = amcc%Apr(ig)%aprl(diag)%lct(1)
        amcc%Apr_reord(ig)%aprl(i)%avl(1) = amcc%Apr(ig)%aprl(diag)%avl(1)      
     enddo
     !$OMP barrier

     if(me_omp == 1) deallocate(tmp_apr_reord%aprl, tmp_diag, tmp_i)

     !$OMP master
     allocate(tmp_i(amcc%tFC(1)%nd))
     tmp_i(1) = 1
     allocate(tmp_apr%aprl(amcc%tFC(ig)%nd))
     do i = 1, amcc%tFC(ig)%nd
        tmp_apr%aprl(i) = amcc%tFC(ig)%aprl(i)
        if(i /= amcc%tFC(ig)%nd) tmp_i(i+1) = tmp_i(i) + amcc%tFC(ig)%aprl(i)%ndl
     enddo
     deallocate(amcc%tFC(ig)%aprl)

     allocate(tmp_apr%basep_avl(amcc%tFC(ig)%nonzero), tmp_apr%basep_lct(amcc%tFC(ig)%nonzero))
     do i = 1, amcc%tFC(ig)%nonzero
        tmp_apr%basep_avl(i) = amcc%tFC(ig)%basep_avl(i)
        tmp_apr%basep_lct(i) = amcc%tFC(ig)%basep_lct(i)
     enddo
     deallocate(amcc%tFC(ig)%basep_lct, amcc%tFC(ig)%basep_avl)
     
     do i = 1, amcc%tFC(ig)%nd
        it = tmp_apr%aprl(i)%ndl
        tmp_apr%aprl(i)%lct(1:it) => tmp_apr%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        tmp_apr%aprl(i)%avl(1:it) => tmp_apr%basep_avl(tmp_i(i):tmp_i(i)+it-1)
     enddo
     allocate(amcc%tFC(ig)%basep_avl(amcc%tFC(ig)%nonzero), amcc%tFC(ig)%basep_lct(amcc%tFC(ig)%nonzero))
     allocate(amcc%tFC(ig)%aprl(amcc%tFC(ig)%nd))
     !$OMP end master
     !$OMP barrier

     stat = amcc%order(ig+1)%thread(me_omp)%stat
     end = amcc%order(ig+1)%thread(me_omp)%end
     do i = stat, end
        amcc%tFC(ig)%aprl(i) = tmp_apr%aprl(i)
        it = amcc%tFC(ig)%aprl(i)%ndl
        amcc%tFC(ig)%aprl(i)%lct(1:it) => amcc%tFC(ig)%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        amcc%tFC(ig)%aprl(i)%avl(1:it) => amcc%tFC(ig)%basep_avl(tmp_i(i):tmp_i(i)+it-1)
        amcc%tFC(ig)%aprl(i)%lct(1:it) = tmp_apr%aprl(i)%lct(1:it)
        amcc%tFC(ig)%aprl(i)%avl(1:it) = tmp_apr%aprl(i)%avl(1:it)        
     enddo
     !$OMP barrier

     if(me_omp == 1) deallocate(tmp_apr%basep_lct, tmp_apr%basep_avl, tmp_apr%aprl, tmp_i)

     !$OMP master
     allocate(tmp_i(amcc%tCF(1)%nd))
     tmp_i(1) = 1
     allocate(tmp_apr%aprl(amcc%tCF(ig)%nd))
     do i = 1, amcc%tCF(ig)%nd
        tmp_apr%aprl(i) = amcc%tCF(ig)%aprl(i)
        if(i /= amcc%tCF(ig)%nd) tmp_i(i+1) = tmp_i(i) + amcc%tCF(ig)%aprl(i)%ndl
     enddo
     deallocate(amcc%tCF(ig)%aprl)

     allocate(tmp_apr%basep_avl(amcc%tCF(ig)%nonzero), tmp_apr%basep_lct(amcc%tCF(ig)%nonzero))
     do i = 1, amcc%tFC(ig)%nonzero
        tmp_apr%basep_avl(i) = amcc%tCF(ig)%basep_avl(i)
        tmp_apr%basep_lct(i) = amcc%tCF(ig)%basep_lct(i)
     enddo
     deallocate(amcc%tCF(ig)%basep_lct, amcc%tCF(ig)%basep_avl)
     
     do i = 1, amcc%tCF(ig)%nd
        it = tmp_apr%aprl(i)%ndl
        tmp_apr%aprl(i)%lct(1:it) => tmp_apr%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        tmp_apr%aprl(i)%avl(1:it) => tmp_apr%basep_avl(tmp_i(i):tmp_i(i)+it-1)
     enddo
     allocate(amcc%tCF(ig)%basep_avl(amcc%tCF(ig)%nonzero), amcc%tCF(ig)%basep_lct(amcc%tCF(ig)%nonzero))
     allocate(amcc%tCF(ig)%aprl(amcc%tCF(ig)%nd))
     !$OMP end master
     !$OMP barrier

     stat = amcc%order(ig)%thread(me_omp)%stat
     end = amcc%order(ig)%thread(me_omp)%end
     do i = stat, end
        amcc%tCF(ig)%aprl(i) = tmp_apr%aprl(i)
        it = amcc%tCF(ig)%aprl(i)%ndl
        amcc%tCF(ig)%aprl(i)%lct(1:it) => amcc%tCF(ig)%basep_lct(tmp_i(i):tmp_i(i)+it-1)
        amcc%tCF(ig)%aprl(i)%avl(1:it) => amcc%tCF(ig)%basep_avl(tmp_i(i):tmp_i(i)+it-1)
        amcc%tCF(ig)%aprl(i)%lct(1:it) = tmp_apr%aprl(i)%lct(1:it)
        amcc%tCF(ig)%aprl(i)%avl(1:it) = tmp_apr%aprl(i)%avl(1:it)        
     enddo
     !$OMP barrier

     if(me_omp == 1) deallocate(tmp_apr%basep_lct, tmp_apr%basep_avl, tmp_apr%aprl, tmp_i)

  enddo

end subroutine first_touch2

subroutine checking_cutpattern(Apr, DCmat, param1, param2, ig)
  use m_primapr
  implicit none
  type(primapr), intent(in) :: Apr
  type(primdc), intent(in) :: DCmat
  double precision, intent(in) :: param1, param2
  integer, intent(in) :: ig

  double precision max_val, min_val, dec
  integer, allocatable :: whole(:), cut_neig(:)
  integer i, j, it
  integer, parameter :: resolution = 10
  integer :: fo = 10

  allocate(whole(resolution), cut_neig(resolution))
 
  do i = 1, Apr%nd
     if(Apr%aprl(i)%ndl == 1) then
        cycle
     else
        min_val = dabs(Apr%aprl(i)%avl(2))
        max_val = dabs(Apr%aprl(i)%avl(2))
        exit
     endif
  enddo
  it = i

  do j = it, Apr%nd
     do i = 2, Apr%aprl(j)%ndl
        if(dabs(Apr%aprl(j)%avl(i)) < min_val) min_val = dabs(Apr%aprl(j)%avl(i))
        if(dabs(Apr%aprl(j)%avl(i)) > max_val) max_val = dabs(Apr%aprl(j)%avl(i))
     enddo
  enddo

  ! write(*, *) min_val, max_val

  whole(:) = 0
  cut_neig(:) = 0
  dec = (max_val - min_val) / dble(resolution)
  do j = 1, Apr%nd
     do i = 2, Apr%aprl(j)%ndl
        it = int(((dabs(Apr%aprl(j)%avl(i)) - min_val) / dec)-dmod((dabs(Apr%aprl(j)%avl(i)) - min_val)/dec, 1.0d0)) + 1
        if(it == 11) it = 10
        ! if(dabs(Apr%aprl(j)%avl(i)) == 1176.47d0) &
        !     write(*, *) dec, min_val, max_val, dabs(Apr%aprl(j)%avl(i)), &
        !     (dabs(Apr%aprl(j)%avl(i)) - min_val) / dec, &
        !      dmod((dabs(Apr%aprl(j)%avl(i)) - min_val)/dec, 1.0d0), &
        !       int(((dabs(Apr%aprl(j)%avl(i)) - min_val) / dec)-dmod((dabs(Apr%aprl(j)%avl(i)) - min_val)/dec, 1.0d0))
        ! write(*, *)
        whole(it) = whole(it) + 1
        if(Dcmat%rev_ind(Apr%aprl(j)%lct(1)) /= Dcmat%rev_ind(Apr%aprl(j)%lct(i))) cut_neig(it) = cut_neig(it) + 1
     enddo
  enddo
  
  whole(:) = whole(:) / 2
  cut_neig(:) = cut_neig(:) / 2
  write(*, *) whole(:)
  write(*, *) cut_neig(:)

  open(fo, file='connect_all.data')
  do i = 1, resolution
     if(whole(i) == 0) then
        write(fo, *) i, whole(i)
     else
        write(fo, *) i, whole(i)/whole(i)*100.0d0
     endif
  enddo
  close(fo)

  open(fo, file='connect_cut.data')
  do i = 1, resolution
     if(whole(i) == 0) then
        write(fo, *) i, whole(i)
     else
        write(fo, *) i, dble(cut_neig(i))/dble(whole(i))*100.0d0
     endif
  enddo
  close(fo)

  stop

end subroutine checking_cutpattern
