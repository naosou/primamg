!C*****************************************************************************************************
!C Created by Masatoshi Kawai.
!C Latest updated date was 2014/9/12.
!C This is the program for ordering.
!C*****************************************************************************************************
subroutine ordering_block(order, Apr_reord, Apr, DCmat, param1, param2, L2_cache_size, L1_cache_size, deciminal, ig)
  use m_primapr
  use mod_mbmc
  use metis_interface
  implicit none
  type(ordering), intent(inout) :: order
  type(primapr), intent(inout) :: Apr_reord
  type(primapr), intent(in) :: Apr
  type(primdc), intent(inout) :: DCmat
  double precision, intent(in) :: param1, param2, L2_cache_size, L1_cache_size, deciminal
  integer, intent(in) :: ig
  type(primdc) :: block_list
  type(neighbor_mat):: neighbor_DC, neighbor_block
  type(list_block) :: color_blist
  double precision param, tmp_L2, block_size, tmp_dsize, tmp_L1
  integer num_threads, block_num, i, it, count, num_threads_old, block_num_L1, max_threads, ntimes_block, block_num_metis, tmp_i
  integer, allocatable :: sub_part(:), tmp_i_array(:)
  logical flag, flag_whole, coloring_alg1, flag_metis, flag_balance, check_balance
  logical, allocatable :: flag_array(:)
  character(100) :: filename
  integer :: fo = 10

  integer(c_int) nvtxs, ncon, nparts, objval
  integer(c_int), allocatable :: xadj(:), adjncy(:), part(:)
  type(c_ptr) vwgt, vsize, adjwgt, tpwgts,  ubvec, opts
  integer(c_int) err
  integer(c_int), pointer :: sub_opts(:), sub_vwgt(:)

  param = param1 * (param2 ** (dble(ig-1)))
  num_threads = 1
  ! num_threads = 28 
  !$ num_threads = omp_get_max_threads()
  num_threads_old = 0

  ! tmp_dsize = (dble(Apr%nonzero-DCmat%ndl_rm)*3.5d0+dble(Apr%nd-DCmat%ndl_rm)*2.0d0)*8.0d0
  tmp_dsize = (dble(Apr%nonzero-DCmat%ndl_rm)*4.0d0+dble(Apr%nd-DCmat%ndl_rm)*2.0d0)*8.0d0
  tmp_L2 = tmp_dsize / dble(L2_cache_size*deciminal)
  tmp_L1 = tmp_dsize / dble(L1_cache_size*deciminal)

  flag_whole = .false.
  do while(.not. flag_whole)
     ! flag = .false.

     flag = .true.
     do while(flag)

        if(num_threads /= num_threads_old) then
           block_num = nint(tmp_L2) + num_threads - mod(nint(tmp_L2), num_threads)
           block_num_L1 = nint(tmp_L1) + num_threads - mod(nint(tmp_L1), num_threads)
           ntimes_block = 1
        endif
        ! write(*, *) 'Check4', block_num, block_num_L1, num_threads, num_threads_old
        if(num_threads == 1) exit

        if(block_num >= 8*num_threads) then
           flag = .false.
        else
           block_num = block_num + 2 * num_threads
           if(block_num > block_num_L1) then
              num_threads = num_threads - 2
              if(num_threads < 1) num_threads = 1
              ! if(num_threads == 1) then
                 ! flag = .false.
                 ! exit
              ! endif
              cycle
           endif
        endif
        num_threads_old = num_threads
           
     enddo
     ! write(*, *) 'Check5', block_num, block_num_L1, num_threads, num_threads_old
     if(block_num == 1) exit

     ! write(*, *) 'Check', num_threads
     ! block_num = 10
     ! block_size = nint(dble((Apr%nonzero-DCmat%ndl_rm)*3+(Apr%nd-DCmat%ndl_rm)*2)/dble(block_num))

     allocate(block_list%basep_val(Apr%nd-DCmat%ndl_rm), block_list%rev_ind(Apr%nd), flag_array(block_num))

     ! write(*, *) 'DCmat'
     ! do i = 1, DCmat%nd
     !    write(*, *) DCmat%row(i)%val_all(:)
     ! enddo
     ! write(*, *)

     call create_neighbor_graph(neighbor_DC, DCmat, Apr) !Should move in the aggregation routine.

     ! write(*, *) 'Checking here', maxval(neighbor_DC%row(:)%ndl), minval(neighbor_DC%row(:)%ndl)
     ! allocate(tmp_i_array(minval(neighbor_DC%row(:)%ndl):maxval(neighbor_DC%row(:)%ndl)))
     ! tmp_i_array(:) = 0
     ! do i = 1, neighbor_DC%nd
     !    it = neighbor_DC%row(i)%ndl
     !    tmp_i_array(it) = tmp_i_array(it) + 1
     ! enddo
     ! open(fo, file = 'test_aggr.txt')
     ! do i = minval(neighbor_DC%row(:)%ndl), maxval(neighbor_DC%row(:)%ndl)
     !    if(tmp_i_array(i) /= 0) write(fo, *) i, tmp_i_array(i)
     ! enddo
     ! close(fo)

     ! deallocate(tmp_i_array)
     ! allocate(tmp_i_array(minval(Apr%aprl(:)%ndl):maxval(Apr%aprl(:)%ndl)))
     ! tmp_i_array(:) = 0
     ! do i = 1, Apr%nd
     !    it = Apr%aprl(i)%ndl
     !    tmp_i_array(it) = tmp_i_array(it) + 1
     ! enddo
     ! open(fo, file = 'test_apr.txt')
     ! do i = minval(Apr%aprl(:)%ndl), maxval(Apr%aprl(:)%ndl)
     !    if(tmp_i_array(i) /= 0) write(fo, *) i, tmp_i_array(i)
     ! enddo
     ! close(fo)

     ! stop

     allocate(xadj(neighbor_DC%nd+1), adjncy(neighbor_DC%total), sub_vwgt(neighbor_DC%nd), part(neighbor_DC%nd), sub_opts(40))
     allocate(sub_part(neighbor_DC%nd))
     count = 1
     xadj(1) = 1
     do i = 1, neighbor_DC%nd
        ! write(*, *) 'Check2', i, neighbor_DC%row(i)%ndl, count
        adjncy(count:count+neighbor_DC%row(i)%ndl-1) = neighbor_DC%row(i)%lct(1:neighbor_DC%row(i)%ndl)
        count = count + neighbor_DC%row(i)%ndl
        xadj(i+1) = count
        sub_vwgt(i) = DCmat%row(i)%nonzero
     enddo

     err = Metis_SetDefaultOptions(sub_opts)
     if(err /= 1) then
        write(*, *) 'Metis library (METIS_SetDefaultOptions) return error. Error conde is ', err
        stop
     endif
     ! write(*, '(a12, 40i3)') 'Metis_option', sub_opts(:)
     !Options for metis. This options match values in header metis.h version 5.1.0.
     sub_opts(2) = 0 !METIS_OBJETYPE=CUT
     sub_opts(18) = 1 !METIS_OPTIONS_NUMBERING=1 :: Fortran style
     opts = c_loc(sub_opts(1))

     nvtxs = neighbor_DC%nd
     ncon = 1
     nparts = block_num
     vsize = c_null_ptr
     adjwgt = c_null_ptr
     tpwgts = c_null_ptr
     ubvec = c_null_ptr
     vwgt = c_loc(sub_vwgt(1))

     ! write(*, *) 'Check', nparts
     ! err = METIS_PartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec, opts, objval, part)
     write(*, *) 'Check1', block_num, nparts, ntimes_block
     do while(.true.)
        do while(.true.)
           
           write(*, *) 'Check2', block_num, nparts, ntimes_block

           if(nparts /= 1) then
              err = METIS_PartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec, opts, objval, part)
              write(*, *) 'End PartGraph. Edge-cut volume of the patitioning solution is ', objval
              if(err /= 1) then
                 write(*, *) 'Metis library (METIS_PartGraphKway) return error. Error conde is ', err
                 stop
              endif
           else
              part(:) = 1
           endif
           
           flag_array(1:nparts) = .false.
           do i = 1, neighbor_DC%nd
              flag_array(part(i)) = .true.
           enddo
           
           flag_metis = .true.
           do i = 1, nparts
              if(.not. flag_array(i)) then
                 write(*, *) 'False', ntimes_block, i
                 ntimes_block = ntimes_block + 1
                 tmp_i = block_num / num_threads
                 if(mod(tmp_i, ntimes_block) /= 0) then
                    tmp_i = (tmp_i + ntimes_block - mod(tmp_i, ntimes_block)) / ntimes_block
                 else
                    tmp_i = tmp_i / ntimes_block
                 endif
                 nparts = tmp_i * num_threads
                 flag_metis = .false.
                 exit
              endif
           enddo

           if(flag_metis) then
              exit
           else
              ! write(*, *) 'Check2', block_num, nparts, ntimes_block
              cycle
           endif
           
        enddo

        block_list%nd = nparts
        allocate(block_list%row(block_list%nd))
        
        sub_part(:) = part(:)
        call create_neighbor_f_PartGraph(block_list, sub_part, DCmat)
        call create_neighbor_graph(neighbor_block, block_list, Apr)
        
        flag_balance = check_balance(neighbor_block, Apr, DCmat, block_list, ntimes_block)

        if(flag_balance) then
           exit
        else
           write(*, *) 'Retry because of unbalance.', ntimes_block, i
           ntimes_block = ntimes_block + 1
           tmp_i = block_num / num_threads
           if(mod(tmp_i, ntimes_block) /= 0) then
              tmp_i = (tmp_i + ntimes_block - mod(tmp_i, ntimes_block)) / ntimes_block
           else
              tmp_i = tmp_i / ntimes_block
           endif
           nparts = tmp_i * num_threads
           do i = 1, neighbor_block%nd
              deallocate(neighbor_block%row(i)%val, neighbor_block%row(i)%lct, neighbor_block%row(i)%line)
           enddo
           deallocate(block_list%row, neighbor_block%row)
        endif

     enddo
     
     ! if(ig == 4) write(*, *) 'Check', part
     deallocate(xadj, adjncy, sub_vwgt, part, sub_opts)

     write(*, *) 'Check', block_num, nparts, ntimes_block
     ! stop

     ! write(*, *) 'neighbor_block'
     ! do i = 1, block_list%nd
     !    write(*, *) block_list%row(i)%val_all(:)
     ! enddo
     ! write(*, *)

     block_num = nparts*ntimes_block
     write(*, '(a12, i6, a17, i5, a22, i5, a6)') 'block_num = ', block_num, ', L2 cachesize = ', nint(L2_cache_size/1000.0d0), 'KByte, L1 cachesize = ', nint(L1_cache_size/1000.0d0), 'KByte.'
     ! stop

     flag_whole = coloring_alg1(color_blist, neighbor_block, Apr, num_threads, ntimes_block)
     
     if(.not. flag_whole) then
        ! write(*, *) 'Check3', num_threads, block_num

        block_num = block_num + 2 * num_threads
        if(block_num > block_num_L1) then
           num_threads = num_threads - 2
           if(num_threads < 1) num_threads = 1
        endif
        ! write(*, *) 'Check3', num_threads, block_num

        ! num_threads = num_threads - 2
        ! if(num_threads < 1) num_threads = 1
        ! if(num_threads == 1) exit

        do i = 1, neighbor_DC%nd
           deallocate(neighbor_DC%row(i)%val, neighbor_DC%row(i)%lct, neighbor_DC%row(i)%line)
        enddo
        do i = 1, neighbor_block%nd
           deallocate(neighbor_block%row(i)%val, neighbor_block%row(i)%lct, neighbor_block%row(i)%line)
        enddo
        deallocate(block_list%basep_val, block_list%rev_ind, block_list%row, sub_part, neighbor_DC%row, neighbor_block%row, flag_array)

     endif

  enddo

  write(*, *) 'num_threads = ', num_threads

  stop

  ! if(ig == 3) then
  !    write(*, *) 'Check here.', order%c_num
  !    do j = k, order%c_num
  !       write(*, *) 'color and b_num.', k, order%b_num
  !       do j = 1, max_threads
  !          write(*, *) 'me_omp, stat and end', j, order%color(k)%thread(j)%stat, order%color(k)%thread(j)%end


  !       do i = j, order%color(k)%b_num

  !       enddo
  !    enddo

  ! endif
  ! stop

  ! if(num_threads == 1) then
  !    block_num = ((dble(Apr%nonzero-DCmat%ndl_rm)*3.5d0+dble(Apr%nd-DCmat%ndl_rm)*2.0d0)*8.0d0)/dble(cache_size*deciminal)
  !    allocate(order%color(1))
  !    order%color(1)%b_num = block_num
  !    call sequantial_ord(order, Apr_reord, Apr, dble(Apr%nonzero-DCmat%ndl_rm)/dble(block_num))
  ! else
  if(block_num == 1) then
     ! write(*, *) 'Last ordering upper side', ig
     Apr_reord = Apr
     Apr_reord%ndl_rm = DCmat%ndl_rm
     order%num_threads = 1
     order%c_num = 1
     max_threads = 1
     !$ max_threads = omp_get_max_threads()
     allocate(order%color(1), order%basep_thread(max_threads*3))
     allocate(order%color(1)%iblock(1))
     order%color(1)%b_num = 1
     order%color(1)%iblock(1)%stat = 1
     order%color(1)%iblock(1)%end = Apr_reord%nd
     order%color(1)%thread(1:max_threads) => order%basep_thread(1:max_threads)
     order%thread(1:max_threads) => order%basep_thread(max_threads+1:max_threads*2)
     order%thread_rmnd(1:max_threads) => order%basep_thread(max_threads*2+1:max_threads*3)
     order%color(1)%thread(:)%stat = 1
     order%color(1)%thread(1)%end = 1
     order%thread(1)%stat = 1
     order%thread(1)%end = Apr_reord%nd-Apr_reord%ndl_rm
     write(*, *) 'Check', Apr_reord%nd, Apr_reord%ndl_rm
     order%thread_rmnd(1)%stat = Apr_reord%ndl_rm+1
     order%thread_rmnd(1)%end = Apr_reord%nd
     if(max_threads /= 1) then
        order%color(1)%thread(2:max_threads)%end = 0
        order%thread(2:max_threads)%stat = 1
        order%thread(2:max_threads)%end = 0
        order%thread_rmnd(2:max_threads)%stat = 1
        order%thread_rmnd(2:max_threads)%end = 0
     endif
  else
     ! write(*, *) 'Last ordering downer side', ig
     block_list%ndl_rm = DCmat%ndl_rm
     block_list%val_rm(1:DCmat%ndl_rm) => DCmat%val_rm(1:DCmat%ndl_rm)
     order%num_threads = num_threads
     if(ntimes_block == 1) then
        call reordApr(order, Apr_reord, color_blist, block_list, Apr)
     else
        call reordApr_ntimes_block(order, Apr_reord, color_blist, block_list, Apr, ntimes_block)
     endif
  endif
  ! endif

  ! call block_greedy(block_list, Apr, num_elem, block_size)
  ! call block_aggregation_rule(block_list, Apr, Dcmat, num_elem, block_size, param)

  ! write(*, *) objval2
  ! write(*, *)
  ! write(*, *) sub_part(:)

  ! stop

  !allocate(order%color(order%c_num))


end subroutine ordering_block

subroutine create_neighbor_graph(neighbor, inputDC, Apr)
  use m_primapr
  use mod_mbmc
  implicit none
  type(neighbor_mat), intent(inout) :: neighbor
  type(primdc), intent(inout) :: inputDC
  type(primapr), intent(in) :: Apr
  integer, allocatable :: tmp_i1(:), tmp_i2(:)
  double precision, allocatable :: tmp_r1(:)
  integer count, rev_ind, i, it, j, jt, k

  allocate(neighbor%row(inputDC%nd), tmp_i1(inputDC%nd), tmp_i2(inputDC%nd), tmp_r1(inputDC%nd))
  neighbor%nd = inputDC%nd

  ! write(*, *) 'Check3', Apr%nd-inputDC%ndl_rm, sum(inputDC%row(:)%ndl_all)

  tmp_i1(:) = 0
  tmp_r1(:) = 0.0d0
  neighbor%total = 0
  do k = 1, inputDC%nd
     inputDC%row(k)%nonzero = 0
     count = 1
     do j = 1, inputDC%row(k)%ndl_all
        jt = inputDC%row(k)%val_all(j)
        do i = 1, Apr%aprl(jt)%ndl
           it = Apr%aprl(jt)%lct(i)
           rev_ind = inputDC%rev_ind(it)
           if(rev_ind /= k) then
              if(tmp_i1(rev_ind) == 0) then
                 tmp_i2(count) = rev_ind
                 count = count + 1
              endif
              tmp_i1(rev_ind) = tmp_i1(rev_ind) + 1
              tmp_r1(rev_ind) = tmp_r1(rev_ind) + dabs(Apr%aprl(jt)%avl(i)) / sqrt(Apr%Aprl(jt)%avl(1) * Apr%Aprl(it)%avl(1))
           endif
           inputDC%row(k)%nonzero = inputDC%row(k)%nonzero + Apr%aprl(jt)%ndl + 1
        enddo
     enddo

     neighbor%row(k)%ndl = count-1
     allocate(neighbor%row(k)%val(count-1), neighbor%row(k)%lct(count-1), neighbor%row(k)%line(count-1))
     neighbor%row(k)%lct(1:count-1) = tmp_i2(1:count-1)
     neighbor%total = neighbor%total + count-1
     ! write(*, *) 'Check1', k, neighbor%row(k)%ndl

     do j = 1, count-1
        jt = neighbor%row(k)%lct(j)
        neighbor%row(k)%line(j) = tmp_i1(jt)
        tmp_i1(jt) = 0
        neighbor%row(k)%val(j) = tmp_r1(jt)
        tmp_r1(jt) = 0.0d0
     enddo
  enddo

end subroutine create_neighbor_graph

subroutine create_neighbor_f_PartGraph(block_list, part, inputDC)
  use m_primapr
  implicit none
  type(primdc), intent(inout) :: block_list
  type(primdc), intent(in) :: inputDC
  integer, intent(in) :: part(inputDC%nd)
  integer i, it, j, jt, count, countd

  block_list%row(:)%ndl_all = 0
  do i = 1, inputDC%nd
     it = part(i)
     block_list%row(it)%ndl_all = block_list%row(it)%ndl_all + inputDC%row(i)%ndl_all
  enddo

  count = 1
  do i = 1, block_list%nd
     block_list%row(i)%val_all(1:block_list%row(i)%ndl_all) => block_list%basep_val(count:count+block_list%row(i)%ndl_all-1)
     count = count + block_list%row(i)%ndl_all
     block_list%row(i)%ndl_all = 0
  enddo

  do j = 1, inputDC%nd
     jt = part(j)
     count = block_list%row(jt)%ndl_all
     countd = inputDC%row(j)%ndl_all
     block_list%row(jt)%val_all(count+1:count+countd) = inputDC%row(j)%val_all(1:countd)
     block_list%row(jt)%ndl_all = block_list%row(jt)%ndl_all + countd
     do i = 1, countd
        it = inputDC%row(j)%val_all(i)
        block_list%rev_ind(it) = jt
     enddo
  enddo

end subroutine create_neighbor_f_PartGraph

logical function check_balance(neighbor_block, Apr, DCmat, block_list, ntimes_block)
  use m_primapr
  implicit none
  type(neighbor_mat), intent(in) :: neighbor_block
  type(primapr), intent(in) :: Apr
  type(primdc), intent(in) :: DCmat, block_list
  integer, intent(in) :: ntimes_block
  integer, allocatable :: tmp(:), usage_mem(:), amt_calc(:)
  logical, allocatable :: check_mat(:), mem(:)
  integer i, it, j, jt, k, count

  allocate(mem(Apr%nd), check_mat(Apr%nd), tmp(Apr%nd))
  allocate(usage_mem(block_list%nd), amt_calc(block_list%nd))
  check_mat(:) = .false.
  mem(:) = .false.

  do i = 1, DCmat%ndl_rm
     it = DCmat%val_rm(i)
     check_mat(it) = .true.
  enddo

  usage_mem(:) = 0
  amt_calc(:) = 0
  do k = 1, block_list%nd
     count = 1
     do j = 1, block_list%row(k)%ndl_all
        jt = block_list%row(k)%val_all(j)
        if(check_mat(jt)) then
           write(*, *) jt, 'is already added to other block.', k
        else
           check_mat(jt) = .true.
        endif
        amt_calc(k) = amt_calc(k) +  Apr%aprl(jt)%ndl*2 + 2
        usage_mem(k) = usage_mem(k) + Apr%aprl(jt)%ndl*3 + 5
        do i = 1, Apr%aprl(jt)%ndl
           it = Apr%aprl(jt)%lct(i)
           if(.not. mem(it)) then
              mem(it) = .true.
              tmp(count) = it
              count = count + 1
           endif
        enddo
     enddo
     usage_mem(k) = (usage_mem(k) + (count-1)*2) * 8
     do j = 1, count-1
        jt = tmp(j)
        mem(j) = .false.
     enddo
  enddo
        
  write(*, *) 'Checking routine. Check some datas of each block.'
  write(*, '(a34, i9, a8, i9, a8, i9)') 'Amount of calculation. Average is ', sum(amt_calc(:)/block_list%nd), ' Max is ', maxval(amt_calc(:)), ' Min is ', minval(amt_calc(:))
  write(*, '(a25, i9, a8, i9, a8, i9)') 'Usage memory. Average is ', nint(sum(dble(usage_mem(:))/dble(block_list%nd))), ' Max is ', maxval(usage_mem(:)), ' Min is ', minval(usage_mem(:))
  write(*, '(a28, i9, a8, i9, a8, i9)') 'Number of links. Average is ', sum(neighbor_block%row(:)%ndl)/block_list%nd, ' Max is ', maxval(neighbor_block%row(:)%ndl), ' Min is ', minval(neighbor_block%row(:)%ndl)

  ! write(*, *) maxval(amt_calc(:)), minval(amt_calc(:)), sum(amt_calc(:)/block_list%nd), maxval(amt_calc(:))-minval(amt_calc(:)), dble(maxval(amt_calc(:))-minval(amt_calc(:))) / dble(sum(amt_calc(:)/block_list%nd))
  if(dble(maxval(amt_calc(:))-minval(amt_calc(:))) / dble(sum(amt_calc(:)/block_list%nd)) > 0.1d0) then
     check_balance = .false.
  else
     check_balance = .true.
  endif

  write(*, *) 'Result of balance checking ', check_balance
  ! write(*, *) 'Check unbalance.', sum(block_list%row(:)%nonzero), Apr%nonzero-DCmat%ndl_rm, sum(block_list%row(:)%nonzero)/block_list%nd, minval(block_list%row(:)%nonzero), maxval(block_list%row(:)%nonzero)
  
end function check_balance

logical function coloring_alg1(color_blist, neighbor_block, Apr, num_threads, ntimes_block)
  use m_primapr
  implicit none
  type(list_block), intent(inout) :: color_blist
  type(neighbor_mat), intent(in) :: neighbor_block
  ! type(primdc), intent(in) :: block_list
  type(primapr), intent(in) :: Apr
  integer, intent(inout) :: num_threads
  integer, intent(in) :: ntimes_block
  type ord
     integer, allocatable :: list(:)
  end type ord
  type(ord), allocatable :: color(:)
  integer, allocatable :: tmp_color(:), block_color(:)
  logical, allocatable :: logic_color(:)
  integer max_color, count, i, it, icolor, j, jt, jcolor, k, kt, color_block, tmp, min_color, min, min_tmp, num_threads_back
  logical flag1, flag2, flag, flag3

  num_threads_back = num_threads
  max_color = maxval(neighbor_block%row(:)%ndl)*2
  if(max_color == 0) max_color = 1
  min_tmp = max_color
  allocate(tmp_color(max_color), block_color(neighbor_block%nd), logic_color(0:max_color), color(max_color))

  ! if(neighbor_block%nd == 280) write(*, *) 'Check maxcolor', max_color

  flag2 = .true.
  do while(flag2)
     flag2 = .false.

     flag1 = .true.
     do while(flag1)
        flag1 = .false.

        tmp_color(:) = 0
        logic_color(:) = .true.
        block_color(:) = 0

        max_color = 0
        do j = 1, neighbor_block%nd
           do i = 1, neighbor_block%row(j)%ndl
              it = neighbor_block%row(j)%lct(i)
              icolor =  block_color(it)
              logic_color(icolor) = .false.
           enddo
           ! if(Apr%nd == 65921) write(*, *) 'Check', max_color

           icolor = 0
           jcolor = 0
           color_block = num_threads
           min_color = neighbor_block%nd
           do i = 1, max_color
              if(logic_color(i)) then
                 tmp = mod(tmp_color(i), num_threads)
                 if(tmp /= 0 .and. color_block > tmp) then
                    icolor = i
                    color_block = tmp
                 endif
                 if(min_color > tmp_color(i)) then
                    min_color = tmp_color(i)
                    jcolor = i
                 endif
              endif
           enddo
           if(icolor /= 0) then
              ! if(neighbor_block%nd == 280) write(*, *) 'Check1', max_color, icolor, j, tmp_color(icolor)
              block_color(j) = icolor
              tmp_color(icolor) = tmp_color(icolor) + 1
              color(icolor)%list(tmp_color(icolor)) = j
           elseif(jcolor /= 0) then
              block_color(j) = jcolor
              tmp_color(jcolor) = tmp_color(jcolor) + 1
              color(jcolor)%list(tmp_color(jcolor)) = j
           endif

           if(block_color(j) == 0) then
              max_color = max_color + 1
              allocate(color(max_color)%list(neighbor_block%nd))
              block_color(j) = max_color
              tmp_color(max_color) = 1
              color(max_color)%list(1) = j
           endif

           do i = 1, neighbor_block%row(j)%ndl
              it = neighbor_block%row(j)%lct(i)
              icolor =  block_color(it)
              logic_color(icolor) = .true.
           enddo
        enddo

        do j = 1, max_color
           if(tmp_color(j) < num_threads) then
              if(ntimes_block == 1) then
                 write(*, *) 'There is a color which has less than "num_threads" blocks. Retry.'
                 coloring_alg1 = .false.
                 return !Now I cut retry routine in this subroutine.
              else
                 num_threads = num_threads - 1
                 do i = 1, max_color
                    deallocate(color(i)%list)
                 enddo
                 if(num_threads < 1) num_threads = 1
                 flag1 = .true.
                 exit
              endif
           endif
        enddo
           ! write(*, *) 'Check', i, tmp_color(1:max_color), num_threads

           ! write(*, *) 'Before.', max_color, tmp_color(1:max_color)
           ! write(*, *)
           ! do i = 1, 1!max_color
           !    write(*, *) 'Block of color ', i, ' : ', color(i)%list(1:tmp_color(i))
           ! enddo

        flag = .true.
        do i = 1, max_color
           if(mod(tmp_color(i), num_threads) /= 0) then
              flag = .false.
              exit
           endif
        enddo

        if(.not. flag) then
           !Balancing. This is uncertain algorithm.
           max_color = max_color + 1
           do i = 1, max_color-1
              tmp_color(max_color) = tmp_color(max_color) + mod(tmp_color(i), num_threads)
           enddo
           allocate(color(max_color)%list(tmp_color(max_color))) 

           tmp = 1
           do k = 1, tmp_color(max_color)
              do while(mod(tmp_color(tmp), num_threads) == 0)
                 if(tmp == max_color-1) then
                    tmp = 1
                 else
                    tmp = tmp + 1
                 endif
              enddo
              ! write(*, *) 'tmp is ', tmp, k

              min = min_tmp
              do j = 1, tmp_color(tmp)
                 jt = color(tmp)%list(j)
                 if(min > neighbor_block%row(jt)%ndl) then
                    flag = .true.
                    do i = 1, neighbor_block%row(jt)%ndl
                       if(block_color(neighbor_block%row(jt)%lct(i)) == max_color) then
                          flag = .false.
                          exit
                       endif
                    enddo
                    if(flag) then
                       min = neighbor_block%row(jt)%ndl
                       color(max_color)%list(k) = jt
                       ! if(tmp == 1) write(*, *) 'Check', min, jt
                    endif
                 endif
              enddo
              ! write(*, *) 'Choosed block number is ', color(max_color)%list(k), k

              if(min == min_tmp) then
                 if(ntimes_block == 1) then
                    write(*, *) 'Balancing routine does not work well. Retry'
                    coloring_alg1 = .false.
                    return
                 else
                    num_threads = num_threads - 2
                    if(num_threads < 1) num_threads = 1
                    if(num_threads == 1) then
                       num_threads = num_threads_back
                       coloring_alg1 = .false.
                       return
                    endif
                    flag2 = .true.
                    do j = 1, max_color
                       deallocate(color(j)%list)
                    enddo
                    exit
                 endif
              else
                 kt = color(max_color)%list(k)
                 block_color(kt) = max_color
                 tmp_color(tmp) = tmp_color(tmp) - 1
                 jt = 1
                 do while(color(tmp)%list(jt) /= kt)
                    jt = jt + 1
                 enddo
                 ! write(*, *) 'jt is', jt, k, tmp_color(tmp)
                 do j = jt, tmp_color(tmp)
                    ! if(color(tmp)%list(j+1) > neighbor_block%nd .or. color(tmp)%list(j+1) < 1) then
                    !    write(*, *) 'Error.',color(tmp)%list(j+1) 
                    !    stop
                    ! endif
                    color(tmp)%list(j) = color(tmp)%list(j+1)
                 enddo
              endif

              ! if(tmp == 1) then
              !    write(*, *) 'Block of color ', tmp, ' : ', color(tmp)%list(1:tmp_color(tmp))
              ! endif

              if(tmp == max_color - 1) then
                 tmp = 1
              else
                 tmp = tmp + 1
              endif

              ! stop
           enddo

        endif

     enddo
  enddo

  write(*, *) 'Max_color and number of block in each color', max_color, tmp_color(1:max_color)
  ! write(*, *)
  ! write(*, *) 'Color of each block.', block_color(:)
  ! write(*, *)
  ! do i = 1, max_color
  !    write(*, *) 'Block of color ', i, ' : ', color(i)%list(1:tmp_color(i))
  ! enddo

  !From here is check routine
  deallocate(logic_color)
  allocate(logic_color(neighbor_block%nd))
  logic_color(:) = .false.
  do k = 1, max_color
     do j = 1, tmp_color(k)
        jt = color(k)%list(j)
        if(logic_color(jt)) write(*, *) jt, 'is already added to other color.', k
        logic_color(jt) = .true.
        do i = 1, neighbor_block%row(jt)%ndl
           it = neighbor_block%row(jt)%lct(i)
           if(block_color(it) == k) write(*, *) 'Same color is adjancent to ', jt, k
        enddo
     enddo
  enddo

  !Copy routine
  color_blist%c_num = max_color
  allocate(color_blist%row(max_color), color_blist%basep_list(neighbor_block%nd))
  count = 1
  do i = 1, max_color
     color_blist%row(i)%ndl = tmp_color(i)
     color_blist%row(i)%list(1:tmp_color(i)) => color_blist%basep_list(count:count+tmp_color(i)-1)
     color_blist%row(i)%list(1:tmp_color(i)) = color(i)%list(1:tmp_color(i))
     count = count + tmp_color(i)
     deallocate(color(i)%list)
  enddo

  coloring_alg1 = .true.
    
end function coloring_alg1

subroutine reordApr(order, Apr_reord, color_blist, block_list, Apr)
  use m_primapr
  implicit none
  type(ordering), intent(inout) :: order
  type(primapr), intent(inout) :: Apr_reord
  type(list_block), intent(in) :: color_blist
  type(primdc), intent(inout) :: block_list
  type(primapr), intent(in) :: Apr
  integer i, it, itt, j, jt, k, kt, count1, count2, base, max_threads, tmp
  logical, allocatable :: logic_apr(:)

  order%c_num = color_blist%c_num
  Apr_reord%nd = Apr%nd
  allocate(order%color(order%c_num), order%basep_block(block_list%nd), Apr_reord%aprl(Apr_reord%nd))

  count1 = 1
  count2 = 1
  do k = 1, order%c_num
     kt = color_blist%row(k)%ndl
     order%color(k)%iblock(1:kt) => order%basep_block(count2:count2+kt-1)
     count2 = count2 + kt
     order%color(k)%b_num = kt
     do j = 1, kt
        jt = color_blist%row(k)%list(j)
        order%color(k)%iblock(j)%stat = count1
        do i = 1, block_list%row(jt)%ndl_all
           it = block_list%row(jt)%val_all(i)
           itt = Apr%aprl(it)%ndl
           Apr_reord%aprl(count1) = Apr%aprl(it)
           ! Apr_reord%aprl(count1)%ndl = Apr%aprl(it)%ndl
           ! Apr_reord%aprl(count1)%ado = Apr%aprl(it)%ado
           ! Apr_reord%aprl(count1)%lct(1:itt) => Apr%aprl(it)%lct(1:itt)
           ! Apr_reord%aprl(count1)%avl(1:itt) => Apr%aprl(it)%avl(1:itt)
           ! base = sum(Apr%aprl(1:it)%ndl)
           ! Apr_reord%aprl(count1)%lct(1:itt) => Apr%basep_lct(base:base+itt-1)
           ! Apr_reord%aprl(count1)%avl(1:itt) => Apr%basep_avl(base:base+itt-1)
           count1 = count1 + 1
        enddo
        order%color(k)%iblock(j)%end = count1-1
     enddo
  enddo

  Apr_reord%ndl_rm = block_list%ndl_rm
  count1 = Apr_reord%nd - Apr_reord%ndl_rm + 1
  do i = 1, Apr_reord%ndl_rm
     it = block_list%val_rm(i)
     Apr_reord%aprl(count1) = Apr%aprl(it)
     count1 = count1 + 1
  enddo

  !Assign range for each thread.
  max_threads = 1
  ! max_threads = 28
  !$ max_threads = omp_get_max_threads()
  allocate(order%basep_thread((order%c_num+2)*max_threads))
  do j = 1, order%c_num
     order%color(j)%thread(1:max_threads) => order%basep_thread((j-1)*max_threads+1:j*max_threads)
     do i = 1, order%num_threads
        order%color(j)%thread(i)%stat = (order%color(j)%b_num / order%num_threads) * (i - 1) + 1
        order%color(j)%thread(i)%end = (order%color(j)%b_num / order%num_threads) * i
     enddo
     do i = order%num_threads+1, max_threads
        order%color(j)%thread(i)%stat = 1
        order%color(j)%thread(i)%end = 0
     enddo
  enddo

  ! write(*, *) 'here', order%c_num, max_threads, mod(Apr%nd, max_threads), Apr%nd

  order%thread(1:max_threads) => order%basep_thread(order%c_num*max_threads+1:(order%c_num+1)*max_threads)
  tmp = mod(Apr%nd, max_threads)
  do i = 1, tmp
     order%thread(i)%stat = ((Apr%nd-tmp)/max_threads+1) * (i-1) + 1
     order%thread(i)%end = ((Apr%nd-tmp)/max_threads+1) * i
  enddo
  do i = tmp+1, max_threads
     order%thread(i)%stat = ((Apr%nd-tmp)/max_threads) * (i-1) + 1 + tmp
     order%thread(i)%end = ((Apr%nd-tmp)/max_threads) * i + tmp
  enddo
  ! write(*, *) 'here', mod(Apr_reord%ndl_rm, max_threads), Apr_reord%ndl_rm
  order%thread_rmnd(1:max_threads) => order%basep_thread((order%c_num+1)*max_threads+1:(order%c_num+2)*max_threads)
  tmp = mod(Apr_reord%ndl_rm, max_threads)
  do i = 1, tmp
     ! write(*, *) i
     order%thread_rmnd(i)%stat = ((Apr_reord%ndl_rm-tmp)/max_threads+1) * (i-1) + 1 + Apr_reord%nd-Apr_reord%ndl_rm
     order%thread_rmnd(i)%end = ((Apr_reord%ndl_rm-tmp)/max_threads+1) * i + Apr_reord%nd-Apr_reord%ndl_rm
  enddo
  do i = tmp+1, max_threads
     ! write(*, *) i
     order%thread_rmnd(i)%stat = ((Apr_reord%ndl_rm-tmp)/max_threads) * (i-1) + 1 + tmp + Apr_reord%nd-Apr_reord%ndl_rm
     order%thread_rmnd(i)%end = ((Apr_reord%ndl_rm-tmp)/max_threads) * i + tmp + Apr_reord%nd-Apr_reord%ndl_rm
  enddo

  ! write(*, *) 'Order', order%thread_rmnd(:)

  ! do j = 1, order%c_num
  !    do i = 1, max_threads
  !       write(*, *) order%color(j)%thread(i)%stat, order%color(j)%thread(i)%end
  !    enddo
  ! enddo
  
  ! tmp = (Apr_reord%ndl-Apr_reord%ndl_rm)

  !From here is Checking routine.
  ! write(*, *) 'order%color%bnum', order%color(:)%b_num
  ! write(*, *) 'order%color%iblock%stat and end'
  ! do j = 1, order%c_num
  !    write(*, *) 'Color is ', j
  !    do i = 1, order%color(j)%b_num
  !       write(*, *) 'block id is ', i, 'stat ', order%color(j)%iblock(i)%stat, 'end ', order%color(j)%iblock(i)%end
  !    enddo
  ! enddo

  ! write(*, *) 'Last', Apr%nd

  allocate(logic_apr(Apr_reord%nd))
  logic_apr(:) = .false.
  do k = 1, order%c_num
     do j = 1, order%color(k)%b_num
        do i = order%color(k)%iblock(j)%stat, order%color(k)%iblock(j)%end
           it = Apr_reord%aprl(i)%lct(1)
           logic_apr(it) = .true.
        enddo
     enddo
  enddo

  do i = 1, Apr%nd
     if((.not. logic_apr(i)) .and. Apr%aprl(i)%ndl /= 1) write(*, *) i, 'is not added in Apr_reord.'
  enddo

end subroutine reordApr

subroutine reordApr_ntimes_block(order, Apr_reord, color_blist, block_list, Apr, ntimes_block)
  use m_primapr
  implicit none
  type(ordering), intent(inout) :: order
  type(primapr), intent(inout) :: Apr_reord
  type(list_block), intent(in) :: color_blist
  type(primdc), intent(inout) :: block_list
  type(primapr), intent(in) :: Apr
  integer, intent(in) :: ntimes_block
  integer i, it, itt, j, jt, k, kt, count1, count2, base, max_threads, tmp
  logical, allocatable :: logic_apr(:)

  order%c_num = color_blist%c_num
  Apr_reord%nd = Apr%nd
  allocate(order%color(order%c_num), order%basep_block(block_list%nd), Apr_reord%aprl(Apr_reord%nd))

  count1 = 1
  count2 = 1
  do k = 1, order%c_num
     kt = color_blist%row(k)%ndl
     order%color(k)%iblock(1:kt) => order%basep_block(count2:count2+kt-1)
     count2 = count2 + kt
     order%color(k)%b_num = kt
     do j = 1, kt
        jt = color_blist%row(k)%list(j)
        order%color(k)%iblock(j)%stat = count1
        do i = 1, block_list%row(jt)%ndl_all
           it = block_list%row(jt)%val_all(i)
           itt = Apr%aprl(it)%ndl
           Apr_reord%aprl(count1) = Apr%aprl(it)
           ! Apr_reord%aprl(count1)%ndl = Apr%aprl(it)%ndl
           ! Apr_reord%aprl(count1)%ado = Apr%aprl(it)%ado
           ! Apr_reord%aprl(count1)%lct(1:itt) => Apr%aprl(it)%lct(1:itt)
           ! Apr_reord%aprl(count1)%avl(1:itt) => Apr%aprl(it)%avl(1:itt)
           ! base = sum(Apr%aprl(1:it)%ndl)
           ! Apr_reord%aprl(count1)%lct(1:itt) => Apr%basep_lct(base:base+itt-1)
           ! Apr_reord%aprl(count1)%avl(1:itt) => Apr%basep_avl(base:base+itt-1)
           count1 = count1 + 1
        enddo
        order%color(k)%iblock(j)%end = count1-1
     enddo
  enddo

  Apr_reord%ndl_rm = block_list%ndl_rm
  count1 = Apr_reord%nd - Apr_reord%ndl_rm + 1
  do i = 1, Apr_reord%ndl_rm
     it = block_list%val_rm(i)
     Apr_reord%aprl(count1) = Apr%aprl(it)
     count1 = count1 + 1
  enddo

  !Assign range for each thread.
  max_threads = 1
  ! max_threads = 28
  !$ max_threads = omp_get_max_threads()
  allocate(order%basep_thread((order%c_num+2)*max_threads))
  do j = 1, order%c_num
     order%color(j)%thread(1:max_threads) => order%basep_thread((j-1)*max_threads+1:j*max_threads)
     do i = 1, order%num_threads
        order%color(j)%thread(i)%stat = (order%color(j)%b_num / order%num_threads) * (i - 1) + 1
        order%color(j)%thread(i)%end = (order%color(j)%b_num / order%num_threads) * i
     enddo
     do i = order%num_threads+1, max_threads
        order%color(j)%thread(i)%stat = 1
        order%color(j)%thread(i)%end = 0
     enddo
  enddo

  ! write(*, *) 'here', order%c_num, max_threads, mod(Apr%nd, max_threads), Apr%nd

  order%thread(1:max_threads) => order%basep_thread(order%c_num*max_threads+1:(order%c_num+1)*max_threads)
  tmp = mod(Apr%nd, max_threads)
  do i = 1, tmp
     order%thread(i)%stat = ((Apr%nd-tmp)/max_threads+1) * (i-1) + 1
     order%thread(i)%end = ((Apr%nd-tmp)/max_threads+1) * i
  enddo
  do i = tmp+1, max_threads
     order%thread(i)%stat = ((Apr%nd-tmp)/max_threads) * (i-1) + 1 + tmp
     order%thread(i)%end = ((Apr%nd-tmp)/max_threads) * i + tmp
  enddo
  ! write(*, *) 'here', mod(Apr_reord%ndl_rm, max_threads), Apr_reord%ndl_rm
  order%thread_rmnd(1:max_threads) => order%basep_thread((order%c_num+1)*max_threads+1:(order%c_num+2)*max_threads)
  tmp = mod(Apr_reord%ndl_rm, max_threads)
  do i = 1, tmp
     ! write(*, *) i
     order%thread_rmnd(i)%stat = ((Apr_reord%ndl_rm-tmp)/max_threads+1) * (i-1) + 1 + Apr_reord%nd-Apr_reord%ndl_rm
     order%thread_rmnd(i)%end = ((Apr_reord%ndl_rm-tmp)/max_threads+1) * i + Apr_reord%nd-Apr_reord%ndl_rm
  enddo
  do i = tmp+1, max_threads
     ! write(*, *) i
     order%thread_rmnd(i)%stat = ((Apr_reord%ndl_rm-tmp)/max_threads) * (i-1) + 1 + tmp + Apr_reord%nd-Apr_reord%ndl_rm
     order%thread_rmnd(i)%end = ((Apr_reord%ndl_rm-tmp)/max_threads) * i + tmp + Apr_reord%nd-Apr_reord%ndl_rm
  enddo

  ! write(*, *) 'Order', order%thread_rmnd(:)

  ! do j = 1, order%c_num
  !    do i = 1, max_threads
  !       write(*, *) order%color(j)%thread(i)%stat, order%color(j)%thread(i)%end
  !    enddo
  ! enddo
  
  ! tmp = (Apr_reord%ndl-Apr_reord%ndl_rm)

  !From here is Checking routine.
  ! write(*, *) 'order%color%bnum', order%color(:)%b_num
  ! write(*, *) 'order%color%iblock%stat and end'
  ! do j = 1, order%c_num
  !    write(*, *) 'Color is ', j
  !    do i = 1, order%color(j)%b_num
  !       write(*, *) 'block id is ', i, 'stat ', order%color(j)%iblock(i)%stat, 'end ', order%color(j)%iblock(i)%end
  !    enddo
  ! enddo

  ! write(*, *) 'Last', Apr%nd

  allocate(logic_apr(Apr_reord%nd))
  logic_apr(:) = .false.
  do k = 1, order%c_num
     do j = 1, order%color(k)%b_num
        do i = order%color(k)%iblock(j)%stat, order%color(k)%iblock(j)%end
           it = Apr_reord%aprl(i)%lct(1)
           logic_apr(it) = .true.
        enddo
     enddo
  enddo

  do i = 1, Apr%nd
     if((.not. logic_apr(i)) .and. Apr%aprl(i)%ndl /= 1) write(*, *) i, 'is not added in Apr_reord.'
  enddo

end subroutine reordApr_ntimes_block

subroutine sequantial_ord(order, Apr_reord, Apr, threashold)
  use m_primapr
  implicit none
  type(ordering), intent(inout) :: order
  type(primapr), intent(inout) :: Apr_reord
  type(primapr), intent(in) :: Apr
  double precision, intent(in) :: threashold

  
end subroutine sequantial_ord
