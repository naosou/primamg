!C**************************************************************************
!C  This file includes basic routines for arithmetic of matrices
!C  which are represented by apr format
!C  created by Akihiro Ida at Kyoto University on Jly 2014
!C  last modified Akihiro Ida on Nov 2014
!C**************************************************************************
module m_primamg
  use m_primapr

#ifndef __INTEL_COMPILER
  use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI
#else
  use lapack95, only : GETRF, GETRI
#endif

!*** type :: primaprccs
! type :: primaprccs
!  integer nm ! Number of corse-grids
!  type(primapr),pointer :: apr(:)=>null()
!  type(primapr),pointer :: tfc(:)=>null()
!  type(primapr),pointer :: tcf(:)=>null()
!  type(primvec),pointer :: uc(:)=>null()
!  type(primvec),pointer :: bc(:)=>null()
!  type(primvec),pointer :: rc(:)=>null()
!  real*8 :: param(20) ! parameters for AMG
 
! end type primaprccs
end module m_primamg
 
!***construct_corse_matrices
subroutine construct_corse_matrices(amcf,amccs)
  use m_primamg
  ! use mod_mbmc
  implicit none
  type(primapr), intent(in) :: amcf
  type(primaprccs), intent(inout) :: amccs
  integer lcorsest, i_oldnd, ig, ndc, me_omp, max_threads, id_thread

  me_omp = 1
  max_threads = 1
  !$ me_omp = omp_get_thread_num() + 1
  !$ max_threads = omp_get_num_threads()

  if(me_omp == 1) then
     lcorsest=amccs%param(1)
     amccs%nm=amccs%param(2)
     allocate(amccs%apr(amccs%nm),amccs%Apr_reord(amccs%nm),amccs%tfc(amccs%nm),amccs%tcf(amccs%nm),amccs%order(amccs%nm))
     amccs%apr(1) = amcf
  endif
  ! !$OMP barrier

  ! do i = 1, amccs%apr(1)%nd
  !    amccs%apr(1)%aprl(i)%diag_ind = i
  ! enddo
  
  i_oldnd = amccs%apr(1)%nd
  do ig=1,amccs%nm-1
     call construct_a_corse(amccs%apr(ig+1),amccs%tfc(ig),amccs%tcf(ig),amccs%apr(ig),amccs%apr_reord(ig),amccs%order(ig),ig,amccs%param,amccs%num_params)
     if(me_omp == 1) then
        if(amccs%apr(ig+1)%nd <lcorsest) then
           amccs%nm=ig+1
           exit
        endif
        
        if(amccs%apr(ig+1)%nd == i_oldnd) then
           amccs%nm=ig
           exit
        else
           i_oldnd = amccs%apr(ig)%nd
        endif
     endif
     ! !$OMP barrier
     ! stop
  enddo
  
  if(amccs%param(18) == 1 .and. me_omp == 1) then
     write(*, *) 'Coarsest solver is direct of lapack.'
     call construct_invA(amccs%apr(amccs%nm), amccs%apr_dense) !
  endif

  allocate(amccs%order(amccs%nm)%basep_thread(max_threads*3))
  amccs%order(amccs%nm)%thread(1:max_threads) => amccs%order(amccs%nm)%basep_thread(max_threads+1:max_threads*2)
  do id_thread = 1, max_threads
     call init_bound(amccs%apr(amccs%nm)%nd, max_threads, id_thread, amccs%order(amccs%nm)%thread(id_thread)%stat, amccs%order(amccs%nm)%thread(id_thread)%end)
  enddo

  ! amccs%nm = 3
  if(me_omp == 1) then
     write(*, '(a25, i2, a29, i2, a19, i2, a1)') 'Maximum level for AMG is ', amccs%nm, '. Number of pre-smoothing is ', int(amccs%param(11)), '. pre-smoothing is ', int(amccs%param(12)), '.'
  
     allocate(amccs%uc(amccs%nm),amccs%bc(amccs%nm),amccs%rc(amccs%nm))
     write(*, *) 'Check', amccs%apr(1)%nd
     allocate(amccs%rc(1)%v(amccs%apr(1)%nd))
     do ig=2,amccs%nm
        ndc=amccs%apr(ig)%nd
        allocate(amccs%uc(ig)%v(ndc),amccs%bc(ig)%v(ndc),amccs%rc(ig)%v(ndc))
     enddo
  end if
  ! !$OMP barrier

endsubroutine construct_corse_matrices

!***construct_a_corse
subroutine construct_a_corse(amcc,tfc,tcf,amcf,amcf_reord,order,ig,param,num_params)
  use m_primamg
  ! use mod_mbmc
  implicit none
  type(primapr),intent(in) :: amcf
  type(primapr),intent(inout) :: amcc,tfc,tcf,amcf_reord
  type(ordering), intent(inout) :: order
  integer, intent(in) :: ig, num_params
  double precision, intent(in) :: param(num_params)
  type(primdc), save :: DCmat ! Aggregations on the grid of amcf
  double precision time_s, time_e
  integer me_omp
  character dummy
  logical flag_dynamic
  integer :: fo = 10
  character(1000) :: fname
  integer il, it
  
  me_omp = 1
  !$ me_omp = omp_get_thread_num() + 1

  if(me_omp == 1) then
     if(param(20) == 1.0d0) then
        flag_dynamic = .true.
     else
        flag_dynamic = .false.
     endif
     write(*, *) 'Start aggregation. level=', ig
     time_s = omp_get_wtime()
     call gen_DC_mat(amcf, DCmat, param(3), param(4), flag_dynamic, param(21), ig) !Generate "Disjoint Covering" matrix.
     time_e = omp_get_wtime()
     write(*, *) 'End aggregation. Time is ', time_e-time_s
  endif
  ! !$OMP barrier
  ! if(ig == 2) stop
  ! if(ig >= 2) read(*, *) dummy
  
  ! call checking_cutpattern(amcf, DCmat, param(3), param(4), ig)
  
  if(me_omp == 1) then
     write(*, *) 'Start ordering.'
     time_s = omp_get_wtime()
     call ordering_block(order, amcf_reord, amcf, DCmat, param(3), param(4), param(15), param(16), param(17), ig) !Decide Ordering.
     time_e = omp_get_wtime()
     write(*, *) 'End ordering. Time is ', time_e-time_s
  endif
  ! !$OMP barrier

  if(me_omp == 1) then
     write(*, *) 'Start calculation of restriction and prolongation operator.'
     time_s = omp_get_wtime()
  ! endif
  ! !$OMP barrier
  call construct_transfer_functions(amcf,tfc,tcf,DCmat,param(5),param(6),param(3),param(4),ig)
  ! !$OMP barrier
  ! if(me_omp == 1) then
     time_e = omp_get_wtime()
     write(*, *) 'End calculation of restriction and prolongation operator. Time is ', time_e-time_s
  endif
  ! !$OMP barrier

  ! call reorder_matA(amcf,amcf_reord,DCmat)

  ! !$OMP barrier
  if(me_omp == 1) then
     write(*, *)'Start calculation of galerkin_operator.'
     time_s = omp_get_wtime()
  endif
  ! !$OMP barrier
  call galerkin_operator(amcc,tfc,tcf,amcf)
  ! !$OMP barrier
  if(me_omp == 1) then
     time_e = omp_get_wtime()
     write(*, *) 'End calculation of galerkin_operator. Time is ', time_e-time_s
  endif
  ! !$OMP barrier

#ifdef output_matrix
  write(*, *) 'Oputput matrix of level', ig
  write(fname, '(a, i1, a)') 'mat_amg_', ig, '.txt'
  open(fo, file=fname)
  write(fo, *) amcc%nd, amcc%nd, amcc%nonzero
  do il = 1, amcc%nd
     write(fo, *) il, il, amcc%aprl(il)%avl(1)
     do it=2,amcc%aprl(il)%ndl
        write(fo, *) il, amcc%aprl(il)%lct(it), amcc%aprl(il)%avl(it)
     enddo
  enddo
  close(fo)
#endif
  
  ! call test_mat_check(amcf, tfc, tcf, amcc)
  ! stop

endsubroutine construct_a_corse

subroutine construct_invA(apr, apr_dense)
  use m_primamg
  implicit none
  type(primapr),intent(in) :: apr
  type(primapr_dense),intent(out) :: apr_dense
  integer i, j
  integer me_omp
  integer, allocatable :: pivot(:)
  
  apr_dense%nd = apr%nd
  allocate(apr_dense%avl(apr_dense%nd, apr_dense%nd), pivot(apr_dense%nd))

  ! write(*, *) 'Size of dense matrix', apr_dense%nd, 'x', apr_dense%nd
  apr_dense%avl = 0.0d0
  do j = 1, apr%nd
     do i = 1, apr%aprl(j)%ndl
        apr_dense%avl(j, apr%aprl(j)%lct(i)) = apr%aprl(j)%avl(i)
        ! write(*, *) 'Check avl', apr%aprl(j)%avl(i)
     enddo
  enddo
  
  call GETRF(apr_dense%avl, pivot)
  call GETRI(apr_dense%avl, pivot)

end subroutine construct_invA

! !***aggrigate_on_fine-grid
! subroutine aggrigate_on_fine-grid(lagg,amcf)
!  type(primapr) :: amcf
!  integer :: lagg(:)
!  ndf=amcf%nd
 
! endsubroutine

! !***construct_transfer_functions
! subroutine construct_transfer_functions(tfc,tcf,dcmat,param)
!  type(primapr) :: tfc,tcf
!  type(primdc) :: DCmat
!  double precision, intent(in) :: param(:)

 
 
! endsubroutine

!********************************************************************************
!***vclycle
! subroutine vcycle(zu,amccs,amcf,zb)
subroutine vcycle(amccs)
  use m_primamg
  implicit none
  type(primaprccs), intent(inout) :: amccs
  integer nitr, itr_pr, itr_po, itr_ipr, itr_ipo, itr, ig
  double precision threshold, bnorm, res_norm
  double precision :: dummy(2) = 0.0d0
  
  interface
    
     subroutine residual(zr,aprl,zv,zb,nd,stat,end)
       use m_primamg
       type(primail),intent(in) ::  aprl(:)
       double precision, intent(in) :: zv(:),zb(:)
       double precision, intent(out) :: zr(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine residual
     
     subroutine restriction(zbc, opr_fc, zrf, nd, stat, end)
       use m_primamg
       type(primail), intent(in) :: opr_fc(:)
       double precision, intent(in) :: zrf(:)
       double precision, intent(out) :: zbc(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine restriction
     
     subroutine prolongation(zuf, opr_cf, zuc, nd, stat, end)
       use m_primamg
       type(primail), intent(in) :: opr_cf(:)
       double precision, intent(in) :: zuc(:)
       double precision, intent(inout) :: zuf(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine prolongation
     
     subroutine normalization(zuc, zbc, aprl, nd)
       use m_primamg
       double precision, intent(in) :: zbc(:)
       double precision, intent(inout) :: zuc(:)
       type(primail), intent(in) :: aprl(:)
       integer, intent(in) :: nd
     endsubroutine normalization
     
     subroutine solve_coarsest(zu, aprl, zb, zr, nd, nitr, threshold, ig_max)
       use m_primamg
       double precision, intent(in) :: zb(:)
       double precision, intent(in) :: threshold
       type(primail), intent(in)::  aprl(:)
       integer, intent(in) :: nd, nitr, ig_max
       double precision, intent(out) :: zu(:), zr(:)
     end subroutine solve_coarsest

  end interface
  
  nitr=amccs%param(7)
  itr_pr = int(amccs%param(9))
  itr_po = int(amccs%param(10))
  itr_ipr = int(amccs%param(11))
  itr_ipo = int(amccs%param(12))
  threshold = amccs%param(8)

 ! ndf=amcf%nd; ndc=amccs%apr(1)%nd
 ! puf=>zu; pbf=>zb; puc=>amccs%uc(1); pbc=>amccs%bc(1)
 ! call gs_ail(puf,amcf%aprl,pbf,ndf,nitr)
 ! call restriction(pbc,amcf%aprl,amccs%tfc(1),puf,pbf)

  call residual(amccs%rc(1)%v, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd) !Residual calculation
  
  bnorm = dot_product(amccs%rc(1)%v(:), amccs%rc(1)%v(:))
  bnorm = sqrt(bnorm)
  ! amccs%rc(1)%v(:) = 0.0d0

  do itr = 1, nitr
 
     do ig=1,amccs%nm-1
        ! ndf=amccs%apr(ig)%nd; ndc=amccs%apr(ig+1)%nd
        ! puf=>amccs%uc(ig); pbf=>amccs%bc(ig); puc=>amccs%uc(ig+1); pbc=>amccs%bc(ig+1)
        if(ig /= 1) amccs%uc(ig)%v(:) = 0.0d0
        call gs_ail(amccs%uc(ig)%v, amccs%apr(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr, dummy, ig) !Pre-smoothing
        ! write(*, *) 'after val', ig, sum(amccs%uc(ig)%v(:))
        ! write(*, *) 'Check aprl', amccs%apr(2)%aprl(1)%avl(:)
        call residual(amccs%rc(ig)%v, amccs%apr(ig)%aprl, amccs%uc(ig)%v, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd) !Residual
        ! write(*, *) 'after cal_res', ig, sum(amccs%rc(ig)%v(:))
        call restriction(amccs%bc(ig+1)%v, amccs%tfc(ig)%aprl, amccs%rc(ig)%v, amccs%tfc(ig)%nd, 1, amccs%tfc(ig+1)%nd) !Restriction
        ! write(*, *) 'after rest', ig, sum(amccs%bc(ig+1)%v(:))
     enddo
     call solve_coarsest(amccs%uc(amccs%nm)%v, amccs%apr(amccs%nm)%aprl, amccs%bc(amccs%nm)%v, amccs%rc(amccs%nm)%v, amccs%apr(amccs%nm)%nd &
                      , int(amccs%param(13)), amccs%param(14), amccs%nm)
     ! call normalization((puc,amccs%apr(amccs%nm),pbc)
     ! call prolongation(puf,amccs%tcf(amccs%nm),puc)
     do ig=amccs%nm-1,1,-1
        ! ndf=amccs%apr(ig-1)%nd; ndc=amccs%apr(ig)%nd
        ! puf=>amccs%uc(ig); pbf=>amccs%bc(ig-1); puc=>amccs%uc(ig); pbc=>amccs%bc(ig-1)
        ! call normalization(puc,amccs%apr(ig),pbc)
        call prolongation(amccs%uc(ig)%v, amccs%tcf(ig)%aprl, amccs%uc(ig+1)%v, amccs%tcf(ig)%nd, 1, amccs%tcf(ig+1)%nd) !Prolongation
        call gs_ail(amccs%uc(ig)%v, amccs%apr(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_po, dummy, ig) !Post-smoothing
     enddo
  
     call residual(amccs%rc(1)%v, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd) !Residual calculation
     res_norm = dot_product(amccs%rc(1)%v(:), amccs%rc(1)%v(:))
     res_norm = sqrt(res_norm)/bnorm
     write(*, '(a6, i3, a16, e12.5)') 'itr = ', itr, 'residual norm = ', res_norm
     if(res_norm <= threshold) then
        write(*, *) 'convarged'
        exit
     endif
   
  enddo

 ! call gs_ail(puf,amccs%apr(1)%aprl,pbf,ndf,nitr)
 ! call normalization(puf,amccs%apr(1),pbf)
 ! call prolongation(zu,amccs%tcf(1),puf)
 
endsubroutine vcycle

subroutine vcycle_symmetric(amccs, time)
  use m_primamg
  implicit none
  type(primaprccs), intent(inout) :: amccs
  double precision, intent(inout) :: time(amccs%nm+7)
  integer nitr, itr_pr, itr_po, itr_ipr, itr_ipo, itr, ig, itr_o
  double precision threshold, tmp_d
  double precision, save :: bnorm, res_norm
  integer i, clr, blc, stat, end, me_omp, diag
  double precision, allocatable :: test_array(:)
  
  interface
    
     subroutine residual(zr,aprl,zv,zb,nd,stat,end)
       use m_primamg
       type(primail),intent(in) ::  aprl(:)
       double precision, intent(in) :: zv(:),zb(:)
       double precision, intent(out) :: zr(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine residual
     
     subroutine restriction(zbc, opr_fc, zrf, nd, stat, end)
       use m_primamg
       type(primail), intent(in) :: opr_fc(:)
       double precision, intent(in) :: zrf(:)
       double precision, intent(out) :: zbc(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine restriction
     
     subroutine prolongation(zuf, opr_cf, zuc, nd, stat, end)
       use m_primamg
       type(primail), intent(in) :: opr_cf(:)
       double precision, intent(in) :: zuc(:)
       double precision, intent(inout) :: zuf(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine prolongation
     
     subroutine normalization(zuc, zbc, aprl, nd)
       use m_primamg
       double precision, intent(in) :: zbc(:)
       double precision, intent(inout) :: zuc(:)
       type(primail), intent(in) :: aprl(:)
       integer, intent(in) :: nd
     endsubroutine normalization
     
     subroutine solve_coarsest(zu, aprl, zb, zr, nd, nitr, threshold, ig_max)
       use m_primamg
       double precision, intent(in) :: zb(:)
       double precision, intent(in) :: threshold
       type(primail), intent(in)::  aprl(:)
       integer, intent(in) :: nd, nitr, ig_max
       double precision, intent(out) :: zu(:), zr(:)
     end subroutine solve_coarsest

  end interface
  
  me_omp = 1
  !$ me_omp = omp_get_thread_num()+1

  nitr=amccs%param(7)
  itr_pr = amccs%param(9)
  itr_po = amccs%param(10)
  itr_ipr = amccs%param(11)
  itr_ipo = amccs%param(12)
  threshold = amccs%param(8)

 ! ndf=amcf%nd; ndc=amccs%apr(1)%nd
 ! puf=>zu; pbf=>zb; puc=>amccs%uc(1); pbc=>amccs%bc(1)
 ! call gs_ail(puf,amcf%aprl,pbf,ndf,nitr)
 ! call restriction(pbc,amcf%aprl,amccs%tfc(1),puf,pbf)

  stat = amccs%order(1)%thread_rmnd(me_omp)%stat
  end = amccs%order(1)%thread_rmnd(me_omp)%end
  do i = stat, end
     diag = amccs%Apr_reord(1)%aprl(i)%lct(1)
     amccs%rc(1)%v(diag) = amccs%bc(1)%v(diag)
  enddo
  do clr = 1, amccs%order(1)%c_num
     !do blc = 1, amccs%order(1)%color(clr)%b_num
     do blc = amccs%order(1)%color(clr)%thread(me_omp)%stat, amccs%order(1)%color(clr)%thread(me_omp)%end
        stat = amccs%order(1)%color(clr)%iblock(blc)%stat
        end = amccs%order(1)%color(clr)%iblock(blc)%end
        call residual(amccs%rc(1)%v, amccs%Apr_reord(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, stat, end) !Residual calculation
     enddo
  enddo
  !$OMP barrier

  !allocate(test_array(amccs%apr(1)%nd))
  !call residual(test_array, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)
  !call residual(amccs%rc(1)%v, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)

  ! do i = 1, amccs%apr(1)%nd
  !    if(test_array(i) /= amccs%rc(1)%v(i)) write(*, *) 'There is a diffrent on ', i, 'between', test_array(i), 'and',  amccs%rc(1)%v(i)
  ! enddo
  ! stop

  stat = amccs%order(1)%thread(me_omp)%stat
  end = amccs%order(1)%thread(me_omp)%end
  if(me_omp == 1) bnorm = 0.0d0
  !$OMP barrier
  tmp_d = 0.0d0
  do i = stat, end
     tmp_d = tmp_d + amccs%rc(1)%v(i) * amccs%rc(1)%v(i)
  enddo
  !$OMP atomic
  bnorm = bnorm + tmp_d
  !$OMP barrier
  if(me_omp == 1) bnorm = sqrt(bnorm)

  stat = amccs%order(1)%thread_rmnd(me_omp)%stat
  end = amccs%order(1)%thread_rmnd(me_omp)%end
  do i = stat, end
     diag = amccs%Apr_reord(1)%aprl(i)%lct(1)
     amccs%uc(1)%v(diag) = amccs%Apr_reord(1)%aprl(i)%ado * amccs%bc(1)%v(diag)
  enddo

  do itr = 1, nitr
 
     do ig=1,amccs%nm-1

        time(ig+2) = time(ig+2) - omp_get_wtime()
        ! ndf=amccs%apr(ig)%nd; ndc=amccs%apr(ig+1)%nd
        ! puf=>amccs%uc(ig); pbf=>amccs%bc(ig); puc=>amccs%uc(ig+1); pbc=>amccs%bc(ig+1)
        if(ig /= 1) then
           stat = amccs%order(ig)%thread(me_omp)%stat
           end = amccs%order(ig)%thread(me_omp)%end
           amccs%uc(ig)%v(stat:end) = 0.0d0
           !$OMP barrier
        endif

        do itr_o = 1, itr_pr
           do clr = 1, amccs%order(ig)%c_num
              ! do blc = 1, amccs%order(ig)%color(clr)%b_num
              if(ig == 1) time(amccs%nm+3) = time(amccs%nm+3) - omp_get_wtime()
              do blc = amccs%order(ig)%color(clr)%thread(me_omp)%stat, amccs%order(ig)%color(clr)%thread(me_omp)%end
                 stat = amccs%order(ig)%color(clr)%iblock(blc)%stat
                 end = amccs%order(ig)%color(clr)%iblock(blc)%end
                 call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd &
                           , stat, end, itr_ipr, time(1:2), ig) !Pre-smoothing
              enddo
              if(ig == 1) time(amccs%nm+3) = time(amccs%nm+3) + omp_get_wtime()
              !$OMP barrier
           enddo
        enddo
        ! call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr)

        ! write(*, *) 'after val', ig, sum(amccs%uc(ig)%v(:))
        ! write(*, *) 'Check aprl', amccs%apr(2)%aprl(1)%avl(:)

        if(ig == 1) time(amccs%nm+4) = time(amccs%nm+4) - omp_get_wtime()
        stat = amccs%order(ig)%thread_rmnd(me_omp)%stat
        end = amccs%order(ig)%thread_rmnd(me_omp)%end
        do i = stat, end
           diag = amccs%Apr_reord(ig)%aprl(i)%lct(1)
           amccs%rc(ig)%v(diag) = amccs%bc(ig)%v(diag) - amccs%Apr_reord(ig)%aprl(i)%avl(1) * amccs%uc(ig)%v(diag)
        enddo
        do clr = 1, amccs%order(ig)%c_num
           ! do blc = 1, amccs%order(ig)%color(clr)%b_num
           do blc = amccs%order(ig)%color(clr)%thread(me_omp)%stat, amccs%order(ig)%color(clr)%thread(me_omp)%end
              stat = amccs%order(ig)%color(clr)%iblock(blc)%stat
              end = amccs%order(ig)%color(clr)%iblock(blc)%end
              call residual(amccs%rc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%uc(ig)%v, amccs%bc(ig)%v, amccs%apr(ig)%nd, stat, end) !Residual
           enddo
        enddo
        if(ig == 1) time(amccs%nm+4) = time(amccs%nm+4) + omp_get_wtime()
        !$OMP barrier
        ! call residual(amccs%rc(ig)%v, amccs%apr(ig)%aprl, amccs%uc(ig)%v, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd)
        ! write(*, *) 'after cal_res', ig, sum(amccs%rc(ig)%v(:))

        if(ig == 1) time(amccs%nm+5) = time(amccs%nm+5) - omp_get_wtime()
        stat = amccs%order(ig+1)%thread(me_omp)%stat
        end = amccs%order(ig+1)%thread(me_omp)%end
        call restriction(amccs%bc(ig+1)%v, amccs%tfc(ig)%aprl, amccs%rc(ig)%v, amccs%tfc(ig)%nd, stat, end) !Restriction
        if(ig == 1) time(amccs%nm+5) = time(amccs%nm+5) + omp_get_wtime()
        !$OMP barrier
        ! write(*, *) 'after rest', ig, sum(amccs%bc(ig+1)%v(:))

        time(ig+2) = time(ig+2) + omp_get_wtime()
     enddo

     time(amccs%nm+2) = time(amccs%nm+2) - omp_get_wtime()
     if(amccs%param(18) == 1) then
        stat = amccs%order(amccs%nm)%thread(me_omp)%stat
        end = amccs%order(amccs%nm)%thread(me_omp)%end
        ! write(*, *) 'Check', me_omp, stat, end
        ! call dgemv('N', amccs%apr(amccs%nm)%nd, amccs%apr(amccs%nm)%nd, 1.0d0, amccs%apr_dense%avl, amccs%apr(amccs%nm)%nd, &
        !            amccs%bc(amccs%nm)%v, 1, 0.0d0, amccs%uc(amccs%nm)%v, 1)
        amccs%uc(amccs%nm)%v(stat:end) = matmul(amccs%apr_dense%avl(stat:end, :) ,amccs%bc(amccs%nm)%v)
     else
        !$OMP master
        call solve_coarsest(amccs%uc(amccs%nm)%v, amccs%apr(amccs%nm)%aprl, amccs%bc(amccs%nm)%v, amccs%rc(amccs%nm)%v, amccs%apr(amccs%nm)%nd &
                          , int(amccs%param(13)), amccs%param(14), amccs%nm)
        !$OMP end master
     endif
     !$OMP barrier
     time(amccs%nm+2) = time(amccs%nm+2) + omp_get_wtime()
     ! call normalization((puc,amccs%apr(amccs%nm),pbc)
     ! call prolongation(puf,amccs%tcf(amccs%nm),puc)

     do ig=amccs%nm-1,1,-1

        time(ig+2) = time(ig+2) - omp_get_wtime()
        ! ndf=amccs%apr(ig-1)%nd; ndc=amccs%apr(ig)%nd
        ! puf=>amccs%uc(ig); pbf=>amccs%bc(ig-1); puc=>amccs%uc(ig); pbc=>amccs%bc(ig-1)
        ! call normalization(puc,amccs%apr(ig),pbc)
        if(ig == 1) time(amccs%nm+6) = time(amccs%nm+6) - omp_get_wtime()
        stat = amccs%order(ig)%thread(me_omp)%stat
        end = amccs%order(ig)%thread(me_omp)%end
        call prolongation(amccs%uc(ig)%v, amccs%tcf(ig)%aprl, amccs%uc(ig+1)%v, amccs%tcf(ig)%nd, stat, end) !Prolongation
        if(ig == 1) time(amccs%nm+6) = time(amccs%nm+6) + omp_get_wtime()
        !$OMP barrier

        do itr_o = 1, itr_po
           do clr = amccs%order(ig)%c_num, 1, -1
              !do blc = 1, amccs%order(ig)%color(clr)%b_num
              if(ig == 1) time(amccs%nm+3) = time(amccs%nm+3) - omp_get_wtime()
              do blc = amccs%order(ig)%color(clr)%thread(me_omp)%stat, amccs%order(ig)%color(clr)%thread(me_omp)%end
                 stat = amccs%order(ig)%color(clr)%iblock(blc)%stat
                 end = amccs%order(ig)%color(clr)%iblock(blc)%end
                 call gs_ail_rev(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd&
                               , stat, end, itr_ipo, time(1:2), ig) !Post-smoothing
                 ! call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, stat, end, itr_po) !Post-smoothing
              enddo
              if(ig == 1) time(amccs%nm+3) = time(amccs%nm+3) + omp_get_wtime()
              !$OMP barrier
           enddo
        enddo
        ! call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr)
        ! call gs_ail_rev(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr)

        time(ig+2) = time(ig+2) + omp_get_wtime()
     
     enddo

     time(3) = time(3) - omp_get_wtime()
     time(amccs%nm+4) = time(amccs%nm+4) - omp_get_wtime()
     do clr = 1, amccs%order(1)%c_num
        ! do blc = 1, amccs%order(1)%color(clr)%b_num
        do blc = amccs%order(1)%color(clr)%thread(me_omp)%stat, amccs%order(1)%color(clr)%thread(me_omp)%end
           stat = amccs%order(1)%color(clr)%iblock(blc)%stat
           end = amccs%order(1)%color(clr)%iblock(blc)%end
           call residual(amccs%rc(1)%v, amccs%Apr_reord(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, stat, end) !Residual calculation
        enddo
     enddo
     time(amccs%nm+4) = time(amccs%nm+4) + omp_get_wtime()
     !$OMP barrier
     time(3) = time(3) + omp_get_wtime()
     
     ! call residual(amccs%rc(1)%v, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)
     ! allocate(test_array(amccs%apr(1)%nd))
     ! call residual(test_array, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)

     ! do i = 1, amccs%apr(1)%nd
     !    if(test_array(i) /= amccs%rc(1)%v(i)) write(*, *) 'There is a diffrent on ', i, 'between', test_array(i), 'and',  amccs%rc(1)%v(i)
     ! enddo
     ! stop

     stat = amccs%order(1)%thread(me_omp)%stat
     end = amccs%order(1)%thread(me_omp)%end
     if(me_omp == 1) res_norm = 0.0d0
     !$OMP barrier
     tmp_d = 0.0d0
     do i = stat, end
        tmp_d = tmp_d + amccs%rc(1)%v(i) * amccs%rc(1)%v(i)
     enddo
     !$OMP atomic
     res_norm = res_norm + tmp_d
     !$OMP barrier
     if(me_omp == 1) then
        res_norm = sqrt(res_norm) / bnorm
        write(*, '(a10, i3, a16, e12.5)') 'AMG itr = ', itr, 'residual norm = ', res_norm
     endif

     ! !$OMP master
     ! res_norm = dot_product(amccs%rc(1)%v(:), amccs%rc(1)%v(:))
     ! res_norm = sqrt(res_norm)/bnorm
     ! write(*, '(a6, i3, a16, e12.5)') 'itr = ', itr, 'residual norm = ', res_norm
     ! !$OMP end master
     ! !$OMP barrier
     if(res_norm <= threshold) then
        if(me_omp == 1) write(*, *) 'AMG convarged'
        exit
     endif
   
  enddo

 ! call gs_ail(puf,amccs%apr(1)%aprl,pbf,ndf,nitr)
 ! call normalization(puf,amccs%apr(1),pbf)
 ! call prolongation(zu,amccs%tcf(1),puf)
 
endsubroutine vcycle_symmetric

subroutine vcycle_symmetric_seq(amccs, time)
  use m_primamg
  implicit none
  type(primaprccs), intent(inout) :: amccs
  double precision, intent(inout) :: time(2)
  integer nitr, itr_pr, itr_po, itr_ipr, itr_ipo, itr, ig, itr_o
  double precision threshold, tmp_d
  double precision, save :: bnorm, res_norm
  integer i, clr, blc, stat, end, me_omp, diag
  double precision, allocatable :: test_array(:)
  
  interface
    
     subroutine residual(zr,aprl,zv,zb,nd,stat,end)
       use m_primamg
       type(primail),intent(in) ::  aprl(:)
       double precision, intent(in) :: zv(:),zb(:)
       double precision, intent(out) :: zr(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine residual
     
     subroutine restriction(zbc, opr_fc, zrf, nd, stat, end)
       use m_primamg
       type(primail), intent(in) :: opr_fc(:)
       double precision, intent(in) :: zrf(:)
       double precision, intent(out) :: zbc(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine restriction
     
     subroutine prolongation(zuf, opr_cf, zuc, nd, stat, end)
       use m_primamg
       type(primail), intent(in) :: opr_cf(:)
       double precision, intent(in) :: zuc(:)
       double precision, intent(inout) :: zuf(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine prolongation
     
     subroutine normalization(zuc, zbc, aprl, nd)
       use m_primamg
       double precision, intent(in) :: zbc(:)
       double precision, intent(inout) :: zuc(:)
       type(primail), intent(in) :: aprl(:)
       integer, intent(in) :: nd
     endsubroutine normalization
     
     subroutine solve_coarsest(zu, aprl, zb, zr, nd, nitr, threshold, ig_max)
       use m_primamg
       double precision, intent(in) :: zb(:)
       double precision, intent(in) :: threshold
       type(primail), intent(in)::  aprl(:)
       integer, intent(in) :: nd, nitr, ig_max
       double precision, intent(out) :: zu(:), zr(:)
     end subroutine solve_coarsest

  end interface
  
  me_omp = 1
  !$ me_omp = omp_get_thread_num()+1

  nitr=amccs%param(7)
  itr_pr = amccs%param(9)
  itr_po = amccs%param(10)
  itr_ipr = amccs%param(11)
  itr_ipo = amccs%param(12)
  threshold = amccs%param(8)

 ! ndf=amcf%nd; ndc=amccs%apr(1)%nd
 ! puf=>zu; pbf=>zb; puc=>amccs%uc(1); pbc=>amccs%bc(1)
 ! call gs_ail(puf,amcf%aprl,pbf,ndf,nitr)
 ! call restriction(pbc,amcf%aprl,amccs%tfc(1),puf,pbf)

  stat = 1
  end = amccs%Apr(1)%nd

  call residual(amccs%rc(1)%v, amccs%Apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, stat, end) !Residual calculation

  !allocate(test_array(amccs%apr(1)%nd))
  !call residual(test_array, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)
  !call residual(amccs%rc(1)%v, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)

  ! do i = 1, amccs%apr(1)%nd
  !    if(test_array(i) /= amccs%rc(1)%v(i)) write(*, *) 'There is a diffrent on ', i, 'between', test_array(i), 'and',  amccs%rc(1)%v(i)
  ! enddo
  ! stop

  if(me_omp == 1) bnorm = 0.0d0
  !$OMP barrier
  tmp_d = 0.0d0
  do i = stat, end
     tmp_d = tmp_d + amccs%rc(1)%v(i) * amccs%rc(1)%v(i)
  enddo
  !$OMP atomic
  bnorm = bnorm + tmp_d
  !$OMP barrier
  if(me_omp == 1) bnorm = sqrt(bnorm)

  do i = stat, end
     diag = amccs%Apr(1)%aprl(i)%lct(1)
     amccs%uc(1)%v(diag) = amccs%Apr(1)%aprl(i)%ado * amccs%bc(1)%v(diag)
  enddo

  do itr = 1, nitr
 
     do ig=1,amccs%nm-1

        stat = 1
        end = amccs%Apr(ig)%nd

        ! ndf=amccs%apr(ig)%nd; ndc=amccs%apr(ig+1)%nd
        ! puf=>amccs%uc(ig); pbf=>amccs%bc(ig); puc=>amccs%uc(ig+1); pbc=>amccs%bc(ig+1)
        if(ig /= 1) then
           amccs%uc(ig)%v(stat:end) = 0.0d0
           !$OMP barrier
        endif

        do itr_o = 1, itr_pr
           call gs_ail(amccs%uc(ig)%v, amccs%Apr(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, stat, end, itr_ipr, time, ig) !Pre-smoothing
        enddo
        ! call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr)

        ! write(*, *) 'after val', ig, sum(amccs%uc(ig)%v(:))
        ! write(*, *) 'Check aprl', amccs%apr(2)%aprl(1)%avl(:)

        do i = stat, end
           diag = amccs%Apr(ig)%aprl(i)%lct(1)
           amccs%rc(ig)%v(diag) = amccs%bc(ig)%v(diag) - amccs%Apr(ig)%aprl(i)%avl(1) * amccs%uc(ig)%v(diag)
        enddo
        call residual(amccs%rc(ig)%v, amccs%Apr(ig)%aprl, amccs%uc(ig)%v, amccs%bc(ig)%v, amccs%apr(ig)%nd, stat, end) !Residual
        !$OMP barrier
        ! call residual(amccs%rc(ig)%v, amccs%apr(ig)%aprl, amccs%uc(ig)%v, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd)
        ! write(*, *) 'after cal_res', ig, sum(amccs%rc(ig)%v(:))

        stat = 1
        end = amccs%Apr(ig+1)%nd
        call restriction(amccs%bc(ig+1)%v, amccs%tfc(ig)%aprl, amccs%rc(ig)%v, amccs%tfc(ig)%nd, stat, end) !Restriction
        !$OMP barrier
        ! write(*, *) 'after rest', ig, sum(amccs%bc(ig+1)%v(:))

     enddo

     !$OMP master
     call solve_coarsest(amccs%uc(amccs%nm)%v, amccs%apr(amccs%nm)%aprl, amccs%bc(amccs%nm)%v, amccs%rc(amccs%nm)%v, amccs%apr(amccs%nm)%nd &
                      , int(amccs%param(13)), amccs%param(14), amccs%nm)
     !$OMP end master
     !$OMP barrier
     ! call prolongation(puf,amccs%tcf(amccs%nm),puc)

     do ig=amccs%nm-1,1,-1

        ! ndf=amccs%apr(ig-1)%nd; ndc=amccs%apr(ig)%nd
        ! puf=>amccs%uc(ig); pbf=>amccs%bc(ig-1); puc=>amccs%uc(ig); pbc=>amccs%bc(ig-1)
        ! call normalization(puc,amccs%apr(ig),pbc)
        stat = 1
        end = amccs%Apr(ig)%nd

        ! call normalization(amccs%uc(ig+1)%v, amccs%bc(ig+1)%v, amccs%Apr(ig+1)%aprl, amccs%Apr(ig+1)%nd)

        call prolongation(amccs%uc(ig)%v, amccs%tcf(ig)%aprl, amccs%uc(ig+1)%v, amccs%tcf(ig)%nd, stat, end) !Prolongation
        !$OMP barrier

        do itr_o = 1, itr_po
           call gs_ail_rev(amccs%uc(ig)%v, amccs%Apr(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, stat, end, itr_ipo, time, ig) !Post-smoothing
                 ! call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, stat, end, itr_po) !Post-smoothing
        enddo
        ! call gs_ail(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr)
        ! call gs_ail_rev(amccs%uc(ig)%v, amccs%Apr_reord(ig)%aprl, amccs%bc(ig)%v, amccs%apr(ig)%nd, 1, amccs%apr(ig)%nd, itr_pr)

     enddo

     stat = 1
     end = amccs%Apr(1)%nd

     call residual(amccs%rc(1)%v, amccs%Apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, stat, end) !Residual calculation
     !$OMP barrier
     
     ! call residual(amccs%rc(1)%v, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)
     ! allocate(test_array(amccs%apr(1)%nd))
     ! call residual(test_array, amccs%apr(1)%aprl, amccs%uc(1)%v, amccs%bc(1)%v, amccs%apr(1)%nd, 1, amccs%apr(1)%nd)

     ! do i = 1, amccs%apr(1)%nd
     !    if(test_array(i) /= amccs%rc(1)%v(i)) write(*, *) 'There is a diffrent on ', i, 'between', test_array(i), 'and',  amccs%rc(1)%v(i)
     ! enddo
     ! stop

     if(me_omp == 1) res_norm = 0.0d0
     !$OMP barrier
     tmp_d = 0.0d0
     do i = stat, end
        tmp_d = tmp_d + amccs%rc(1)%v(i) * amccs%rc(1)%v(i)
     enddo
     !$OMP atomic
     res_norm = res_norm + tmp_d
     !$OMP barrier
     if(me_omp == 1) then
        res_norm = sqrt(res_norm) / bnorm
        write(*, '(a10, i3, a16, e12.5)') 'AMG itr = ', itr, 'residual norm = ', res_norm
     endif

     ! !$OMP master
     ! res_norm = dot_product(amccs%rc(1)%v(:), amccs%rc(1)%v(:))
     ! res_norm = sqrt(res_norm)/bnorm
     ! write(*, '(a6, i3, a16, e12.5)') 'itr = ', itr, 'residual norm = ', res_norm
     ! !$OMP end master
     ! !$OMP barrier
     if(res_norm <= threshold) then
        if(me_omp == 1) write(*, *) 'AMG convarged'
        exit
     endif
   
  enddo

 ! call gs_ail(puf,amccs%apr(1)%aprl,pbf,ndf,nitr)
 ! call normalization(puf,amccs%apr(1),pbf)
 ! call prolongation(zu,amccs%tcf(1),puf)
 
endsubroutine vcycle_symmetric_seq

subroutine residual(zr,aprl,zv,zb,nd,stat,end)
  use m_primamg
  implicit none
  type(primail),intent(in) ::  aprl(:)
  double precision, intent(in) :: zv(:),zb(:)
  double precision, intent(out) :: zr(:)
  integer, intent(in) :: nd, stat, end
  double precision zz
  integer il, it, nt, diag
  
  do il=stat,end
     diag = aprl(il)%lct(1)

     zr(diag) = zb(diag)
     do it=1,aprl(il)%ndl
        nt=aprl(il)%lct(it); zz=aprl(il)%avl(it)
        zr(diag)= zr(diag) - zz*zv(nt)
     enddo

  enddo
  
endsubroutine residual

!***restriction
subroutine restriction(zbc, opr_fc, zrf, nd, stat, end)
  use m_primamg
  implicit none
  type(primail), intent(in) :: opr_fc(:)
  double precision, intent(in) :: zrf(:)
  double precision, intent(out) :: zbc(:)
  integer, intent(in) :: nd, stat, end
  double precision zz
  integer il, it, nt
 
  do il = stat, end
     zbc(il) = 0.0d0
     do it = 1, opr_fc(il)%ndl
        nt=opr_fc(il)%lct(it); zz=opr_fc(il)%avl(it)
        zbc(il)=zbc(il) + zz*zrf(nt)
     enddo
  enddo

endsubroutine restriction

!***prolongation
subroutine prolongation(zuf, opr_cf, zuc, nd, stat, end)
  use m_primamg
  implicit none
  type(primail), intent(in) :: opr_cf(:)
  double precision, intent(in) :: zuc(:)
  double precision, intent(inout) :: zuf(:)
  integer, intent(in) :: nd, stat, end
  double precision zz
  integer il, it, nt
  
  do il = stat, end
     do it = 1, opr_cf(il)%ndl
        nt=opr_cf(il)%lct(it); zz=opr_cf(il)%avl(it)
        zuf(il)=zuf(il) + zz*zuc(nt)
     enddo
  enddo

endsubroutine prolongation

!***normalization
subroutine normalization(zuc, zbc, aprl, nd)
  use m_primamg
  implicit none
  integer, intent(in) :: nd
  double precision, intent(in) :: zbc(:)
  double precision, intent(inout) :: zuc(:)
  type(primail), intent(in) :: aprl(:)
  double precision, allocatable :: tmp_d(:)
  double precision s1, s2
  integer i

  allocate(tmp_d(nd))

  do i = 1, nd
     tmp_d(i) = sum(aprl(i)%avl(:))
  enddo
  
  s1 = dot_product(zuc(:), zbc(:))
  s2 = dot_product(zuc(:), tmp_d(:))
  
  ! write(*, *) 'normalization', s1/s2

  zuc(:) = s1/s2 * zuc(:)

endsubroutine normalization

!***solve_corsest
subroutine solve_coarsest(zu, aprl, zb, zr, nd, nitr, threshold, ig_max)
  use m_primamg
  implicit none
  double precision, intent(in) :: zb(:)
  double precision, intent(in) :: threshold
  type(primail), intent(in)::  aprl(:)
  integer, intent(in) :: nd, nitr, ig_max
  double precision, intent(out) :: zu(:), zr(:)
  double precision bnorm, res_norm
  double precision :: dum(2)
  integer itr

  interface
     subroutine residual(zr,aprl,zv,zb,nd,stat,end)
       use m_primamg
       type(primail),intent(in) ::  aprl(:)
       double precision, intent(in) :: zv(:),zb(:)
       double precision, intent(out) :: zr(:)
       integer, intent(in) :: nd, stat, end
     endsubroutine residual
  end interface
 
  zu(:)= 0.0d0
  ! write(*, *) 'Check1', zb(:)
  call residual(zr, aprl, zu, zb, nd, 1, nd) !Residual calculation
  bnorm = dot_product(zr(:), zr(:))
  bnorm = sqrt(bnorm)
  if(bnorm == 0.0d0) then
     write(*, *) 'Norm of initial residual is zero.'
     return
  endif
  ! write(*, *) 'Coarsest', bnorm

  do itr = 1, nitr

     call gs_ail(zu, aprl, zb, nd, 1, nd, 1, dum, ig_max)
     call gs_ail_rev(zu, aprl, zb, nd, 1, nd, 1, dum, ig_max)
     call residual(zr, aprl, zu, zb, nd, 1, nd) !Residual calculation
     res_norm = dot_product(zr(:), zr(:))
     res_norm = sqrt(res_norm)/bnorm
     ! write(*, *) 'itr = ', itr, 'residual norm = ', res_norm
     if(res_norm <= threshold) then
        write(*, *) 'Convarged on coarsest itr = ', itr*2, res_norm
        exit
     endif  

  enddo

  if(itr == nitr+1) write(*, *) 'Coarsest iteration is not convarged.', itr-1, res_norm

endsubroutine solve_coarsest
 
subroutine test_mat_check(amcf, tfc, tcf, amcc)
  use m_primamg
  implicit none
  type(primapr), intent(in) :: amcf, tfc, tcf, amcc
  double precision, allocatable :: amcf_full(:, :), tfc_full(:, :), tcf_full(:, :), amcc_full(:, :)
  double precision, allocatable :: ans1(:, :), ans2(:, :)
  integer i, j, k

  allocate(amcf_full(amcf%nd, amcf%nd), tfc_full(tfc%nd, tcf%nd), tcf_full(tcf%nd, tfc%nd), amcc_full(amcc%nd, amcc%nd))
  allocate(ans1(tfc%nd, tcf%nd), ans2(tfc%nd, tfc%nd))

  amcf_full(:, :) = 0.0d0
  do i = 1, amcf%nd
     do j = 1, amcf%aprl(i)%ndl
        amcf_full(i, amcf%aprl(i)%lct(j)) = amcf%aprl(i)%avl(j)
     enddo
  enddo

  tfc_full(:, :) = 0.0d0
  do i = 1, tfc%nd
     do j = 1, tfc%aprl(i)%ndl
        tfc_full(i, tfc%aprl(i)%lct(j)) = tfc%aprl(i)%avl(j)
     enddo
  enddo

  tcf_full(:, :) = 0.0d0
  do i = 1, tcf%nd
     do j = 1, tcf%aprl(i)%ndl
        tcf_full(i, tcf%aprl(i)%lct(j)) = tcf%aprl(i)%avl(j)
     enddo
  enddo
  
  amcc_full(:, :) = 0.0d0
  do i = 1, amcc%nd
     do j = 1, amcc%aprl(i)%ndl
        amcc_full(i, amcc%aprl(i)%lct(j)) = amcc%aprl(i)%avl(j)
     enddo
  enddo

  write(*, *) 'From here is the result of checking routine.'
  !Check transposition
  do i = 1, tfc%nd
     do j = 1, tcf%nd
        if(tfc_full(i, j) /= tcf_full(j, i)) write(*, *) 'Test routine. Transposition is missing.', i, j, tfc_full(i, j), tcf_full(j, i)
     enddo
  enddo

  !Multiplication of tfc * amcf = ans1
  do j = 1, amcf%nd
     ans1(:, j) = 0.0d0
     do k = 1, amcf%nd
        do i = 1, tfc%nd
           ans1(i, j) = ans1(i, j) + tfc_full(i, k) * amcf_full(k, j)
        enddo
     enddo
  enddo

  ! do i = 1, tfc%nd
  !    do j = 1, amcf%nd
  !       ans1(i, j) = 0.0d0
  !       do k = 1, amcf%nd
  !          if(i == 1 .and. tfc_full(i, k) /= 0 .and. amcf_full(k, j) /=0) write(*, *) 'Check', i, k, tfc_full(i, k), k, j, amcf_full(k, j)
  !          ans1(i, j) = ans1(i, j) + tfc_full(i, k) * amcf_full(k, j)
  !       enddo
  !    enddo
  ! enddo
  
  !Output ans1
  write(*, *) 'Result of ans1'
  do i = 1, tfc%nd
     do j = 1, amcf%nd
        if(ans1(i, j) /= 0.0d0) write(*, *) i, j, ans1(i, j)
     enddo
  enddo
  
  !Multiplication of ans1 * tcf = ans2
  do j = 1, tfc%nd
     ans2(:, j) = 0.0d0
     do k = 1, amcf%nd
        do i = 1, tfc%nd
           ans2(i, j) = ans2(i, j) + ans1(i, k) * tcf_full(k, j)
        enddo
     enddo
  enddo
  
  !Output ans1
  write(*, *) 'Result of ans2'
  do i = 1, tfc%nd
     do j = 1, tfc%nd
        if(ans2(i, j) /= 0.0d0) write(*, *) i, j, ans2(i, j)
     enddo
  enddo

  !Check difference of amcc ans ans2
  do j = 1, amcc%nd
     do i = 1, amcc%nd
        if(amcc_full(i, j) /= ans2(i, j)) write(*, *) 'Diff', i, j, amcc_full(i, j), ans2(i, j)
     enddo
  enddo

end subroutine test_mat_check
