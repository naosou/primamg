!***dotp_d

 double precision function dotp_d(nd,za,zb,stat,end)
   implicit none
   integer, intent(in) :: nd, stat, end
   double precision, intent(in) :: za(:),zb(:)

   dotp_d=sum(za(stat:end)*zb(stat:end))

 end function

!***unrm_d
 double precision function unrm_d(nd,za)
 implicit double precision(a-h,o-z)
 double precision :: za(:)
 zscl=0.0d0; zzz=1.0d0
 do il=1,nd
   zza=abs(za(il))
   if(zza<1.0d-30) cycle
   if(zscl<zza)then
     zzz=1.0d0+zzz*(zscl/zza)*(zscl/zza)
     zscl=zza
   else
     zzz=zzz+(zza/zscl)*(zza/zscl)
   endif
 enddo
 unrm_d=zscl*dsqrt(zzz)
 end function
!*** maxabsvalloc_d
 subroutine maxabsvalloc_d(za,zz,il,nd)
 implicit double precision(a-h,o-z)
  double precision :: za(:)
  il = 1; zz = dabs(za(1))
  do it=2,nd
    if(dabs(za(it)) > zz)then
      il = it; zz = dabs(za(it))
    endif
  enddo
 endsubroutine
!*** maxabsvallocm_d
 subroutine maxabsvallocm_d(za,zz,il,nd,lmask)
 implicit double precision(a-h,o-z)
 double precision :: za(:)
 integer lmask(:)
  il = 0; zz = 0.0d0
  do it=1,nd
    if(lmask(it)==1) cycle
    if(dabs(za(it)) > zz)then
      il = it; zz = dabs(za(it))
    endif
  enddo
 endsubroutine
!*** minabsvalloc_d
 subroutine minabsvalloc_d(za,zz,il,nd)
 implicit double precision(a-h,o-z)
  double precision za(:)
  il = 1; zz = dabs(za(1))
  do it=2,nd
    if(dabs(za(it)) < zz)then
      il = it; zz = dabs(za(it))
    endif
  enddo
 endsubroutine

!***adot_dsm
 subroutine adot_dsm(zau,zaa,zu,ndl,ndt,mdl)
 implicit double precision(a-h,o-z)
 double precision :: zaa(:,:)
 double precision :: zu(:),zau(:)

 zau=0.0d0
 do it=1,ndt
   zau(1:ndl)=zau(1:ndl)+zaa(1:ndl,it)*zu(it)
 enddo

 end subroutine adot_dsm

!***adotsub_dsm
 subroutine adotsub_dsm(zr,zaa,zu,ndl,ndt,mdl)
 implicit double precision(a-h,o-z)
 double precision :: zaa(:,:)
 double precision :: zu(:),zr(:)
 double precision,dimension(:),allocatable :: zau

 interface
    subroutine adot_dsm(zau,zaa,zu,ndl,ndt,mdl)
      implicit double precision(a-h,o-z)
      double precision :: zaa(:,:)
      double precision :: zu(:),zau(:)
    end subroutine adot_dsm
 end interface

 allocate(zau(ndl))
 call adot_dsm(zau,zaa,zu,ndl,ndt,mdl)
 zr(1:ndl)=zr(1:ndl)-zau(1:ndl)
 deallocate(zau)

 end subroutine adotsub_dsm

!***adotprt_dsm
 subroutine adotprt_dsm(zau,zaa,zu,ndl,ndt,mdl,ils,ile)
 implicit double precision(a-h,o-z)
 double precision :: zaa(mdl,*)
 double precision :: zu(ndt),zau(ndl)
 zau(:)=0.0d0
 do it=1,ndt
   zau(ils:ile)=zau(ils:ile)+zaa(ils:ile,it)*zu(it)
 enddo
 end subroutine adotprt_dsm
!***adotsubprt_dsm
 subroutine adotsubprt_dsm(zr,zaa,zu,ndl,ndt,mdl,ils,ile)
 implicit double precision(a-h,o-z)
 double precision :: zaa(mdl,*)
 double precision :: zu(ndt),zr(ndl)
 double precision,dimension(:),allocatable :: zau
 allocate(zau(ndl))
! call adot_dsm(zau,zaa,zu,ndl,ndt,mdl,ils,ile)
 zr(ils:ile)=zr(ils:ile)-zau(ils:ile)
 deallocate(zau)
 end subroutine adotsubprt_dsm
!***strtend_omp
 subroutine strtend_omp(j0,j1,ith,nth,ndim)
 implicit double precision(a-h,o-z)
  inth = ith + nth;     nth2 = nth + nth
  ntdim = ndim / nth;    mth = ndim - ntdim*nth
  ntdim1 = ntdim + 1;     j0 = 0
  if ( ith .lt. mth ) then
    do i=1, ith; j0 = j0 + ntdim1; enddo
    j1 = j0 + ntdim1
  else
    do i=1, mth; j0 = j0 + ntdim1; enddo
    do i=mth+1, ith; j0 = j0 + ntdim; enddo
    j1 = j0 + ntdim
  endif
  j0=j0+1
 end subroutine strtend_omp
!***med3
 double precision function med3(nl,nr,nlr2)
    if(nl < nr)then
      if (nr < nlr2)then;  med3=nr; elseif (nlr2 < nl)then; med3=nl; else;  med3=nlr2; endif
    else
      if (nlr2 < nr)then;  med3=nr; elseif (nl < nlr2)then; med3=nl; else; med3=nlr2; endif
    endif
 endfunction

