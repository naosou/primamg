# SYSTEM = FX10
# SYSTEM = INTEL
# SYSTEM = SystemA
SYSTEM = GNU

#FX10
ifeq ($(SYSTEM),FX10)
OPTFLAGS = -fs
CC=mpifccpx
F90=mpifrtpx -Kfast,openmp
#F90=mpifrtpx -Kopenmp
CCFLAGS = $(OPTFLAGS)
F90FLAGS = $(OPTFLAGS) -Cfpp
LDFLAGS = -SSL2
endif

#SystemA
ifeq ($(SYSTEM),SystemA)
OPTFLAGS = -O3 -homp
CC=cc
F90=ftn
CCFLAGS = $(OPTFLAGS)
F90FLAGS = $(OPTFLAGS)
endif

#intel
ifeq ($(SYSTEM),INTEL)
#OPTFLAGS = -O3 -traceback -ip -heap-arrays -openmp
OPTFLAGS = -O3 -xHost -qopenmp -qno-opt-prefetch
# OPTFLAGS = -check all -g -traceback -qopenmp
CC=icc
F90=ifort
CCFLAGS = $(OPTFLAGS)
#F90FLAGS = $(OPTFLAGS) -fpp -assume nounderscore -names uppercase
F90FLAGS = -fpp -Doutput_matrix $(OPTFLAGS) -I/home/kawai/original_env/include/intel64/lp64/
#F90FLAGS = $(OPTFLAGS) -fpp -check all
#F90FLAGS = -fpe0 -traceback -g -CB -assume nounderscore -names lowercase -fpp -check all
#LDFLAGS = -mkl -trace
LDFLAGS = -mkl $(OPTFLAGS) /home/kawai/original_env/lib/intel64/libmkl_lapack95_lp64.a ./libmetis.a
endif

#GNU
ifeq ($(SYSTEM),GNU)
F90=gfortran
CCFLAGS=$(OPTFLAGS)
F90FLAGS= -Dgnu -cpp -fopenmp -ffree-form -fbounds-check -O0 -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -mcmodel=medium -ffree-line-length-none -g -I /home/naosou/original_env/opt/lapack95_gnu/
# F90FLAGS= -Dgnu -cpp -fopenmp -ffree-form -mcmodel=medium -ffree-line-length-none -g -O3 -I /home/naosou/original_env/opt/lapack95_gnu/
LDFLAGS= -Dgnu -cpp -fopenmp -ffree-form -fbounds-check -O0 -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -mcmodel=medium -ffree-line-length-none -g /home/naosou/original_env/opt/lapack95_gnu/lapack95.a ../metis-5.1.0/build/Linux-x86_64/libmetis/libmetis.a -lblas -llapack
# LDFLAGS= -Dgnu -cpp -fopenmp -ffree-form -mcmodel=medium -ffree-line-length-none -g -O3 /home/naosou/original_env/opt/lapack95_gnu/lapack95.a ../metis-5.1.0/build/Linux-x86_64/libmetis/libmetis.a -lblas -llapack
endif

LINK=$(F90)

OBJS= idalib.o m_primapr.o m_primamg.o test_primamg.o initialization.o ordering_3.o mod_mbmc.o original_matrix.o random_mod.o make_spatter_problem.o

TARGET=test_primamg

.SUFFIXES:
.SUFFIXES: .o .f90

$(TARGET): $(OBJS)
			$(LINK) -o $@ $(OBJS) $(LDFLAGS)

.f90.o:

			$(F90) $(F90FLAGS) -c $<

m_primapr.o ordering_3.o: mod_mbmc.o
m_primamg.o test_primamg.o initialization.o : m_primapr.o
original_matrix.o make_spatter_problem.o : random_mod.o

clean:
	rm -f $(OBJS) $(OBJS:.o=.mod) $(TARGET)

